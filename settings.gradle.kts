pluginManagement {
    includeBuild("build-logic")

    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        maven {
            setUrl("https://jitpack.io") // maven repo where the current library resides
        }
    }
}
rootProject.name = "VideoboxMultiplatform"


include(":common")

include(":client:androidApp")
include(":client:shared")
include(":client:desktop")
// работает через жопу. фиг с ним.
// include(":client:web")
include(":server:core")
include(":server:instrumentation")
include(":server:db")
include(":server:jwt")
include(":server:common")
