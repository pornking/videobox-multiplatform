package ru.shadowsparky.videobox.server.domain

import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.TokenResponse

data class JwtInfo(
    val secret: String,
    val issuer: String = "https://shadowsparky.ru/",
    val audience: String = "https://shadowsparky.ru/login",
    val realm: String = "server side data storage"
)

interface JwtInfoProvider {
    fun getJwtInfo(): JwtInfo

    fun prepare(
        loginInfo: LoginInfo,
        userId: Long
    ): TokenResponse
}
