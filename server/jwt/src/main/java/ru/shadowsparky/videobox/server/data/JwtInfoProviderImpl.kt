package ru.shadowsparky.videobox.server.data

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.koin.core.annotation.Single
import ru.shadowsparky.videobox.multiplatform.shared.domain.USER_ID_ARG
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.TokenResponse
import ru.shadowsparky.videobox.server.common.EnvFetcher
import ru.shadowsparky.videobox.server.domain.JwtInfo
import ru.shadowsparky.videobox.server.domain.JwtInfoProvider
import ru.shadowsparky.videobox.server.domain.LOGIN_NAME

@Single
class JwtInfoProviderImpl(
    private val envFetcher: EnvFetcher
) : JwtInfoProvider {
    private var cached: JwtInfo? = null

    override fun getJwtInfo(): JwtInfo {
        cached?.let { return it }
        val secret = envFetcher.get("VIDEOBOX_SECRET", "some_secret")
        return JwtInfo(secret).apply { cached = this }
    }

    override fun prepare(
        loginInfo: LoginInfo,
        userId: Long
    ): TokenResponse {
        val jwtInfo = getJwtInfo()
        val token = JWT.create()
            .withAudience(jwtInfo.audience)
            .withIssuer(jwtInfo.issuer)
            .withClaim(LOGIN_NAME, loginInfo.login)
            .withClaim(USER_ID_ARG, userId)
            .sign(Algorithm.HMAC512(jwtInfo.secret))
        return TokenResponse(token)
    }
}
