package ru.shadowsparky.videobox.server.domain

interface TokenVerifier {
    suspend fun verify(login: String)
}

const val LOGIN_NAME = "login"
const val INVALID_TOKEN_MSG = "Был передан неправильный токен"

class VerifyTokenException(override val message: String?) : IllegalStateException()
