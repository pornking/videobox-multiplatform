plugins {
    alias(libs.plugins.vb.server)
}

dependencies {
    implementation(project(":common"))
    implementation(project(":server:common"))
    implementation(libs.java.jwt)
}
