package ru.shadowsparky.videobox.server.common

import org.koin.core.annotation.ComponentScan
import org.koin.core.annotation.Module

@Module
@ComponentScan
class CommonModule
