package ru.shadowsparky.videobox.server

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.koin.core.component.inject
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginInfo

class AuthTest : BaseTest() {
    private val auth by koin.inject<AuthTokenRepository>()

    @Test
    fun login(): Unit = runBlocking {
        auth.login(LoginInfo("test", "test"))
    }
}
