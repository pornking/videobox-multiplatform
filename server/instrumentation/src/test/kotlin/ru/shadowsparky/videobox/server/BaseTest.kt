package ru.shadowsparky.videobox.server

import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.koin.core.component.get
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.di.newKoinModules
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfiguration
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository

abstract class BaseTest {

    @Before
    fun before() {
        startKoin {
            modules(newKoinModules(true))
        }
        runBlocking {
            koin.get< ServerConfigurationRepository>()
                .setServerConfiguration(ServerConfiguration("http://127.0.0.1:8181"))
        }
    }

    @After
    fun after() {
        stopKoin()
    }
}
