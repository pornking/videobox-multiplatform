plugins {
    alias(libs.plugins.vb.server)
}

dependencies {
    implementation(project(":common"))
    implementation(project(":client:shared"))

    testImplementation(libs.koin.test)
    testImplementation(libs.mockk)
    testImplementation(libs.junit)
}
