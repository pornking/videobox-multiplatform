plugins {
    alias(libs.plugins.vb.server)
    alias(libs.plugins.sqldelight)
}

sqldelight {
    databases {
        create("AppDatabase") {
            packageName.set("ru.shadowsparky.videobox.server")
            dialect("app.cash.sqldelight:postgresql-dialect:2.0.2")
        }
    }
}

dependencies {
    implementation(project(":common"))
    implementation(project(":server:common"))
    implementation(project(":server:jwt"))


    implementation(libs.jdbc.driver)
    implementation(libs.postgresql)
}
