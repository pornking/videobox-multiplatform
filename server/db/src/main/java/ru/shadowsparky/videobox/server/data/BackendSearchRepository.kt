package ru.shadowsparky.videobox.server.data

import kotlinx.coroutines.withContext
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEvent
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventHandler
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchRepository
import ru.shadowsparky.videobox.server.AppDatabase
import java.sql.SQLException

class BackendSearchRepository(
    private val db: AppDatabase,
    private val userId: Long,
    private val dispatcherProvider: DispatcherProvider,
    private val eventHandler: RemoteEventHandler
) : SearchRepository {

    override suspend fun search(query: String): List<String> {
        return withContext(dispatcherProvider.io) {
            if (query.trim().isEmpty()) {
                db.searchQueries.selectAll(userId)
            } else {
                db.searchQueries.selectByQuery(query.addWilcard(), userId)
            }.executeAsList().map { it.query }
        }
    }

    private fun String.addWilcard(): String {
        return if (this.endsWith("%")) this else "$this%"
    }

    override suspend fun addToSearch(query: String) {
        withContext(dispatcherProvider.io) {
            try {
                db.searchQueries.insertSearchInfo(query, userId)
                notifyChanged()
            } catch (ignored: SQLException) {
            }
        }
    }

    override suspend fun deleteFromSearch(query: String) {
        withContext(dispatcherProvider.io) {
            db.searchQueries.deleteSearchInfo(query, userId)
            notifyChanged()
        }
    }

    override suspend fun clear() {
        withContext(dispatcherProvider.io) {
            db.searchQueries.deleteAll(userId)
            notifyChanged()
        }
    }

    private suspend fun notifyChanged() {
        eventHandler.notify(RemoteEvent.OnSearch(System.currentTimeMillis(), userId))
    }
}
