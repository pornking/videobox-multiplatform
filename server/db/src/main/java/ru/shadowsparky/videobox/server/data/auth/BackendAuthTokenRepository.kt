package ru.shadowsparky.videobox.server.data.auth

import kotlinx.coroutines.withContext
import org.koin.core.annotation.Factory
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.ALREADY_REG_TEXT
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthException
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.UNKNOWN_USER_TEXT
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.TokenResponse
import ru.shadowsparky.videobox.server.AppDatabase
import ru.shadowsparky.videobox.server.domain.JwtInfoProvider
import ru.shadowsparky.videobox.server.domain.TokenVerifier

@Factory
class BackendAuthTokenRepository(
    private val db: AppDatabase,
    private val dispatcherProvider: DispatcherProvider,
    private val jwtInfoProvider: JwtInfoProvider,
    private val tokenVerifier: TokenVerifier
) : AuthTokenRepository {
    override suspend fun register(loginInfo: LoginInfo): TokenResponse = withContext(dispatcherProvider.io) {
        val user = db.usersQueries.selectUserByLogin(loginInfo.login).executeAsOneOrNull()
        if (user != null) {
            throw AuthException(ALREADY_REG_TEXT)
        }
        db.usersQueries.addUser(
            loginInfo.login,
            loginInfo.passwordHash,
            System.currentTimeMillis(),
            null
        )
        create(loginInfo).apply { tokenVerifier.verify(loginInfo.login) }
    }

    override suspend fun login(loginInfo: LoginInfo): TokenResponse {
        val userInfo = db.usersQueries.selectUserByLogin(loginInfo.login)
            .executeAsOneOrNull() ?: throw AuthException(UNKNOWN_USER_TEXT)
        if (loginInfo.passwordHash != userInfo.password_hash) {
            throw AuthException(UNKNOWN_USER_TEXT)
        }
        return create(loginInfo).apply { tokenVerifier.verify(loginInfo.login) }
    }

    private fun create(loginInfo: LoginInfo): TokenResponse {
        val userInfo = db.usersQueries.selectUserByLogin(loginInfo.login)
            .executeAsOneOrNull() ?: throw AuthException(UNKNOWN_USER_TEXT)
        return jwtInfoProvider.prepare(loginInfo, userInfo.user_id)
    }
}
