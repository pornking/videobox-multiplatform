package ru.shadowsparky.videobox.server.data

import kotlinx.coroutines.withContext
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEvent
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventHandler
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.RecentlyWatchedInfo
import ru.shadowsparky.videobox.server.AppDatabase
import java.sql.SQLException

class BackendRecentlyWatchedRepository(
    private val db: AppDatabase,
    private val userId: Long,
    private val dispatcherProvider: DispatcherProvider,
    private val remoteEventHandler: RemoteEventHandler
) : RecentlyWatchedRepository {

    override suspend fun removeInfo(info: RecentlyWatchedInfo) =
        withContext(dispatcherProvider.io) {
            db.recentQueries.deleteRwByIds(info.movieId, info.episode, info.season, userId)
            notifyChanged(info.movieId)
        }

    override suspend fun writeInfo(info: RecentlyWatchedInfo) =
        withContext(dispatcherProvider.io) {
            try {
                db.recentQueries.insertRecentlyWatched(
                    userId,
                    info.movieId,
                    info.episode,
                    info.season
                )
                notifyChanged(info.movieId)
            } catch (ignored: SQLException) {
            }
        }

    override suspend fun toggleInfo(info: RecentlyWatchedInfo) =
        withContext(dispatcherProvider.io) {
            val hasInfo =
                db.recentQueries.selectByIds(info.movieId, info.episode, info.season, userId)
                    .executeAsOneOrNull() != null
            if (hasInfo) {
                removeInfo(info)
            } else {
                writeInfo(info)
            }
        }

    override suspend fun queryRecentlyWatched(
        movieId: Long,
        seasonId: Long?
    ): List<RecentlyWatchedInfo> {
        val rsp = withContext(dispatcherProvider.io) {
            if (seasonId == null) {
                db.recentQueries.selectByMovieId(movieId, userId)
                    .executeAsList()
                    .map {
                        RecentlyWatchedInfo(
                            it.recently_watched_id,
                            it.movie_id,
                            it.episode,
                            it.season
                        )
                    }
            } else {
                db.recentQueries.selectByMovieIdAndSeason(movieId, userId, seasonId)
                    .executeAsList()
                    .map {
                        RecentlyWatchedInfo(
                            it.recently_watched_id,
                            it.movie_id,
                            it.episode,
                            seasonId
                        )
                    }
            }
        }
        return rsp
    }

    override suspend fun getAllRecentlyWatched(): List<RecentlyWatchedInfo> {
        return withContext(dispatcherProvider.io) {
            db.recentQueries.selectAll(userId).executeAsList()
                .map {
                    RecentlyWatchedInfo(
                        it.recently_watched_id,
                        it.movie_id,
                        it.episode,
                        it.season
                    )
                }
        }
    }

    private suspend fun notifyChanged(seasonId: Long) {
        remoteEventHandler.notify(RemoteEvent.OnRecent(System.currentTimeMillis(), userId, seasonId))
    }
}
