package ru.shadowsparky.videobox.server.data

import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.MOVIE_FLAG
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.SERIAL_FLAG
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.server.AppDatabase
import ru.shadowsparky.videobox.server.common.Logger
import java.sql.SQLException

class UpdateMovieVideoApi(
    private val wrapper: VideoApi,
    private val db: AppDatabase,
    private val logger: Logger
) : VideoApi by wrapper {
    override suspend fun fetchDetails(id: Long): VideoDetails {
        val details = wrapper.fetchDetails(id)
        try {
            db.movieQueries.insertMovie(
                details.id,
                details.poster,
                details.desc,
                details.title,
                if (details.isMovie) {
                    MOVIE_FLAG
                } else {
                    SERIAL_FLAG
                }
            )
        } catch (ignored: SQLException) {
        } catch (e: Exception) {
            logger.error("UpdateMovieVideoApi", e)
        }
        return details
    }
}
