package ru.shadowsparky.videobox.server.data.auth

import org.koin.core.annotation.Factory
import ru.shadowsparky.videobox.multiplatform.shared.domain.FLAG_USER_BLOCKED
import ru.shadowsparky.videobox.multiplatform.shared.domain.UNKNOWN_USER_TEXT
import ru.shadowsparky.videobox.multiplatform.shared.domain.USER_BLOCKED
import ru.shadowsparky.videobox.server.AppDatabase
import ru.shadowsparky.videobox.server.domain.TokenVerifier
import ru.shadowsparky.videobox.server.domain.VerifyTokenException

@Factory
class BackendTokenVerifier(
    private val db: AppDatabase
) : TokenVerifier {
    override suspend fun verify(login: String) {
        val user = db.usersQueries.selectUserByLogin(login)
            .executeAsOneOrNull()
            ?: throw VerifyTokenException(UNKNOWN_USER_TEXT)
        user.flags?.let {
            if ((it and FLAG_USER_BLOCKED) != 0) {
                throw VerifyTokenException(USER_BLOCKED)
            }
        }
    }
}
