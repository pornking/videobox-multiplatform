package ru.shadowsparky.videobox.server.factory

import org.koin.core.annotation.Factory
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventHandler
import ru.shadowsparky.videobox.server.AppDatabase
import ru.shadowsparky.videobox.server.data.BackendRecentlyWatchedRepository

@Factory
class RecentlyWatchedRepositoryFactory(
    private val db: AppDatabase,
    private val dispatcherProvider: DispatcherProvider,
    private val remoteEventHandler: RemoteEventHandler
) {
    fun create(userId: Long): RecentlyWatchedRepository {
        return BackendRecentlyWatchedRepository(db, userId, dispatcherProvider, remoteEventHandler)
    }
}
