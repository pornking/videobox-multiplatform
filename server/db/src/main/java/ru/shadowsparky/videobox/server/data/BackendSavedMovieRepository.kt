package ru.shadowsparky.videobox.server.data

import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.SERIAL_FLAG
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.server.AppDatabase
import ru.shadowsparky.videobox.server.Saved_movie
import ru.shadowsparky.videobox.server.common.Logger
import java.sql.SQLException

class BackendSavedMovieRepository(
    private val db: AppDatabase,
    private val userId: Long,
    private val dispatcherProvider: DispatcherProvider,
    private val savedMovieCache: SavedMovieCache,
    private val logger: Logger
) : SavedMovieRepository {

    override suspend fun getAll(): Flow<List<VideoDetails>> {
        return callbackFlow {
            val request = db.saved_movieQueries.selectByUserId(userId)
            val listener = Runnable {
                launch { send(request.executeAsList().parse()) }
            }
            val entry = SavedMovieCache.CacheEntry(userId, null)
            savedMovieCache.put(entry, listener)
            send(request.executeAsList().parse())
            awaitClose {
                savedMovieCache.remove(entry, listener)
            }
        }
    }

    private suspend fun List<Saved_movie>.parse(): List<VideoDetails> = withContext(dispatcherProvider.io) {
        mapNotNull {
            val movie = db.movieQueries.selectMovie(it.movie_id)
                .executeAsOneOrNull() ?: return@mapNotNull null
            VideoDetails(
                movie.movie_id,
                movie.poster_url,
                movie.description,
                movie.title,
                (movie.flags and SERIAL_FLAG) != 0
            )
        }
    }

    override suspend fun save(details: VideoDetails) {
        logger.debug(TAG, "save(${details.id}) ${details.title}")
        withContext(dispatcherProvider.io) {
            try {
                db.saved_movieQueries.addSavedMovie(
                    details.id,
                    userId
                )
                savedMovieCache.notifyAll(SavedMovieCache.CacheEntry(userId, details.id))
            } catch (ignored: SQLException) {
            }
        }
    }

    override suspend fun isSaved(id: Long): Flow<Boolean> {
        return callbackFlow {
            val request = db.saved_movieQueries.selectByUserIdAndMovieId(userId, id)
            val listener = Runnable {
                launch { send(request.executeAsOneOrNull() != null) }
            }
            val entry = SavedMovieCache.CacheEntry(userId, id)
            savedMovieCache.put(entry, listener)
            send(request.executeAsOneOrNull() != null)
            awaitClose {
                savedMovieCache.remove(entry, listener)
            }
        }
    }

    override suspend fun remove(id: Long) {
        logger.debug(TAG, "remove(${id})")
        withContext(dispatcherProvider.io) {
            db.saved_movieQueries.removeSavedMovie(userId, id)
            savedMovieCache.notifyAll(SavedMovieCache.CacheEntry(userId, id))
        }
    }

    private companion object {
        const val TAG = "BackendSavedMovieRepository"
    }
}
