package ru.shadowsparky.videobox.server.data

import org.koin.core.annotation.Single

@Single
class SavedMovieCache {
    private val map = HashMap<CacheEntry, List<Runnable>>()

    @Synchronized
    fun put(entry: CacheEntry, runnable: Runnable) {
        val list = getList(entry)
        list.add(runnable)
        map[entry] = list
    }

    @Synchronized
    fun remove(entry: CacheEntry, runnable: Runnable) {
        val list = getList(entry)
        list.firstOrNull { it == runnable }?.let { list.remove(it) }
        map[entry] = list
    }

    @Synchronized
    fun notifyAll(entry: CacheEntry) {
        notify(entry)
        notify(entry.copy(movieId = null))
    }

    @Synchronized
    private fun notify(entry: CacheEntry) {
        val list = getList(entry)
        list.forEach {
            try {
                it.run()
            } catch (e: Exception) {
                list.remove(it)
            }
        }
        map[entry] = list
    }

    private fun getList(entry: CacheEntry): MutableList<Runnable> {
        return map[entry]?.toMutableList() ?: mutableListOf()
    }

    data class CacheEntry(
        val userId: Long,
        val movieId: Long?
    )
}
