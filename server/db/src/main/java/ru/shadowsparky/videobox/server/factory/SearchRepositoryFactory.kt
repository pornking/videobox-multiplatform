package ru.shadowsparky.videobox.server.factory

import org.koin.core.annotation.Factory
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventHandler
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchRepository
import ru.shadowsparky.videobox.server.AppDatabase
import ru.shadowsparky.videobox.server.data.BackendSearchRepository

@Factory
class SearchRepositoryFactory(
    private val db: AppDatabase,
    private val dispatcherProvider: DispatcherProvider,
    private val eventHandler: RemoteEventHandler
) {
    fun create(userId: Long): SearchRepository {
        return BackendSearchRepository(db, userId, dispatcherProvider, eventHandler)
    }
}
