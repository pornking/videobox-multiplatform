package ru.shadowsparky.videobox.server.factory

import org.koin.core.annotation.Factory
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedMovieRepository
import ru.shadowsparky.videobox.server.AppDatabase
import ru.shadowsparky.videobox.server.common.Logger
import ru.shadowsparky.videobox.server.data.BackendSavedMovieRepository
import ru.shadowsparky.videobox.server.data.SavedMovieCache

@Factory
class SavedMovieRepositoryFactory(
    private val db: AppDatabase,
    private val dispatcherProvider: DispatcherProvider,
    private val savedMovieCache: SavedMovieCache,
    private val logger: Logger
) {
    fun create(userId: Long): SavedMovieRepository {
        return BackendSavedMovieRepository(db, userId, dispatcherProvider, savedMovieCache, logger)
    }
}
