package ru.shadowsparky.videobox.server.presentation.routing

import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.get
import io.ktor.server.util.getOrFail
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi

fun Routing.setupBaseMethods(api: VideoApi) {
    get(VideoApi.VIDEOS_PATH) {
        val p = call.parameters
        val page = p[VideoApi.PAGE_ARG]?.toIntOrNull() ?: 1
        val obj = api.fetchNewVideos(p[VideoApi.SEARCH_ARG], page)
        call.respond(obj)
    }
    get(VideoApi.DETAILS_PATH) {
        val p = call.parameters
        call.respond(api.fetchDetails(p.getOrFail(VideoApi.ID_ARG).toLong()))
    }
    get(VideoApi.LINKS_PATH) {
        val p = call.parameters
        call.respond(
            api.fetchVideoLinks(
                p.getOrFail(VideoApi.ID_ARG).toLong(),
                p[VideoApi.SEASON_ID_ARG]?.toLong()
            )
        )
    }
    get("/") {
        call.respond(HttpStatusCode.NotFound)
    }
}
