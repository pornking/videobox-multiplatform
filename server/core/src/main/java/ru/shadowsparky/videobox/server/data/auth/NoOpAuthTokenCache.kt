package ru.shadowsparky.videobox.server.data.auth

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import org.koin.core.annotation.Single
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.TokenResponse

@Single
class NoOpAuthTokenCache : AuthTokenCache {
    override val responseFlow: Flow<TokenResponse?> = flowOf(null)
    override suspend fun put(token: TokenResponse) = Unit
    override suspend fun remove() = Unit
}
