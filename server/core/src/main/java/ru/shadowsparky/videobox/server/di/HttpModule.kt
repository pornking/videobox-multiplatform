package ru.shadowsparky.videobox.server.di

import org.koin.core.annotation.Factory
import org.koin.core.annotation.Module
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.server.AppDatabase
import ru.shadowsparky.videobox.server.common.Logger
import ru.shadowsparky.videobox.server.data.UpdateMovieVideoApi
import ru.shadowsparky.videobox.server.data.http.FallbackVideoApi
import ru.shadowsparky.videobox.server.data.http.HlamerApi
import ru.shadowsparky.videobox.server.data.http.HtmlParseVideoApi

@Module
class HttpModule {

    @Factory
    fun provideVideoApi(
        impl: HlamerApi,
        dispatcherProvider: DispatcherProvider,
        appDatabase: AppDatabase,
        logger: Logger
    ): VideoApi {
        return FallbackVideoApi(
            UpdateMovieVideoApi(
                HtmlParseVideoApi(
                    impl,
                    dispatcherProvider
                ),
                appDatabase,
                logger
            )
        )
    }
}
