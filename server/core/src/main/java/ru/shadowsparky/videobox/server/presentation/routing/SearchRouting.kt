package ru.shadowsparky.videobox.server.presentation.routing

import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchDeleteRequest
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchQueryRequest
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchQueryResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchRepository
import ru.shadowsparky.videobox.server.factory.SearchRepositoryFactory
import ru.shadowsparky.videobox.server.presentation.obtainUserId

fun Route.setupSearch(searchFactory: SearchRepositoryFactory) {
    get(SearchRepository.SEARCH) {
        val p = call.parameters
        val userId = call.obtainUserId()
        val list = searchFactory.create(userId).search(p[SearchRepository.QUERY_ARG] ?: "")
        call.respond(SearchQueryResponse(list))
    }
    post(SearchRepository.ADD_TO_SEARCH) {
        val userId = call.obtainUserId()
        val request = call.receive<SearchQueryRequest>()
        searchFactory.create(userId).addToSearch(request.query)
        call.respond(HttpStatusCode.OK)
    }
    delete(SearchRepository.CLEAR) {
        val userId = call.obtainUserId()
        val request = call.receive<SearchDeleteRequest>()
        val repo = searchFactory.create(userId)
        if (request.query == null) {
            repo.clear()
        } else {
            repo.deleteFromSearch(request.query!!)
        }
        call.respond(HttpStatusCode.OK)
    }
}
