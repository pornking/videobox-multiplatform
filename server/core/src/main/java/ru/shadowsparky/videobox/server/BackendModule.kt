package ru.shadowsparky.videobox.server

import org.koin.core.annotation.ComponentScan
import org.koin.core.annotation.Module
import ru.shadowsparky.videobox.server.common.CommonModule
import ru.shadowsparky.videobox.server.di.EventModule
import ru.shadowsparky.videobox.server.di.HttpModule

@Module(
    includes = [
        CommonModule::class,
        DbModule::class,
        JwtModule::class,
        HttpModule::class,
        EventModule::class
    ]
)
@ComponentScan
class BackendModule
