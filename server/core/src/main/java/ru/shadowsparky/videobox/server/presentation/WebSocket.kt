package ru.shadowsparky.videobox.server.presentation

import io.ktor.server.application.Application
import io.ktor.server.application.install
import io.ktor.server.routing.Route
import io.ktor.server.routing.routing
import io.ktor.server.websocket.WebSockets
import io.ktor.server.websocket.pingPeriod
import io.ktor.server.websocket.timeout
import io.ktor.server.websocket.webSocket
import io.ktor.websocket.DefaultWebSocketSession
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.delay
import ru.shadowsparky.videobox.multiplatform.shared.domain.EventType
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventHandler
import ru.shadowsparky.videobox.server.data.BackendRemoteEventHandler
import ru.shadowsparky.videobox.server.data.SessionCache
import ru.shadowsparky.videobox.server.di.WebSocketEntryPoint
import java.time.Duration

fun Application.configureWebSocket(socketEntryPoint: WebSocketEntryPoint) {
    install(WebSockets) {
        pingPeriod = Duration.ofSeconds(15)
        timeout = Duration.ofSeconds(15)
        maxFrameSize = Long.MAX_VALUE
        masking = false
    }
    routing {
        webSocket(EventType.SEARCH, socketEntryPoint.searchEventHandler)
        webSocket(EventType.RECENT, socketEntryPoint.recentlyEventHandler)
    }
}

@OptIn(DelicateCoroutinesApi::class)
private fun Route.webSocket(eventType: String, handler: BackendRemoteEventHandler) {
    val path = when (eventType) {
        EventType.SEARCH -> RemoteEventHandler.SEARCH_CHANGED
        EventType.RECENT -> RemoteEventHandler.RECENTLY_CHANGED
        else -> throw IllegalArgumentException("Unsupported type $eventType")
    }
    webSocket(path) {
        val userId = getUserId()
        val session = object : SessionCache.Writer {
            override suspend fun writeText(text: String) {
                send(Frame.Text(text))
            }
        }
        handler.put(userId, session)
        try {
            while (!outgoing.isClosedForSend) {
                delay(100)
            }
        } finally {
            handler.remove(userId, session)
        }
    }
}

suspend fun DefaultWebSocketSession.getUserId(): Long {
    val token = this.incoming.receive() as Frame.Text
    return token.readText().obtainUserId()
}
