package ru.shadowsparky.videobox.server.di

import kotlinx.serialization.json.Json
import org.koin.core.annotation.Module
import org.koin.core.annotation.Named
import org.koin.core.annotation.Single
import ru.shadowsparky.videobox.multiplatform.shared.domain.EventType
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventHandler
import ru.shadowsparky.videobox.server.data.BackendRemoteEventHandler
import ru.shadowsparky.videobox.server.data.SessionCache

@Module
class EventModule {

    @Single(binds = [BackendRemoteEventHandler::class])
    @Named(EventType.SEARCH)
    fun provideSearch(json: Json, sessionCache: SessionCache): RemoteEventHandler {
        return BackendRemoteEventHandler(json, sessionCache)
    }

    @Single(binds = [BackendRemoteEventHandler::class])
    @Named(EventType.RECENT)
    fun provideRecent(json: Json, sessionCache: SessionCache): RemoteEventHandler {
        return BackendRemoteEventHandler(json, sessionCache)
    }
}
