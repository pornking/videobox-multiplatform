package ru.shadowsparky.videobox.server.data.http

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.url
import io.ktor.client.statement.bodyAsText
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import org.koin.core.annotation.Factory
import org.slf4j.LoggerFactory
import ru.shadowsparky.videobox.multiplatform.shared.domain.UnableToFetchDetailsException
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.Season
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoItem
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideosResponse

@Factory(binds = [HlamerApi::class])
class HlamerApi(
    private val client: HttpClient,
    private val mapper: HlamerMapper,
    private val cache: HlamerCache
) : VideoApi {
    private val logger = LoggerFactory.getLogger("hlamer")

    override suspend fun fetchNewVideos(search: String?, page: Int, sort: String): VideosResponse {
        logger.debug("fetchNewVideos($search, $page, $sort) called")
        return coroutineScope {
            val movies = async { fetchFavoritesInternal("movie", search) }
            val series = async { fetchFavoritesInternal("series", search) }
            val result = (series.await() + movies.await())
            result.forEach {
                cache.put(it.id, it)
            }
            return@coroutineScope VideosResponse(false, result, 1)
        }
    }

    private suspend fun fetchFavoritesInternal(request: String, search: String?): List<VideoItem> {
        val rsp = client.get {
            url("https", ENDPOINT, path = "/public/$request")
            if (search != null) {
                parameter("search", search)
            }
        }
        val rspText = rsp.bodyAsText()
        return mapper.movieRspToItems(rspText, request)
    }

    override suspend fun fetchDetails(id: Long): VideoDetails {
        val info = cache.get(id) ?: throw UnableToFetchDetailsException(id)
        logger.debug("fetchDetails($id) called. ${info.title}")
        return VideoDetails(
            poster = info.poster,
            id = info.id,
            title = info.title,
            desc = info.description ?: "",
            isSerial = info.isSerial
        )
    }

    override suspend fun fetchVideoLinks(
        id: Long,
        seasonId: Long?
    ): VideoLinksResponse {
        val isSerial = cache.get(id)?.isSerial ?: throw UnableToFetchDetailsException(id)
        logger.debug("fetchVideoLinks($id, $seasonId) called. ${cache.get(id)?.title}")
        return if (isSerial) {
            val seasons = mutableListOf<Season>()
            if (seasonId == null) {
                getSerialSeasonIds(id).forEach { seasons.add(Season(emptyList(), it.id, it.title)) }
            } else {
                val entry = getSerialSeasonIds(id).first { it.id == seasonId }
                seasons.add(getSeason(entry))
            }
            VideoLinksResponse(seasons = seasons)
        } else {
            val rsp = client.get {
                url("https", ENDPOINT, path = "/public/series/")
                parameter("id", id)
                parameter("movie", "")
            }.bodyAsText()
            VideoLinksResponse(files = mapper.parseMovie(rsp))
        }
    }

    private suspend fun getSerialSeasonIds(id: Long): List<HlamerMapper.SeasonEntry> {
        val rsp = client.get {
            url("https", ENDPOINT, path = "/public/series/categories/")
            parameter("id", id)
        }
        val text = rsp.bodyAsText()
        return mapper.categoryRspToSeasonInfo(text)
    }

    private suspend fun getSeason(entry: HlamerMapper.SeasonEntry): Season {
        val rsp2 = client.get {
            url("https", ENDPOINT, path = "/public/series/category/")
            parameter("id", entry.id)
        }.bodyAsText()
        return mapper.parseSeason(rsp2, entry)
    }

    companion object {
        private const val ENDPOINT = "hlamer.ru"
    }
}