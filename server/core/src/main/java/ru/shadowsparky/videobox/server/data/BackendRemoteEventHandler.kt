package ru.shadowsparky.videobox.server.data

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.koin.core.annotation.Factory
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEvent
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventHandler

@Factory
class BackendRemoteEventHandler(
    private val json: Json,
    private val sessionCache: SessionCache
) : RemoteEventHandler {

    suspend fun put(userId: Long, socketSession: SessionCache.Writer) {
        sessionCache.put(userId, socketSession)
    }

    suspend fun remove(userId: Long, socketSession: SessionCache.Writer) {
        sessionCache.remove(userId, socketSession)
    }

    override suspend fun notify(eventInfo: RemoteEvent) {
        val json = json.encodeToString(eventInfo)
        sessionCache.get(eventInfo.userId)?.forEach {
            try {
                it.writeText(json)
            } catch (e: Exception) {
                sessionCache.remove(eventInfo.userId, it)
            }
        }
    }
}
