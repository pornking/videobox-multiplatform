package ru.shadowsparky.videobox.server.data.http

import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideosResponse

class HtmlParseVideoApi(
    private val wrapper: VideoApi,
    private val dispatcherProvider: DispatcherProvider
) : VideoApi by wrapper {

    override suspend fun fetchNewVideos(search: String?, page: Int, sort: String): VideosResponse {
        val rsp = wrapper.fetchNewVideos(search, page, sort)
        val newItems = rsp.items.map {
            it.copy(
                title = it.title.parseHtml(),
                description = it.description?.parseHtml()
            )
        }
        return rsp.copy(items = newItems)
    }

    override suspend fun fetchDetails(id: Long): VideoDetails {
        val details = wrapper.fetchDetails(id)
        return details.copy(
            desc = details.desc.parseHtml(),
            title = details.title.parseHtml()
        )
    }

    private suspend fun String.parseHtml(): String = withContext(dispatcherProvider.io) {
        Jsoup.parse(this@parseHtml).wholeText()
    }
}
