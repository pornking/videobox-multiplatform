package ru.shadowsparky.videobox.server.presentation.routing

import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.post
import io.ktor.server.websocket.webSocket
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoveRequest
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.server.factory.SavedMovieRepositoryFactory
import ru.shadowsparky.videobox.server.presentation.getUserId
import ru.shadowsparky.videobox.server.presentation.obtainUserId

@OptIn(DelicateCoroutinesApi::class)
fun Route.setupSavedMovie(
    savedMovieFactory: SavedMovieRepositoryFactory,
    json: Json
) {
    post(SavedMovieRepository.REMOVE) {
        val saveRequest = call.receive<RemoveRequest>()
        savedMovieFactory.create(call.obtainUserId()).remove(saveRequest.movieId)
        call.respond(HttpStatusCode.OK)
    }
    post(SavedMovieRepository.WRITE) {
        val saveRequest = call.receive<VideoDetails>()
        savedMovieFactory.create(call.obtainUserId()).save(saveRequest)
        call.respond(HttpStatusCode.OK)
    }
    webSocket(SavedMovieRepository.QUERY) {
        val listenJob = SupervisorJob()
        try {
            val repo = savedMovieFactory.create(getUserId())
            val movieId = (incoming.receive() as Frame.Text).readText().toLongOrNull()
            launch(listenJob) {
                if (movieId == null) {
                    repo.getAll().collect {
                        outgoing.send(Frame.Text(json.encodeToString(it)))
                    }
                } else {
                    repo.isSaved(movieId).collect {
                        outgoing.send(Frame.Text(it.toString()))
                    }
                }
            }
            while (!outgoing.isClosedForSend) {
                delay(100)
            }
        } finally {
            listenJob.cancel()
        }
    }
}
