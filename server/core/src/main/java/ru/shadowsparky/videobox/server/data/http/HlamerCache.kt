package ru.shadowsparky.videobox.server.data.http

import kotlinx.coroutines.withContext
import org.koin.core.annotation.Single
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoItem
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.File
import java.io.InputStream

@Single
class HlamerCache(private val dispatcherProvider: DispatcherProvider) {

    suspend fun put(id: Long, item: VideoItem): Unit = withContext(dispatcherProvider.io) {
        val file = File(getDir(), id.toString())
        if (file.exists()) {
            file.delete()
        }
        file.writeBytes(item.serialize())
    }

    suspend fun get(id: Long): VideoItem? = withContext(dispatcherProvider.io) {
        val file = File(getDir(), id.toString())
        if (!file.exists()) {
            return@withContext null
        }
        val data = file.readBytes()
        read(id, ByteArrayInputStream(data))
    }

    private fun VideoItem.serialize(): ByteArray {
        val out = ByteArrayOutputStream()
        DataOutputStream(out).use {
            it.writeUTF(poster)
            it.writeUTF(title)
            it.writeUTF(description ?: "")
            it.writeBoolean(isSerial)
        }
        return out.toByteArray()
    }

    private fun read(id: Long, stream: InputStream): VideoItem {
        DataInputStream(stream).use {
            val poster = it.readUTF()
            val title = it.readUTF()
            val description = it.readUTF()
            val isSerial = it.readBoolean()
            return VideoItem(id, poster, title, description, isSerial)
        }
    }

    private fun getDir(): File {
        val path = File("cache").apply { mkdirs() }
        return File(path, "hlamer").apply { mkdirs() }
    }
}
