package ru.shadowsparky.videobox.server.presentation.routing

import io.ktor.server.application.call
import io.ktor.server.auth.authenticate
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Routing
import io.ktor.server.routing.post
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginInfo
import ru.shadowsparky.videobox.server.di.AuthEntryPoint
import ru.shadowsparky.videobox.server.di.RoutingEntryPoint
import ru.shadowsparky.videobox.server.presentation.AUTH_JWT_NAME

fun Routing.setupAuthMethods(
    routingEntryPoint: RoutingEntryPoint,
    authEntryPoint: AuthEntryPoint
) {
    val tokenRepository = authEntryPoint.authTokenRepository
    post(AuthTokenRepository.REGISTER_PATH) {
        call.respond(tokenRepository.register(call.receive<LoginInfo>()))
    }
    post(AuthTokenRepository.LOGIN_PATH) {
        call.respond(tokenRepository.login(call.receive<LoginInfo>()))
    }
    authenticate(AUTH_JWT_NAME) {
        setupSearch(routingEntryPoint.searchFactory)
        setupRecentlyWatched(routingEntryPoint.recentlyFactory)
        setupSavedMovie(routingEntryPoint.savedMovieFactory, routingEntryPoint.json)
    }
}
