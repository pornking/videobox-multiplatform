package ru.shadowsparky.videobox.server.di

import kotlinx.serialization.json.Json
import org.koin.core.annotation.Named
import org.koin.core.annotation.Single
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.EventType
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.server.data.BackendRemoteEventHandler
import ru.shadowsparky.videobox.server.domain.JwtInfoProvider
import ru.shadowsparky.videobox.server.domain.TokenVerifier
import ru.shadowsparky.videobox.server.factory.RecentlyWatchedRepositoryFactory
import ru.shadowsparky.videobox.server.factory.SavedMovieRepositoryFactory
import ru.shadowsparky.videobox.server.factory.SearchRepositoryFactory

@Single
class RoutingEntryPoint(
    val videoApi: VideoApi,
    val searchFactory: SearchRepositoryFactory,
    val recentlyFactory: RecentlyWatchedRepositoryFactory,
    val savedMovieFactory: SavedMovieRepositoryFactory,
    val json: Json
)

@Single
class WebSocketEntryPoint(
    @Named(EventType.SEARCH) val searchEventHandler: BackendRemoteEventHandler,
    @Named(EventType.RECENT) val recentlyEventHandler: BackendRemoteEventHandler
)

@Single
class AuthEntryPoint(
    val jwtInfoProvider: JwtInfoProvider,
    val tokenVerifier: TokenVerifier,
    val authTokenRepository: AuthTokenRepository
)
