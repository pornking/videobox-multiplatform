package ru.shadowsparky.videobox.server.presentation

import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.install
import io.ktor.server.plugins.statuspages.StatusPages
import io.ktor.server.response.respond
import io.ktor.server.routing.routing
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ru.shadowsparky.videobox.multiplatform.shared.domain.HttpException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.ServerExceptionInfo
import ru.shadowsparky.videobox.server.di.AuthEntryPoint
import ru.shadowsparky.videobox.server.di.RoutingEntryPoint
import ru.shadowsparky.videobox.server.presentation.routing.setupAuthMethods
import ru.shadowsparky.videobox.server.presentation.routing.setupBaseMethods

val routingLogger: Logger = LoggerFactory.getLogger("routing")

fun Application.configureRouting(
    routingEntryPoint: RoutingEntryPoint,
    authEntryPoint: AuthEntryPoint
) {
    install(StatusPages) {
        exception<HttpException> { call, cause ->
            routingLogger.error("http exception occurred. returns ${cause.code}", cause)
            call.respond(
                status = HttpStatusCode.fromValue(cause.code),
                message = ServerExceptionInfo(cause.message ?: "Неизвестная ошибка")
            )
        }
        exception<Throwable> { call, cause ->
            routingLogger.error("error occurred. returns 500...", cause)
            call.respond(
                status = HttpStatusCode.InternalServerError,
                message = ServerExceptionInfo(cause.message ?: "Неизвестная ошибка")
            )
        }
    }
    routing {
        setupBaseMethods(routingEntryPoint.videoApi)
        setupAuthMethods(routingEntryPoint, authEntryPoint)
    }
}
