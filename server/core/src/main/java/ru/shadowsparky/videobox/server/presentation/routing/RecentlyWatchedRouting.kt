package ru.shadowsparky.videobox.server.presentation.routing

import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRequest
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedResponse
import ru.shadowsparky.videobox.server.factory.RecentlyWatchedRepositoryFactory
import ru.shadowsparky.videobox.server.presentation.obtainUserId

fun Route.setupRecentlyWatched(recentlyFactory: RecentlyWatchedRepositoryFactory) {
    get(RecentlyWatchedRepository.GET_INFO) {
        val p = call.parameters
        val recentlyWatchedId = p[RecentlyWatchedRepository.MOVIE_ID_ARG]?.toLong()
        val seasonId = p[RecentlyWatchedRepository.SEASON_ID_ARG]?.toLong()
        val repo = recentlyFactory.create(call.obtainUserId())
        val list = if (recentlyWatchedId != null) {
            repo.queryRecentlyWatched(recentlyWatchedId, seasonId)
        }  else {
            repo.getAllRecentlyWatched()
        }
        call.respond(RecentlyWatchedResponse(list))
    }
    post(RecentlyWatchedRepository.WRITE) {
        val info = call.receive<RecentlyWatchedRequest>()
        recentlyFactory.create(call.obtainUserId()).writeAll(info.info)
        call.respond(HttpStatusCode.OK)
    }
    post(RecentlyWatchedRepository.REMOVE) {
        val repo = recentlyFactory.create(call.obtainUserId())
        call.receive<RecentlyWatchedRequest>().info.forEach {
            repo.removeInfo(it)
        }
        call.respond(HttpStatusCode.OK)
    }
    post(RecentlyWatchedRepository.TOGGLE) {
        val repo = recentlyFactory.create(call.obtainUserId())
        call.receive<RecentlyWatchedRequest>().info.forEach {
            repo.toggleInfo(it)
        }
        call.respond(HttpStatusCode.OK)
    }
}
