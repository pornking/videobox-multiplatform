package ru.shadowsparky.videobox.server.presentation

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.ApplicationCall
import io.ktor.server.application.install
import io.ktor.server.auth.Authentication
import io.ktor.server.auth.jwt.JWTPrincipal
import io.ktor.server.auth.jwt.jwt
import io.ktor.server.response.respond
import ru.shadowsparky.videobox.multiplatform.shared.domain.USER_ID_ARG
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.ServerExceptionInfo
import ru.shadowsparky.videobox.server.di.AuthEntryPoint
import ru.shadowsparky.videobox.server.domain.INVALID_TOKEN_MSG
import ru.shadowsparky.videobox.server.domain.LOGIN_NAME
import ru.shadowsparky.videobox.server.domain.VerifyTokenException

const val AUTH_JWT_NAME = "auth-jwt"

fun Application.configureJwt(authEntryPoint: AuthEntryPoint) {
    install(Authentication) {
        jwt(AUTH_JWT_NAME) {
            val jwtInfo = authEntryPoint.jwtInfoProvider.getJwtInfo()
            realm = jwtInfo.realm
            verifier(JWT.require(Algorithm.HMAC512(jwtInfo.secret))
                .withAudience(jwtInfo.audience)
                .withIssuer(jwtInfo.issuer)
                .build()
            )
            validate { credential ->
                val tokenRepo = authEntryPoint.tokenVerifier
                val login = credential.payload.getClaim(LOGIN_NAME).asString()
                try {
                    if (login != null) {
                        tokenRepo.verify(login)
                        JWTPrincipal(credential.payload)
                    } else {
                        null
                    }
                } catch (e: VerifyTokenException) {
                    routingLogger.error("verify token failed", e)
                    null
                }
            }
            challenge { defaultScheme, realm ->
                call.respond(
                    HttpStatusCode.Unauthorized,
                    ServerExceptionInfo(INVALID_TOKEN_MSG)
                )
            }
        }
    }
}

private fun String?.obtainToken(): DecodedJWT {
    val token = this?.removePrefix("Bearer ")
        ?: throw VerifyTokenException(INVALID_TOKEN_MSG)
    return JWT.decode(token)
}

fun String.obtainUserId(): Long {
    return obtainToken()
        .getClaim(USER_ID_ARG)
        .asLong()
}

fun ApplicationCall.obtainToken(): DecodedJWT {
    return request.headers["Authorization"].obtainToken()
}

fun ApplicationCall.obtainUserId(): Long {
    return obtainToken()
        .getClaim(USER_ID_ARG)
        .asLong()
}
