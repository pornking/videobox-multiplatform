package ru.shadowsparky.videobox.server.data.http

import kotlinx.coroutines.delay
import org.slf4j.LoggerFactory
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideosResponse

// Если по какой то причине не дошли до оригинального сервера пробуем еще раз.
// в случае если так и не смогли то и фиг с ним. Отдаем клиенту эту ошибку, пусть
// делает с ней что захочет
class FallbackVideoApi(
    private val wrapper: VideoApi
) : VideoApi {
    private val logger = LoggerFactory.getLogger("RepeatVideoApi")

    override suspend fun fetchNewVideos(search: String?, page: Int, sort: String): VideosResponse {
        return exec { wrapper.fetchNewVideos(search, page, sort) }
    }

    override suspend fun fetchDetails(id: Long): VideoDetails {
        return exec { wrapper.fetchDetails(id) }
    }

    override suspend fun fetchVideoLinks(id: Long, seasonId: Long?): VideoLinksResponse {
        return exec { wrapper.fetchVideoLinks(id, seasonId) }
    }

    private suspend fun <T> exec(apiCallback: suspend () -> T): T {
        var lastException: Exception? = null
        repeat(3) {
            try {
                return apiCallback.invoke()
            } catch (e: Exception) {
                lastException = e
                logger.error("Error occurred! Try count $it...")
                delay(1_000)
            }
        }
        lastException?.let { throw it }
        throw IllegalStateException("Unable to go server. Unknown reason. Impossible?")
    }
}
