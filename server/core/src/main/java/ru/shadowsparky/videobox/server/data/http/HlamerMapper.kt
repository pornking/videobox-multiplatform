package ru.shadowsparky.videobox.server.data.http

import org.koin.core.annotation.Factory
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.Episode
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.File
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.Season
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoItem

@Factory
class HlamerMapper(private val resourceProvider: ResourceProvider) {
    private val readerFactory: () -> XmlReader = { XmlReader() }

    fun movieRspToItems(rsp: String, rootTag: String): List<VideoItem> {
        val reader = readerFactory().apply { setInput(rsp) }
        return readRoot(reader, rootTag).toItems()
    }

    fun categoryRspToSeasonInfo(rsp: String): List<SeasonEntry> {
        val reader = readerFactory().apply { setInput(rsp) }
        val movies = readRoot(reader, "category").unit
        return movies.map { SeasonEntry(it.title!!, it.id.toLong()) }
    }

    fun parseSeason(rsp: String, seasonEntry: SeasonEntry): Season {
        val reader = readerFactory().apply { setInput(rsp) }
        val videos = readRoot(reader, "video").unit
        val episodes = mutableListOf<Episode>()
        videos.forEachIndexed { index, hlamerUnit ->
            episodes.add(hlamerUnit.toEpisode(index + 1))
        }
        return Season(episodes, seasonEntry.id, seasonEntry.title)
    }

    fun parseMovie(rsp: String): List<File> {
        val reader = readerFactory().apply { setInput(rsp) }
        val files = readRoot(reader, "video").unit.first()
        return listOf(File(false, -1, files.file!!))
    }

    private fun HlamerUnit.toEpisode(epNumber: Int): Episode {
        val hasTitle = title?.trim()?.isNotEmpty() == true
        val splitTitle = title?.split("&quot;")
            ?.lastOrNull { it.isNotEmpty() }
            ?: title?.removeSurrounding("&quot;")
        val episodeText = resourceProvider.getString(ApplicationString.EPISODE)
        return Episode(
            epNumber,
            listOf(File(false, -1, file!!)),
            if (hasTitle) "$epNumber. $splitTitle" else "$episodeText $epNumber"
        )
    }

    private fun readRoot(reader: XmlReader, rootTag: String): Movie {
        reader.require(XmlReader.Tag.START, rootTag)
        val entries = mutableListOf<HlamerUnit>()
        while (reader.next != XmlReader.Tag.END) {
            if (reader.eventType != XmlReader.Tag.START) {
                continue
            }
            if (reader.name == "unit") {
                val entry = readUnitEntry(reader)
                entries.add(entry)
            }
        }
        return Movie(entries)
    }

    private fun readUnitEntry(reader: XmlReader): HlamerUnit {
        reader.require(XmlReader.Tag.START, "unit")
        val id = reader.getAttributeValue("id")
        var title: String? = null
        var description: String? = null
        var thumb: String? = null
        var section: String? = null
        var file: String? = null
        var image: String? = null
        while (reader.next != XmlReader.Tag.END) {
            if (reader.eventType != XmlReader.Tag.START) {
                continue
            }
            when (reader.name) {
                "title" -> title = readText(reader)
                "description" -> description = readText(reader)
                "thumb" -> thumb = readText(reader)
                "section" -> section = readText(reader)
                "file" -> file = readText(reader)
                "image" -> image = readText(reader)
                else -> readText(reader) // skip
            }
        }
        return HlamerUnit(id, title, description, thumb, section, file, image)
    }

    private fun readText(reader: XmlReader): String {
        var result = ""
        if (reader.next == XmlReader.Tag.TEXT) {
            result = reader.text
            reader.nextTag()
        }
        return result
    }

    private fun Movie.toItems(): List<VideoItem> {
        return unit.map { it.toVideoItem() }
    }

    private fun HlamerUnit.toVideoItem(): VideoItem {
        return VideoItem(
            id = id.toLong(),
            title = title ?: "",
            poster = thumb ?: "",
            description = description,
            isSerial = section == "series"
        )
    }

    data class SeasonEntry(
        val title: String,
        val id: Long
    )

    private data class Movie(
        val unit: List<HlamerUnit>
    )

    private data class HlamerUnit(
        val id: String,
        val title: String?,
        val description: String?,
        val thumb: String?,
        val section: String?,
        val file: String?,
        val image: String?
    )
}