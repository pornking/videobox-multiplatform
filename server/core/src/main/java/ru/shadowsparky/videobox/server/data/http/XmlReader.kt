package ru.shadowsparky.videobox.server.data.http

import org.kxml2.io.KXmlParser
import org.xmlpull.v1.XmlPullParser
import java.io.StringReader

class XmlReader {
    private var parser: XmlPullParser? = null

    val name: String
        get() = parser!!.name
    val eventType: Tag
        get() = parser!!.eventType.fromType()
    val next: Tag
        get() = parser!!.next().fromType()

    fun setInput(raw: String) {
        parser = KXmlParser().apply {
            setInput(StringReader(raw))
            nextTag()
        }
    }

    fun require(
        tag: Tag,
        name: String
    ) {
        parser?.require(tag.toType(), null, name)
    }

    fun Tag.toType(): Int {
        return when (this) {
            Tag.START -> XmlPullParser.START_TAG
            Tag.END -> XmlPullParser.END_TAG
            Tag.TEXT -> XmlPullParser.TEXT
        }
    }

    fun Int.fromType(): Tag {
        return when (this) {
            XmlPullParser.START_TAG -> Tag.START
            XmlPullParser.END_TAG -> Tag.END
            else -> Tag.TEXT
        }
    }

    fun getAttributeValue(name: String): String {
        return parser!!.getAttributeValue(null, name)
    }

    fun nextTag() {
        parser!!.nextTag()
    }

    enum class Tag {
        START, END, TEXT
    }

    val text: String
        get() = parser!!.text
}