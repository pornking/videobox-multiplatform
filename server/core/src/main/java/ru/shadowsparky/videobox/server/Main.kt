package ru.shadowsparky.videobox.server

import io.ktor.server.application.Application
import io.ktor.server.application.install
import io.ktor.server.plugins.cors.routing.CORS
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.context.startKoin
import org.koin.ksp.generated.module
import ru.shadowsparky.videobox.multiplatform.shared.di.newCommonModulesList
import ru.shadowsparky.videobox.server.presentation.configureJwt
import ru.shadowsparky.videobox.server.presentation.configureRouting
import ru.shadowsparky.videobox.server.presentation.configureSerialization
import ru.shadowsparky.videobox.server.presentation.configureWebSocket

val backendModules = newCommonModulesList(true) + BackendModule().module

fun main(args: Array<String>) {
    startKoin { modules(backendModules) }
    io.ktor.server.netty.EngineMain.main(args)
}

fun Application.module() {
    val koin = object : KoinComponent {}
    install(CORS) { anyHost() }
    configureSerialization()
    configureJwt(koin.get())
    configureWebSocket(koin.get())
    configureRouting(koin.get(), koin.get())
}
