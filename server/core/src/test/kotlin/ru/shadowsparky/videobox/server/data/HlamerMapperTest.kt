package ru.shadowsparky.videobox.server.data
import org.junit.Test
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.server.data.http.HlamerMapper

class HlamerMapperTest {
    private val impl = HlamerMapper(ResourceProvider())

    @Test
    fun seriesParse() {
        val r = impl.movieRspToItems(seriesRsp, "series")
        assert(r.size == 2) { r.size }
    }

    @Test
    fun videosParse() {
        val r = impl.parseSeason(videosRsp, HlamerMapper.SeasonEntry("Lala", 1))
        assert(r.episodes.size == 4) {
            r.episodes
        }
    }
}
