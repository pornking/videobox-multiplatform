package ru.shadowsparky.videobox.server.data

val seriesRsp = """
    <?xml version='1.0' encoding='utf-8'?>
    <series>
    <unit id='732'>
        <title>Короли побега / Breakout Kings</title>
        <description> к приобретению своей собственной свободы.</description>
        <thumb>https://image.krasview.ru/channel/732/afeea8b229910c7_180.jpg</thumb>
        <section>series</section>
    </unit>
    <unit id='4742'>
        <title>Мастер побега (Сухим из воды) / The Escape Artist</title>
        <description>Уилл Бертон — талантливый младший адвокат, обладающий несравненным интеллектом и невероятным обаянием. Он специализируется на делах по тяжким уголовным преступлениям. Как адвокат, он пользуется большим спросом, так как не проиграл ни одного дела. Но он оправдывает главного подозреваемого в деле об ужасном убийстве и это приводит к неожиданным пугающим последствиям.</description>
        <thumb>https://image.krasview.ru/channel/4742/d2df6f9a7e67fc46_180.jpg</thumb>
        <section>series</section>
    </unit>
    </series>
""".trimIndent()

val videosRsp = """
    <?xml version='1.0' encoding='utf-8'?>
    <video>
    <unit id='186256'>
    	<title>Побег из тюрьмы / Prison Break - 1 сезон, 19 серия</title>
    	<file>https://m5.krasview.ru/video/59b9f3ceff19841/629963f8761e024.mp4</file>
    	<image>https://image.krasview.ru/video/59b9f3ceff19841/64.jpg</image>
    	<subtitle>1</subtitle>
    </unit>
    <unit id='186260'>
    	<title>Побег из тюрьмы / Prison Break - 1 сезон, 20 серия</title>
    	<file>https://m5.krasview.ru/video/cea9e7c5f62a6bc/aa0c870f9c9ef8d.mp4</file>
    	<image>https://image.krasview.ru/video/cea9e7c5f62a6bc/64.jpg</image>
    	<subtitle>1</subtitle>
    </unit>
    <unit id='186264'>
    	<title>Побег из тюрьмы / Prison Break - 1 сезон, 21 серия</title>
    	<file>https://m5.krasview.ru/video/9cde76fa0113abf/84b6e0e171a0132.mp4</file>
    	<image>https://image.krasview.ru/video/9cde76fa0113abf/64.jpg</image>
    	<subtitle>1</subtitle>
    </unit>
    <unit id='186270'>
    	<title>Побег из тюрьмы / Prison Break - 1 сезон, 22 серия</title>
    	<file>https://m5.krasview.ru/video/875236cb71395f6/be0b413191dbb1b.mp4</file>
    	<image>https://image.krasview.ru/video/875236cb71395f6/64.jpg</image>
    	<subtitle>1</subtitle>
    </unit>
    </video>
""".trimIndent()