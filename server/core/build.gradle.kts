plugins {
    alias(libs.plugins.vb.server)
    alias(libs.plugins.ktor)
    alias(libs.plugins.serialization)
}

dependencies {
    implementation(project(":common"))
    implementation(project(":server:common"))
    implementation(project(":server:db"))
    implementation(project(":server:jwt"))

    implementation(libs.ktor.server.core.jvm)
    implementation(libs.ktor.server.host.common.jvm)
    implementation(libs.ktor.server.status.pages.jvm)
    implementation(libs.ktor.server.content.negotiation.jvm)
    implementation(libs.ktor.serialization.kotlinx.json.jvm)
    implementation(libs.ktor.server.netty.jvm)
    implementation(libs.ktor.server.auth)
    implementation(libs.ktor.server.auth.jwt)
    implementation(libs.ktor.server.websockets.jvm)
    implementation(libs.ktor.server.cors)

    implementation(libs.androidx.datastore.preferences.core)
    implementation(libs.kxml2)
    implementation(libs.jsoup)
    testImplementation(libs.koin.test)
    testImplementation(libs.mockk)
    testImplementation(libs.junit)
}

java {
    sourceCompatibility = JavaVersion.toVersion(libs.versions.proj.java.get())
    targetCompatibility = JavaVersion.toVersion(libs.versions.proj.java.get())
}

group = "ru.shadowsparky.videobox"
version = "1.0"

val mainClassName = "ru.shadowsparky.videobox.server.MainKt"

application {
    mainClass.set(mainClassName)

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

tasks {
    register("fatJar", Jar::class) {
        mustRunAfter(jarTask("db"), jarTask("jwt"), jarTask("common"))

        archiveBaseName = "server-fat"
        manifest {
            attributes["Implementation-Title"] = "Videobox Server"
            attributes["Implementation-Version"] = version
            attributes["Main-Class"] = mainClassName
        }
        from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
        duplicatesStrategy = DuplicatesStrategy.WARN
        with(jar.get() as CopySpec)
    }

    "build" {
        dependsOn("fatJar")
    }

    register<Exec>("deployRemote") {
        dependsOn("build")
        commandLine(
            getCommandLine(
                "docker context use remote",
                "docker build . -t videobox-server",
                "docker context use default"
            )
        )
        notCompatibleWithConfigurationCache("This task uses Exec which is not compatible with configuration cache")
    }

    register<Exec>("deployLocal") {
        dependsOn("build")
        commandLine(
            getCommandLine(
                "docker context use default",
                "docker compose down",
                "docker compose up -d"
            )
        )
        notCompatibleWithConfigurationCache("This task uses Exec which is not compatible with configuration cache")
    }
}

fun jarTask(name: String): String {
    return ":server:$name:jar"
}

private fun getCommandLine(vararg commands: String): List<String> {
    val osName = System.getProperty("os.name").lowercase()
    return if (osName.contains("win")) {
        listOf("cmd", "/c") + commands.joinToString(" && ")
    } else {
        listOf("sh", "-c") + commands.joinToString(" && ")
    }
}
