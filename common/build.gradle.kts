plugins {
    alias(libs.plugins.vb.android.lib)
    alias(libs.plugins.vb.kmp)
    alias(libs.plugins.vb.ktor.client)
    alias(libs.plugins.vb.compose)
    alias(libs.plugins.vb.serialization)
}

kotlin {
    sourceSets {
        val commonMain by getting
        val desktopMain by getting {
            kotlin.srcDir("src/jvmMain/kotlin")
        }
        val androidMain by getting {
            kotlin.srcDir("src/jvmMain/kotlin")
        }
    }
}

android {
    namespace = "ru.shadowsparky.videobox.common"
}
