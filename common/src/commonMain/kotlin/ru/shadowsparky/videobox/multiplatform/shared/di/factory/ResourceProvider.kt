package ru.shadowsparky.videobox.multiplatform.shared.di.factory

class ResourceProvider {
    fun getString(resource: ApplicationString): String {
        return resource.raw
    }
}

enum class ApplicationString(val raw: String) {
    APP_NAME("Videobox"),
    RETRY("Повторить"),
    RECENT("Недавнее"),
    DETAILS_SERIAL("Все детали о сериале"),
    DETAILS_MOVIE("Все детали о фильме"),
    DESC("Описание"),
    WATCH("Смотреть"),
    SEASONS_OVERVIEW("Доступные сезоны"),
    SELECT_SEASON("Выберите сезон"),
    SEASON("Cезон"),
    EPISODES_OVERVIEW("Доступные серии"),
    CHOSE_EPISODE("Выберите серию"),
    EPISODE("Серия"),
    OKAY("Понятно"),
    ERROR_TITLE("Ошибка!"),
    UNKNOWN_ERROR("Неизвестная ошибка"),
    TIMEOUT_ERROR("Превышено время ожидания ответа от сервера"),
    SERVER_NOT_RESPONDING("Сервер не отвечает"),
    UNKNOWN_HOST("Нет доступа к серверу. Проверьте подключение к интернету"),
    CANT_OPEN_VIDEO_VIEWER("Не найдено приложение для проигрывания видео. Пожалуйста, поставьте видео плеер"),
    NOT_FOUND("Ничего не найдено по вашему запросу"),
    SHARE("Поделиться"),
    SETTINGS("Настройки"),
    SERVER_SETTINGS("Настройка сервера"),
    CUSTOM_SERV_ADDR("Адрес сервера"),
    EDIT_CUSTOM_SERV_ADDR("Изменение адреса сервера"),
    ACCOUNT("Аккаунт"),
    ACCOUNT_AUTH("Авторизация"),
    ACCOUNT_REG("Регистрация"),
    ACCOUNT_LOGOUT_AUTH("Выйти из аккаунта"),
    ACCOUNT_LOGIN("Логин"),
    ACCOUNT_PASSWORD("Пароль"),
    ACCOUNT_REG_HINT("У меня нет аккаунта"),
    EMPTY_LOGIN("Логин не может быть пустым"),
    EMPTY_PASS("Пароль не может быть пустым"),
    EDIT_THEME("Изменение темы приложения"),
    LIGHT_THEME("Светлая тема"),
    DARK_THEME("Тёмная тема"),
    AUTO_THEME("Как в системе"),
    THEME_CURRENTLY_ACTIVE("Тема активна на данный момент"),
    SAVED_MOVIES("Сохранённые"),
    SAVED_MOVIES_NOT_FOUND("Вы ничего не сохраняли. Может исправите это недоразумение?"),
    URL_COPIED("Ссылка скопирована"),
    COPY("Скопировать")
}
