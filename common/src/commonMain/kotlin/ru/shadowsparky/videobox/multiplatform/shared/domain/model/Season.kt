package ru.shadowsparky.videobox.multiplatform.shared.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Season(
    val episodes: List<Episode>,
    val id: Long,
    val title: String
)
