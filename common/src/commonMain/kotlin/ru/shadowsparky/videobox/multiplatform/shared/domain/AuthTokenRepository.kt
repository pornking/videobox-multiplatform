package ru.shadowsparky.videobox.multiplatform.shared.domain

import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.TokenResponse

interface AuthTokenRepository {
    suspend fun register(loginInfo: LoginInfo): TokenResponse
    suspend fun login(loginInfo: LoginInfo): TokenResponse

    companion object {
        const val LOGIN_PATH = "$DYNAMIC_PREFIX/login"
        const val REGISTER_PATH = "$DYNAMIC_PREFIX/register"
    }
}

class AuthException(msg: String) : HttpException(401, msg)

const val ALREADY_REG_TEXT = "Пользователь уже зарегистрирован"
const val UNKNOWN_USER_TEXT = "Пользователь не найден или неверный пароль"
const val USER_BLOCKED = "Пользователь заблокирован"

const val FLAG_USER_BLOCKED = 1
