package ru.shadowsparky.videobox.multiplatform.shared.domain.model

import kotlinx.serialization.Serializable
import kotlin.random.Random

@Serializable
data class VideoLinksResponse(
    val name: String? = null,
    val seasons: List<Season>? = null,
    val files: List<File>? = null,
) {

    fun isSerial(): Boolean {
        return seasons != null
    }

    fun markWatched(watched: List<RecentlyWatchedInfo>): VideoLinksResponse {
        return toggle(watched) { true }
    }

    fun toggleWatched(watched: List<RecentlyWatchedInfo>): VideoLinksResponse {
        return toggle(watched) { !it.isWatched }
    }

    private fun toggle(
        watched: List<RecentlyWatchedInfo>,
        predicate: (Episode) -> Boolean
    ): VideoLinksResponse {
        val seasons = seasons?.toMutableList() ?: return this
        watched.forEach { rw ->
            val season = seasons.find { it.id == rw.season }
            if (season != null) {
                val episodes = season.episodes.toMutableList()
                val seasonIdx = seasons.indexOf(season)
                val rawEp = episodes.firstOrNull { it.episode.toLong() == rw.episode }
                if (rawEp != null) {
                    val epId = episodes.indexOf(rawEp)
                    val ep = rawEp.copy(isWatched = predicate.invoke(rawEp))
                    println("update episode $ep")
                    episodes[epId] = ep
                    seasons[seasonIdx] = season.copy(episodes = episodes)
                }
            }
        }
        return this.copy(seasons = seasons)
    }

    companion object {
        fun getStub(
            isSerial: Boolean,
            watched: Boolean = false,
            title: String = "Побег из психушки"
        ): VideoLinksResponse {
            return if (isSerial) {
                val seasons = mutableListOf<Season>()
                (1..10).forEach {
                    val r = mutableListOf<Episode>()
                    (1..Random.nextInt(2, 20)).forEach {
                        r.add(
                            Episode(
                            it,
                            listOf(File(true, 1080, "https://url/video.mp4")),
                            isWatched = watched,
                            title = title
                        )
                        )
                    }
                    seasons.add(Season(r, 123, "Lalala"))
                }
                VideoLinksResponse(title, seasons)
            } else {
                VideoLinksResponse(
                    title,
                    files = listOf(File(true, 1080, "https://url/video.mp4"))
                )
            }
        }
    }
}
