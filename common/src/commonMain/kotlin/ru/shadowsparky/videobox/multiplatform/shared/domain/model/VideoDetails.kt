package ru.shadowsparky.videobox.multiplatform.shared.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class VideoDetails(
    val id: Long,
    val poster: String,
    val desc: String,
    val title: String,
    val isSerial: Boolean
) {
    val isMovie = !isSerial

    val flags: Int
        get() {
            return if (isSerial) SERIAL_FLAG else MOVIE_FLAG
        }

    companion object {
        fun getStub(title: String, isSerial: Boolean = true): VideoDetails {
            return VideoDetails(
                id = 1234,
                poster = "https://poster.ru/img.png",
                title = title,
                desc = "Short Story Description!",
                isSerial = isSerial
            )
        }
    }
}

const val SERIAL_FLAG = 1
const val MOVIE_FLAG = 1 shl 1
