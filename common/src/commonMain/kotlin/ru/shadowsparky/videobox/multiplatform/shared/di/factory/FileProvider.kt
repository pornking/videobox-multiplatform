package ru.shadowsparky.videobox.multiplatform.shared.di.factory

expect class FileProvider() {
    fun getCacheFileDir(): String?
}