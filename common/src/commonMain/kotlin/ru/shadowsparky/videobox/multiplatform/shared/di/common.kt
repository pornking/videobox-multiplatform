package ru.shadowsparky.videobox.multiplatform.shared.di

import kotlinx.serialization.json.Json
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.BearerTokenHolder
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.FileProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.HttpClientFactory
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider

const val IS_SERVER = "is_server"

val commonModule = module {
    single { object : DispatcherProvider {} } bind DispatcherProvider::class
    single { ResourceProvider() } bind ResourceProvider::class
    single { FileProvider() }
    single { Json { ignoreUnknownKeys = true } }
}

val httpClientModule = module {
    single {
        val isClient = !get<Boolean>(named(IS_SERVER))
        BearerTokenHolder(get(), isClient)
    }
    factory {
        HttpClientFactory(get()).create()
    }
}

fun newCommonModulesList(isServer: Boolean) = listOf(
    commonModule,
    httpClientModule,
    module {
        single(named(IS_SERVER)) { isServer }
    }
)
