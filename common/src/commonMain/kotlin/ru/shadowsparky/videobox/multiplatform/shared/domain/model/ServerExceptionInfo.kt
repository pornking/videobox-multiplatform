package ru.shadowsparky.videobox.multiplatform.shared.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class ServerExceptionInfo(val msg: String)

class ServerException(val httpCode: Int, override val message: String?) : IllegalStateException()
