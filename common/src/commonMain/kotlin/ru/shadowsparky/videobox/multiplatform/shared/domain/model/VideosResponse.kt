package ru.shadowsparky.videobox.multiplatform.shared.domain.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class VideosResponse(
    @SerialName("has_next_page")
    val hasNextPage: Boolean,
    val items: List<VideoItem>,
    val page: Int
)
