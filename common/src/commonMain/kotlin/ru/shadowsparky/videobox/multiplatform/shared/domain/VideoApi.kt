package ru.shadowsparky.videobox.multiplatform.shared.domain

import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideosResponse

abstract class HttpException(val code: Int, override val message: String?) : IllegalStateException()
class UnableToFetchDetailsException(val id: Long) : HttpException(404, "Unable to fetch details for this id ($id). It not cached yet.")

const val DYNAMIC_PREFIX = "dynamic"

interface VideoApi {

    suspend fun fetchNewVideos(
        search: String? = null,
        page: Int = 1,
        sort: String = "date"
    ): VideosResponse

    suspend fun fetchDetails(id: Long): VideoDetails

    suspend fun fetchVideoLinks(id: Long, seasonId: Long?): VideoLinksResponse

    companion object {
        const val SEARCH_ARG = "search"
        const val PAGE_ARG = "page"
        const val SORT_ARG = "sort"
        const val ID_ARG = "id"
        const val SEASON_ID_ARG = "season_id"

        const val VIDEOS_PATH = "videos"
        const val DETAILS_PATH = "details"
        const val LINKS_PATH = "links"
    }
}
