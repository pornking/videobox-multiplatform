package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.plugins.HttpRedirect
import io.ktor.client.plugins.auth.Auth
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.websocket.WebSockets
import io.ktor.serialization.kotlinx.json.json
import ru.shadowsparky.videobox.multiplatform.shared.data.BearerTokenHolder

expect class HttpClientFactory(tokenHolder: BearerTokenHolder) {
    fun create(): HttpClient
}

fun HttpClientConfig<*>.setup(tokenHolder: BearerTokenHolder) {
    install(Auth) { tokenHolder.init(this) }
    install(ContentNegotiation) { json() }
    install(WebSockets)
    install(HttpRedirect) { checkHttpMethod = false }
}
