package ru.shadowsparky.videobox.multiplatform.shared.domain

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.TokenResponse

interface AuthTokenCache {
    val responseFlow: Flow<TokenResponse?>

    suspend fun put(token: TokenResponse)
    suspend fun remove()
}

suspend fun AuthTokenCache.hasAccount(): Boolean {
    responseFlow.first() ?: return false
    return true
}
