package ru.shadowsparky.videobox.multiplatform.shared.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class VideoItem(
    val id: Long,
    val poster: String,
    val title: String,
    val description: String? = null,
    val isSerial: Boolean = false
)
