package ru.shadowsparky.videobox.multiplatform.shared.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Episode(
    val episode: Int,
    val files: List<File>,
    val title: String? = null,
    val isWatched: Boolean = false
) {
    val url: String = files.first().url
}
