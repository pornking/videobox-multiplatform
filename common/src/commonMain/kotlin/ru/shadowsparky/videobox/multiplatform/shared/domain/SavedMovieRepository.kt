package ru.shadowsparky.videobox.multiplatform.shared.domain

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.serialization.Serializable
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails

interface SavedMovieRepository {
    suspend fun save(details: VideoDetails)
    suspend fun getAll(): Flow<List<VideoDetails>>
    suspend fun isSaved(id: Long): Flow<Boolean>
    suspend fun remove(id: Long)
    suspend fun saveAll(details: List<VideoDetails>) {
        details.forEach { save(it) }
    }
    suspend fun clear() {
        getAll().first().forEach { remove(it.id) }
    }

    companion object {
        private const val PREFIX = "$DYNAMIC_PREFIX/savedMovie"
        const val REMOVE = "${PREFIX}/delete"
        const val WRITE = "${PREFIX}/add"
        const val QUERY = "${PREFIX}/query"
    }
}

@Serializable
data class RemoveRequest(val movieId: Long)
