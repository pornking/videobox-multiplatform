package ru.shadowsparky.videobox.multiplatform.shared.data

import io.ktor.client.plugins.auth.Auth
import io.ktor.client.plugins.auth.providers.BearerAuthProvider
import io.ktor.client.plugins.auth.providers.BearerTokens
import kotlinx.coroutines.flow.first
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache

class BearerTokenHolder(
    private val authTokenCache: AuthTokenCache,
    private val needInstall: Boolean
) {

    private val tokenProvider = BearerAuthProvider(
        refreshTokens = { loadToken() },
        loadTokens = { loadToken() },
        realm = null
    )

    private suspend fun loadToken(): BearerTokens? {
        val token = authTokenCache.responseFlow.first() ?: return null
        return BearerTokens(token.token, "")
    }

    fun init(auth: Auth) {
        if (auth.providers.isEmpty() && needInstall) {
            auth.providers.add(tokenProvider)
        }
    }

    fun clear() {
        tokenProvider.clearToken()
    }
}
