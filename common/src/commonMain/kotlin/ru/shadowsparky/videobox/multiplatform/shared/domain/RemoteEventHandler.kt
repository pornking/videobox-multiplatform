package ru.shadowsparky.videobox.multiplatform.shared.domain

import kotlinx.coroutines.flow.Flow
import kotlinx.serialization.Serializable

interface RemoteEventHandler {
    suspend fun notify(eventInfo: RemoteEvent)

    companion object {
        const val SEARCH_CHANGED = "${SearchRepository.PREFIX}/onChange"
        const val RECENTLY_CHANGED = "${RecentlyWatchedRepository.PREFIX}/onChange"
    }
}

interface RemoteEventListener {
    val event: Flow<RemoteEvent>
}

@Serializable
sealed interface RemoteEvent {
    val userId: Long

    @Serializable
    data class OnSearch(val millis: Long, override val userId: Long) : RemoteEvent

    @Serializable
    data class OnRecent(val millis: Long, override val userId: Long, val movieId: Long) : RemoteEvent
}

object EventType {
    const val SEARCH = "search"
    const val RECENT = "recent"
}
