package ru.shadowsparky.videobox.multiplatform.shared.domain

import kotlinx.serialization.Serializable
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.RecentlyWatchedInfo

interface RecentlyWatchedRepository {
    suspend fun removeInfo(info: RecentlyWatchedInfo)
    suspend fun writeInfo(info: RecentlyWatchedInfo)

    suspend fun writeAll(info: List<RecentlyWatchedInfo>) {
        info.forEach { writeInfo(it) }
    }

    suspend fun toggleInfo(info: RecentlyWatchedInfo)

    suspend fun queryRecentlyWatched(movieId: Long, seasonId: Long? = null): List<RecentlyWatchedInfo>

    suspend fun getAllRecentlyWatched(): List<RecentlyWatchedInfo>

    suspend fun clear() {
        getAllRecentlyWatched().forEach { removeInfo(it) }
    }

    companion object {
        const val PREFIX = "$DYNAMIC_PREFIX/recent"

        const val REMOVE = "$PREFIX/delete"
        const val WRITE = "$PREFIX/add"
        const val TOGGLE = "$PREFIX/toggle"
        const val GET_INFO = "$PREFIX/query"

        const val MOVIE_ID_ARG = "movieId"
        const val SEASON_ID_ARG = "seasonId"
    }
}

@Serializable
data class RecentlyWatchedResponse(
    val info: List<RecentlyWatchedInfo>
)

@Serializable
data class RecentlyWatchedRequest(
    val info: List<RecentlyWatchedInfo>
)
