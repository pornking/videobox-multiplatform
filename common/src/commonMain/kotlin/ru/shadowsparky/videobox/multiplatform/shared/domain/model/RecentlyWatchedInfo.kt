package ru.shadowsparky.videobox.multiplatform.shared.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class RecentlyWatchedInfo(
    val id: Long,
    val movieId: Long,
    val episode: Long?,
    val season: Long?
)
