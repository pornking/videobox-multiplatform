package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import ru.shadowsparky.videobox.multiplatform.shared.data.BearerTokenHolder

actual class HttpClientFactory actual constructor(private val tokenHolder: BearerTokenHolder) {
    actual fun create(): HttpClient {
        return HttpClient(CIO) {
            setup(tokenHolder)
        }
    }
}