package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import io.ktor.client.HttpClient
import io.ktor.client.engine.darwin.Darwin
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache

actual class HttpClientFactory actual constructor(private val authTokenCache: AuthTokenCache) {
    actual fun create(): HttpClient {
        return HttpClient(Darwin) {
            setup(authTokenCache)
        }
    }
}