import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.compose.resources.ResourcesExtension

plugins {
    alias(libs.plugins.vb.kmp)
    alias(libs.plugins.vb.compose)
}

group = "ru.shadowsparky.videobox"
version = libs.versions.proj.version.name.get()

kotlin {
    sourceSets {
        val desktopMain by getting {
            kotlin.srcDirs("src/jvmMain/kotlin")
            dependencies {
                implementation(compose.components.resources)
                implementation(compose.desktop.currentOs)
                implementation(kotlin("test-junit"))
                implementation(project(":client:shared"))
            }
        }
    }
}

compose.resources {
    publicResClass = true
    generateResClass = ResourcesExtension.ResourceClassGeneration.Always
}

fun findIconFile(name: String): File {
    return File(projectDir, "src/jvmMain/composeResources/drawable/$name")
}

compose.desktop {
    application {
        mainClass = "ru.shadowsparky.videobox.MainKt"
        nativeDistributions {
            modules("java.sql")
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "Videobox"
            windows {
                iconFile.set(findIconFile("icon_win.ico"))
            }
            linux {
                iconFile.set(findIconFile("icon.png"))
            }
            macOS {
                bundleID = group.toString()
            }
        }
        buildTypes.release {
            proguard {
                isEnabled.set(false)

                obfuscate.set(false)
                optimize.set(false)
                configurationFiles.from(project.file("rules.pro"))
            }
        }
    }
}
