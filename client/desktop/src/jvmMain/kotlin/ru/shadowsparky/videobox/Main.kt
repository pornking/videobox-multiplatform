package ru.shadowsparky.videobox

import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.KeyEventType
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.type
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import com.arkivanov.decompose.DefaultComponentContext
import com.arkivanov.essenty.lifecycle.LifecycleRegistry
import com.arkivanov.essenty.lifecycle.destroy
import org.koin.core.component.get
import org.koin.core.context.startKoin
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.di.newKoinModules
import ru.shadowsparky.videobox.multiplatform.shared.presentation.Nav
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent

fun main() {
    startKoin {
        modules(newKoinModules(true))
    }
    application {
        val lifecycle = LifecycleRegistry()
        val root = koin.get<RootComponent.RootFactory>().create(DefaultComponentContext(lifecycle))
        val state = rememberWindowState()
        Window(
            state = state,
            onCloseRequest = {
                lifecycle.destroy()
                exitApplication()
            },
            onKeyEvent = {
                if (it.key == Key.Escape && it.type == KeyEventType.KeyUp) {
                    root.onBack()
                    true
                } else {
                    false
                }
            },
            title = koin.get<ResourceProvider>().getString(ApplicationString.APP_NAME)
        ) {
            Nav(root)
        }
    }
}

