
import java.io.FileInputStream
import java.util.Properties

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    alias(libs.plugins.vb.compose)
}

val keystorePropertiesFile = file("keystore.properties")
val keystoreProperties = Properties()
keystoreProperties.load(FileInputStream(keystorePropertiesFile))

android {
    signingConfigs {
        create("release") {
            storeFile = file("release.keystore")
            storePassword = keystoreProperties["RELEASE_STORE_PASSWORD"].toString()
            keyAlias = keystoreProperties["RELEASE_KEY_ALIAS"].toString()
            keyPassword = keystoreProperties["RELEASE_KEY_PASSWORD"].toString()
        }
    }
    compileSdk = libs.versions.proj.compile.get().toInt()
    defaultConfig {
        applicationId = "ru.shadowsparky.videobox.multiplatform"
        minSdk = libs.versions.proj.min.get().toInt()
        targetSdk = libs.versions.proj.target.get().toInt()
        versionCode = libs.versions.proj.version.code.get().toInt()
        versionName = libs.versions.proj.version.name.get()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            signingConfig = signingConfigs.getByName("release")
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                file("proguard-rules.pro")
            )
        }
        getByName("debug") {
            isMinifyEnabled = false
            signingConfig = signingConfigs.getByName("release")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.toVersion(libs.versions.proj.java.get().toInt())
        targetCompatibility = JavaVersion.toVersion(libs.versions.proj.java.get().toInt())
    }
    kotlinOptions {
        jvmTarget = libs.versions.proj.java.get()
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.compose.compiler.get()
    }
    namespace = "ru.shadowsparky.videobox"
}

kotlin {
    jvmToolchain(libs.versions.proj.java.get().toInt())
}

dependencies {
    implementation(libs.koin.android)
    implementation(libs.activity.compose)
    implementation(project(":client:shared"))
    debugImplementation(libs.ui.tooling)
    compileOnly(libs.ui.tooling.preview.android)
    testImplementation(libs.koin.test)
    testImplementation(libs.mockk)
    testImplementation(kotlin("test-junit"))
    androidTestImplementation(libs.junit)
    androidTestImplementation(libs.kotlin.reflect)
    androidTestImplementation(libs.runner)
}
