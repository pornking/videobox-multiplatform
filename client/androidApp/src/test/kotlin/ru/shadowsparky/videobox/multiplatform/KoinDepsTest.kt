package ru.shadowsparky.videobox.multiplatform

import android.app.Application
import io.mockk.mockk
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.test.check.checkKoinModules
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.di.newKoinModules
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route
import ru.shadowsparky.videobox.multiplatform.shared.presentation.provideFactory

class KoinDepsTest {
    private val fakeContext = mockk<Application>(relaxed = true)

    @Test
    fun verify() {
        checkKoinModules(newKoinModules(true), appDeclaration = {
            androidContext(fakeContext)
        })
    }

    @Test
    fun checkComponents() {
        startKoin {
            modules(newKoinModules(true))
            androidContext(fakeContext)
        }
        listOf(
            Route.Settings.Auth,
            Route.Settings.EditServ,
            Route.Settings.EditTheme,
            Route.Overview.Episode(1, 1),
            Route.Overview(1),
            Route.Settings.Register,
            Route.Saved,
            Route.SearchVideos,
            Route.Overview.Season(1),
            Route.Settings,
            Route.Videos(null)
        ).forEach { it.check() }
    }

    private fun Route.check() {
        provideFactory<Child, Route>(koin)

        when (this) {
            Route.Settings.Auth,
            Route.Settings.EditServ,
            Route.Settings.EditTheme,
            is Route.Overview.Episode,
            is Route.Overview,
            Route.Settings.Register,
            Route.Saved,
            Route.SearchVideos,
            is Route.Overview.Season,
            Route.Settings,
            is Route.Videos -> {}
        }
    }
}
