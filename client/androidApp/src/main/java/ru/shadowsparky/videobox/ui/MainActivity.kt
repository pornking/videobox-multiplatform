package ru.shadowsparky.videobox.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.SystemBarStyle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.lifecycle.lifecycleScope
import com.arkivanov.decompose.retainedComponent
import kotlinx.coroutines.launch
import org.koin.core.component.get
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.presentation.Nav
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.isDark
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route

class MainActivity : ComponentActivity() {
    private var root: RootComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        val overviewId = intent.data?.overviewId()
        root = retainedComponent { componentContext ->
            koin.get<RootComponent.RootFactory>().create(componentContext, overviewId)
        }
        if (openSavedIfNeeded(intent) || overviewId != null) {
            intent = Intent(intent)
                .setData(null)
                .setAction(null)
        }
        setContent {
            Nav(rootComponent = requireNotNull(root) { "impossible to null here?" })
            val theme by root!!.selectedTheme.collectAsState(null)
            val isDarkTheme = theme?.isDark() ?: false
            LaunchedEffect(theme) {
                if (theme != null) {
                    enableEdgeToEdge(
                        statusBarStyle = SystemBarStyle.auto(
                            android.graphics.Color.TRANSPARENT,
                            android.graphics.Color.TRANSPARENT,
                        ) { isDarkTheme },
                        navigationBarStyle = SystemBarStyle.auto(
                            lightScrim,
                            darkScrim,
                        ) { isDarkTheme }
                    )
                }
            }
        }
        lifecycleScope.launch {
            root?.onFinish?.collect {
                finishAndRemoveTask()
            }
        }
    }

    private fun openSavedIfNeeded(intent: Intent?): Boolean {
        if (intent?.action == "ru.shadowsparky.videobox.action.OPEN_SAVED_MOVIES") {
            root?.nav(Route.Saved, true)
            return true
        }
        return false
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        if (!openSavedIfNeeded(intent)) {
            val id = intent.data?.overviewId()
            if (id != null) {
                root?.nav(Route.Overview(id), true)
            }
        }
    }

    private fun Uri.overviewId(): Long? {
        return getQueryParameter("id")?.toLongOrNull()
    }
}

private val lightScrim = android.graphics.Color.argb(0xe6, 0xFF, 0xFF, 0xFF)
private val darkScrim = android.graphics.Color.argb(0x80, 0x1b, 0x1b, 0x1b)
