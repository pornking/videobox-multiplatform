package ru.shadowsparky.videobox.ui

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.bind
import org.koin.dsl.module
import ru.shadowsparky.videobox.BuildConfig
import ru.shadowsparky.videobox.multiplatform.shared.di.newKoinModules
import ru.shadowsparky.videobox.multiplatform.shared.presentation.AndroidUiConfiguration
import ru.shadowsparky.videobox.multiplatform.shared.presentation.UiConfiguration

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(newKoinModules(BuildConfig.DEBUG))
            allowOverride(true)
            modules(androidOverrideModule)
        }
    }
}

private val androidOverrideModule = module {
    factory { AndroidUiConfiguration() } bind UiConfiguration::class
}
