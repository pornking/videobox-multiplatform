package ru.shadowsparky.videobox.ui.preview

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.Season
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.season.SeasonsInfo

@Preview
@Composable
fun SeasonsInfoPreview() {
    SeasonsInfo(
        resourceProvider = ResourceProvider(),
        paddingValues = PaddingValues(0.dp),
        seasons = listOf(Season(listOf(), 23, "Lalalal"))
    ) {

    }
}
