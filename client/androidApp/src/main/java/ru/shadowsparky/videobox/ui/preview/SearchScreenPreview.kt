@file:OptIn(ExperimentalMaterial3Api::class)
package ru.shadowsparky.videobox.ui.preview

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.AppBarWithSearch
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.InternalAppBar
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.OldSearchInfo

@Preview
@Composable
fun AppBarWithSearchPreview() {
    AppBarWithSearch(
        title = "TitleWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW",
        onClick = {},
        isDefault = false,
        onBack = null
    )
}

@Preview
@Composable
fun AppBarWithSearchWithBackPreview() {
    AppBarWithSearch(
        title = "TitleWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW",
        onClick = {},
        onBack = {}
    )
}

@Preview
@Composable
fun InternalAppBarPreview() {
    InternalAppBar(
        null,
        TextFieldValue(""),
        {},
        {},
        {}
    )
}

@Preview(showBackground = true)
@Composable
fun SearchInfoPreview() {
    OldSearchInfo(
        info = listOf(
            "Почему дора дура?",
            "клиинка",
            "симпсоны",
            "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"
        ),
        ResourceProvider(),
        PaddingValues(0.dp),
        onClick = {},
        onDeleteRequested = {}
    )
}
