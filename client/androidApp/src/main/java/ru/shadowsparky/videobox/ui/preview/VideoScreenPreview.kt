package ru.shadowsparky.videobox.ui.preview

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoItem
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.ErrorAndRetry
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.Loading
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.VideoInfo

@Preview
@Composable
fun VideoInfoPreview() {
    val item = VideoItem(
        id = -1,
        title = "Title",
        poster = "http...",
    )
    VideoInfo(item, false) {

    }
}

@Preview
@Composable
fun ErrorPreview() {
    ErrorAndRetry(
        errorMsg = "Неизвестная ошибка",
        resourceProvider = ResourceProvider()
    ) {}
}

@Preview
@Composable
fun LoadingPreview() {
    Loading()
}
