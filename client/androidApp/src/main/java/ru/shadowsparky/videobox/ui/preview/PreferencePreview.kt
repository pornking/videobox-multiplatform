package ru.shadowsparky.videobox.ui.preview

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import ru.shadowsparky.videobox.multiplatform.shared.presentation.TextPreference

@Preview
@Composable
fun SwitchPreferencePreview() {
    TextPreference("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", "summary")
}
