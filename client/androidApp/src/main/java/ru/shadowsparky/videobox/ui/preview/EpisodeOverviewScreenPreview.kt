package ru.shadowsparky.videobox.ui.preview

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode.EpisodesOverviewList

@Preview
@Composable
fun EpisodeOverviewScreenPreview() {
    EpisodesOverviewList(
        null,
        VideoLinksResponse.getStub(isSerial = true, watched = true),
        ResourceProvider(),
        PaddingValues(0.dp),
        {},
        {},
        {}
    )
}

@Preview
@Composable
fun EpisodeOverviewScreenPreviewSomeLong() {
    EpisodesOverviewList(
        null,
        VideoLinksResponse.getStub(isSerial = true, watched = true, "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"),
        ResourceProvider(),
        PaddingValues(0.dp),
        {},
        {},
        {}
    )
}
