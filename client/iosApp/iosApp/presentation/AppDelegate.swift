import UIKit
import SwiftUI
import shared

@main
struct app_iosApp: App {
//    @UIApplicationDelegateAdaptor(AppDelegate.self)
//    var appDelegate: AppDelegate
    
    init() {
        IosKt.doInitKoin()
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private var stateKeeper = StateKeeperDispatcherKt.StateKeeperDispatcher(savedState: nil)

    lazy var root: RootComponent = RootComponent(
        componentContext: DefaultComponentContext(
            lifecycle: ApplicationLifecycle(),
            stateKeeper: stateKeeper,
            instanceKeeper: nil,
            backHandler: nil
        ),
        onFinish: {}
    )
}
