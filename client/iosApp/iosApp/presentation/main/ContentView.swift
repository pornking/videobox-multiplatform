import SwiftUI
import shared
import UIKit

struct ComposeView: UIViewControllerRepresentable {
    @UIApplicationDelegateAdaptor(AppDelegate.self)
    var appDelegate: AppDelegate

    func makeUIViewController(context: Context) -> UIViewController {
        IosKt.MainViewController(rootComponent: appDelegate.root)
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {}
}

struct ContentView: View {
    
    var body: some View {
        ComposeView()
                .ignoresSafeArea(.all, edges: .bottom) // Compose has own keyboard handler
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
