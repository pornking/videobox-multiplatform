plugins {
    alias(libs.plugins.vb.android.lib)
    alias(libs.plugins.vb.kmp)
    alias(libs.plugins.vb.ktor.client)
    alias(libs.plugins.vb.compose)
    alias(libs.plugins.vb.serialization)
    alias(libs.plugins.vb.sqldelight)
}

sqldelight {
    databases {
        create("AppDatabase") {
            packageName.set("ru.shadowsparky.videobox.multiplatform.app")
            generateAsync.set(true)
        }
    }
}

kotlin {
    sourceSets {
        commonMain.dependencies {
            api(project(":common"))
        }
        androidMain.dependencies {
            implementation(libs.androidx.material3)
        }
        jsMain.dependencies {
            implementation(libs.ktor.client.js)
            implementation(libs.web.worker.driver)

            implementation(devNpm("copy-webpack-plugin", "9.1.0"))
        }

        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val desktopMain by getting {
        }
    }
}

android {
    namespace = "ru.shadowsparky.videobox.multiplatform.shared"
    sourceSets["main"].resources.srcDirs("src/commonMain/resources")
}
