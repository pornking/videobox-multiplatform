package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.jdbc.sqlite.JdbcSqliteDriver
import ru.shadowsparky.videobox.multiplatform.app.AppDatabase

actual class DatabaseDriverFactory {
    private val fileProvider = FileProvider()

    actual suspend fun createDriver(): SqlDriver {
        val dir = fileProvider.getCacheFileDir() ?: ""
        val driver = JdbcSqliteDriver(JdbcSqliteDriver.IN_MEMORY + "$dir/videobox.db")
        try {
            AppDatabase.Schema.create(driver).await()
        } catch (e: Exception) {

        }
        return driver
    }
}
