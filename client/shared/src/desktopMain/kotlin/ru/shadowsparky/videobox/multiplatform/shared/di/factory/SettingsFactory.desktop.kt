package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import com.russhwolf.settings.PreferencesSettings
import com.russhwolf.settings.Settings
import java.util.prefs.Preferences

actual class SettingsFactory {
    actual fun create(): Settings {
        return PreferencesSettings(Preferences.userRoot())
    }
}