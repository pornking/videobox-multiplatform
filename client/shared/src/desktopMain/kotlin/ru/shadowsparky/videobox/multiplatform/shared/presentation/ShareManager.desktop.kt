package ru.shadowsparky.videobox.multiplatform.shared.presentation

import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails

actual class ShareManager actual constructor() {
    actual fun shareDetails(details: VideoDetails) = Unit
    actual fun canShare(): Boolean = false
}