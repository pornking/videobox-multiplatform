package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import com.seiko.imageloader.ImageLoader
import com.seiko.imageloader.component.setupDefaultComponents
import com.seiko.imageloader.intercept.bitmapMemoryCacheConfig
import okio.Path.Companion.toPath

actual class ImageLoaderProvider {
    private val fileProvider = FileProvider()

    actual fun provide(): ImageLoader {
        return ImageLoader {
            components {
                setupDefaultComponents()
            }
            interceptor {
                bitmapMemoryCacheConfig {
                    maxSize(32 * 1024 * 1024)
                }
                val cachePath = getCacheDir()
                if (cachePath != null) {
                    diskCacheConfig {
                        directory(cachePath.toPath().resolve("image_cache"))
                        maxSizeBytes(512L * 1024 * 1024) // 512MB
                    }
                }
            }
        }
    }
    private fun getCacheDir() = fileProvider.getCacheFileDir()
}
