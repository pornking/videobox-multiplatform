package ru.shadowsparky.videobox.multiplatform.shared.presentation

import java.awt.Desktop
import java.net.URI

actual open class VideoLauncher {
    actual open fun launch(uri: String) {
        Desktop.getDesktop().browse(URI(uri))
    }
}
