package ru.shadowsparky.videobox.multiplatform.shared.domain

actual class LoggerImpl {
    actual fun log(tag: String, msg: String) {
        println("$tag: $msg")
    }
    actual fun log(tag: String, e: Throwable, message: String?) {
        if (message != null) {
            println("$tag: $message")
        }
        e.printStackTrace()
    }
}
