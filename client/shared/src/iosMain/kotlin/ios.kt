import androidx.compose.ui.window.ComposeUIViewController
import org.koin.core.context.startKoin
import ru.shadowsparky.videobox.multiplatform.shared.di.newKoinModules
import ru.shadowsparky.videobox.multiplatform.shared.presentation.Nav
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent

fun initKoin() {
    startKoin {
        modules(newKoinModules(true))
    }
}

fun MainViewController(rootComponent: RootComponent) = ComposeUIViewController {
    Nav(rootComponent)
}
