package ru.shadowsparky.videobox.multiplatform.shared.domain

actual class LoggerImpl actual constructor() {
    actual fun log(tag: String, msg: String) {
        println("$tag:$msg")
    }

    actual fun log(tag: String, e: Exception, message: String?) {
        println("$tag:$message${e.message}")
    }
}