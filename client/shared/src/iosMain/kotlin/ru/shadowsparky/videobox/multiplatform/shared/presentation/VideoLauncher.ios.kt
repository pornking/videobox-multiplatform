package ru.shadowsparky.videobox.multiplatform.shared.presentation

import platform.Foundation.NSURL.Companion.URLWithString
import platform.UIKit.UIApplication

actual open class VideoLauncher actual constructor() {
    actual open fun launch(uri: String) {
        UIApplication.sharedApplication().openURL(URLWithString(uri)!!)
    }
}
