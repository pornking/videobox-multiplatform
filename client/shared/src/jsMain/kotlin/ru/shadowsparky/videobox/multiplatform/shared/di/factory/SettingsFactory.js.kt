package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import com.russhwolf.settings.Settings
import com.russhwolf.settings.StorageSettings

actual class SettingsFactory {
    actual fun create(): Settings {
        return StorageSettings()
    }
}