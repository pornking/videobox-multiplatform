package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.worker.WebWorkerDriver
import org.w3c.dom.Worker
import ru.shadowsparky.videobox.multiplatform.app.AppDatabase

actual class DatabaseDriverFactory actual constructor() {
    actual suspend fun createDriver(): SqlDriver {
        val driver = WebWorkerDriver(
            Worker(js("""new URL("@cashapp/sqldelight-sqljs-worker/sqljs.worker.js", import.meta.url)"""))
        )

        try {
            AppDatabase.Schema.create(driver).await()
        } catch (e: Exception) {

        }
        return driver
    }
}