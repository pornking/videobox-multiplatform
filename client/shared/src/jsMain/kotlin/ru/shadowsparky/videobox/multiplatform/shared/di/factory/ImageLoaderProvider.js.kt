package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import com.seiko.imageloader.ImageLoader
import com.seiko.imageloader.component.setupDefaultComponents

actual class ImageLoaderProvider actual constructor() {
    actual fun provide(): ImageLoader {
        return ImageLoader {
            components {
                setupDefaultComponents()
            }
//            interceptor {
//                memoryCacheConfig {
//                    maxSizeBytes(32 * 1024 * 1024) // 32MB
//                }
//            }
        }
    }
}