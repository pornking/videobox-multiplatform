package ru.shadowsparky.videobox.multiplatform.shared.presentation


import kotlinx.browser.window

actual open class VideoLauncher actual constructor() {
    actual open fun launch(uri: String) {
        window.open(uri)
    }
}