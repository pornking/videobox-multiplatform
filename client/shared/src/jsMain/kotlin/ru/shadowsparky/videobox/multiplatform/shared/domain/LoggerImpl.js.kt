package ru.shadowsparky.videobox.multiplatform.shared.domain

import io.ktor.utils.io.printStack

actual class LoggerImpl actual constructor() {
    actual fun log(tag: String, msg: String) {
        println("$tag:$msg")
    }

    actual fun log(tag: String, e: Throwable, message: String?) {
        println("$tag:$message")
        e.printStack()
    }
}