package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.season

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.essenty.instancekeeper.getOrCreate
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.interactor.AboutVideoInteractor
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.Season
import ru.shadowsparky.videobox.multiplatform.shared.presentation.ListSnapshot
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.createErrorHandler
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route

class SeasonOverviewComponent(
    private val interactor: AboutVideoInteractor,
    private val context: ComponentContext,
    private val dispatchers: DispatcherProvider,
    val res: ResourceProvider,
    val id: Long,
    val showEpisode: (seasonId: Long) -> Unit,
    val doBack: () -> Unit,
    private val snapshot: ListSnapshot<Season> = context.instanceKeeper.getOrCreate { ListSnapshot() }
) : ComponentContext by context {
    private val exceptionHandler = createErrorHandler(res) {
        snapshot.errorCode.emit(it)
    }
    val season = snapshot.items.asStateFlow()
    val errorMessage = snapshot.errorCode.asSharedFlow()

    fun load() {
        componentScope.launch(dispatchers.main + exceptionHandler) {
            snapshot.errorCode.emit(null)
            if (snapshot.items.value.isNullOrEmpty()) {
                val links = interactor.fetchVideoLinks(id)
                snapshot.items.emit(links.seasons)
            }
        }
    }

    class SeasonOverviewFactory(
        private val dispatcherProvider: DispatcherProvider,
        private val interactor: AboutVideoInteractor,
        private val resourceProvider: ResourceProvider
    ) : ComponentFactory<Child.SeasonOverviewChild, Route.Overview.Season> {
        override fun create(
            route: Route.Overview.Season,
            child: ComponentContext,
            root: RootComponent
        ): Child.SeasonOverviewChild {
            val comp = SeasonOverviewComponent(
                interactor,
                child,
                dispatcherProvider,
                resourceProvider,
                route.id,
                showEpisode = { root.nav(Route.Overview.Episode(route.id, it)) },
                doBack = { root.onBack() }
            )
            return Child.SeasonOverviewChild(comp)
        }
    }
}
