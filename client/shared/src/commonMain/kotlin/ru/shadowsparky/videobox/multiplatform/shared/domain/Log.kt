package ru.shadowsparky.videobox.multiplatform.shared.domain

import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import ru.shadowsparky.videobox.multiplatform.shared.di.IS_DEBUG_KEY

class Log : KoinComponent {
    private val debug: Boolean by lazy { get(named(IS_DEBUG_KEY)) }
    private val impl = LoggerImpl()

    fun log(tag: String, msg: String) {
        if (debug) {
            impl.log(tag, msg)
        }
    }

    fun log(tag: String, e: Throwable, message: String? = null) {
        if (debug) {
            impl.log(tag, e, message)
        }
    }
}
