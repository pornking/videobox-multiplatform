package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.AppDatabaseProvider

val dbModule = module {
    single { AppDatabaseProvider() }
}

enum class DbType {
    REMOTE,
    LOCAL
}
