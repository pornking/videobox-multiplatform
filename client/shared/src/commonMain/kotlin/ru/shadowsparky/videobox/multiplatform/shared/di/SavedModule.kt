package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.saved.RemoteSavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.data.saved.SqlSavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.data.saved.SyncClientSavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.IsSavedUseCase
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedGetAllUseCase
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedMovieRepository

val savedModule = module {
    factory(named(DbType.LOCAL)) {
        SqlSavedMovieRepository(
            get()
        )
    } bind SavedMovieRepository::class
    factory(named(DbType.REMOTE)) {
        RemoteSavedMovieRepository(
            get(),
            get(),
            get(),
            get()
        )
    } bind SavedMovieRepository::class
    factory {
        IsSavedUseCase(
            get(named(DbType.REMOTE)),
            get(named(DbType.LOCAL)),
            get()
        )
    }
    factory {
        SavedGetAllUseCase(
            get(named(DbType.REMOTE)),
            get(named(DbType.LOCAL)),
            get()
        )
    }
    factory {
        SyncClientSavedMovieRepository(
            get(named(DbType.REMOTE)),
            get(named(DbType.LOCAL)),
            get(),
            get()
        )
    } bind SavedMovieRepository::class
}
