package ru.shadowsparky.videobox.multiplatform.shared.presentation.settings

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.presentation.BaseScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.PreferenceCategory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.SimpleAppBar
import ru.shadowsparky.videobox.multiplatform.shared.presentation.TextPreference
import ru.shadowsparky.videobox.multiplatform.shared.presentation.applyColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.asColumnPadding

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsScreen(comp: SettingsComponent) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()
    val servAddr by comp.serverInfo.collectAsState(null)
    val tokenInfo by comp.tokenInfo.collectAsState(null)
    BaseScreen(
        scrollBehavior,
        appBar = {
            SimpleAppBar(
                comp.res.getString(ApplicationString.SETTINGS),
                { comp.doBack() },
                scrollBehavior
            )
        },
        content = {
            LazyColumn(
                modifier = Modifier.applyColumnPadding(it),
                contentPadding = it.asColumnPadding
            ) {
                item {
                    TextPreference(
                        comp.res.getString(ApplicationString.EDIT_THEME),
                        modifier = Modifier.clickable { comp.editTheme() }
                    )
                }
                if (servAddr != null) {
                    item { PreferenceCategory(comp.res.getString(ApplicationString.SERVER_SETTINGS)) }
                    item {
                        TextPreference(
                            comp.res.getString(ApplicationString.CUSTOM_SERV_ADDR),
                            servAddr!!,
                            modifier = Modifier.fillMaxWidth().clickable { comp.editServAddr() }
                        )
                    }
                }
                item { PreferenceCategory(comp.res.getString(ApplicationString.ACCOUNT)) }
                item {
                    val needAuth = tokenInfo == null
                    val msg = if (needAuth) {
                        ApplicationString.ACCOUNT_AUTH
                    } else {
                        ApplicationString.ACCOUNT_LOGOUT_AUTH
                    }
                    TextPreference(
                        comp.res.getString(msg),
                        modifier = Modifier.fillParentMaxWidth().clickable {
                            if (needAuth) {
                                comp.doAuth()
                            } else {
                                comp.logout()
                            }
                        }
                    )
                }
            }
        }
    )
}
