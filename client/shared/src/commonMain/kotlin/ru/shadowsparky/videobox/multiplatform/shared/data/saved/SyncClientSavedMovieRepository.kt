package ru.shadowsparky.videobox.multiplatform.shared.data.saved

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import ru.shadowsparky.videobox.multiplatform.shared.data.RemoteSourceSynchronizer
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.SyncRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.query
import ru.shadowsparky.videobox.multiplatform.shared.domain.use

class SyncClientSavedMovieRepository(
    private val remote: SavedMovieRepository,
    private val local: SavedMovieRepository,
    private val syncRepository: SyncRepository,
    private val authTokenCache: AuthTokenCache
) : SavedMovieRepository {

    override suspend fun save(details: VideoDetails) {
        authTokenCache.use(
            active = { remote.save(details) },
            fallback = { local.save(details) }
        )
    }

    override suspend fun remove(id: Long) {
        authTokenCache.use(
            active = { remote.remove(id) },
            fallback = { local.remove(id) }
        )
    }

    override suspend fun getAll(): Flow<List<VideoDetails>> {
        RemoteSourceSynchronizer(
            isSync = { syncRepository.isSavedMovieSynchronized() },
            setSync = { syncRepository.setSavedMovieSynchronized(it) },
            authTokenCache = authTokenCache,
            queryRemote = { remote.getAll().first() },
            queryLocal = { local.getAll().first() },
            writeRemote = { remote.saveAll(it) },
            writeLocal = { local.saveAll(it) }
        ).syncIfNeeded(false)
        return authTokenCache.query(
            active = { remote.getAll() },
            fallback = { local.getAll() }
        )
    }

    override suspend fun isSaved(id: Long): Flow<Boolean> {
        return authTokenCache.query(
            active = { remote.isSaved(id) },
            fallback = { local.isSaved(id) }
        )
    }
}
