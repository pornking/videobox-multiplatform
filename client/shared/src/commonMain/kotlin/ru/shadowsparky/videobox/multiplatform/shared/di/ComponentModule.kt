package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.core.qualifier.named
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.domain.EventType
import ru.shadowsparky.videobox.multiplatform.shared.domain.LogoutUseCase
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.VideoScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.OverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode.EpisodeOverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.season.SeasonOverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.saved.SavedMovieScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.SearchScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.SettingsComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.auth.AuthComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.register.RegisterComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.serv.EditServComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.theme.EditThemeComponent

val logoutModule = module {
    factory {
        LogoutUseCase(
            get(),
            get(),
            get(named(DbType.LOCAL)),
            get(named(DbType.LOCAL)),
            get(named(DbType.LOCAL)),
            get()
        )
    }
}

val componentModule = module {
    factory { RootComponent.RootFactory(get()) }

    factory { VideoScreenComponent.VideosChildFactory(get(), get(), get(), get(), get()) }
    factory { OverviewComponent.OverviewFactory(get(), get(), get(), get(), get(), get(), get()) }
    factory { EpisodeOverviewComponent.EpisodeOverviewFactory(get(), get(), get(named(EventType.RECENT)), get(), get()) }
    factory { SeasonOverviewComponent.SeasonOverviewFactory(get(), get(), get()) }
    factory { SearchScreenComponent.SearchScreenFactory(get(), get(), get(), get(named(EventType.SEARCH)), get()) }
    factory { SettingsComponent.SettingsComponentFactory(get(), get(), get(), get()) }
    factory { EditThemeComponent.EditThemeFactory(get(), get()) }
    factory { EditServComponent.EditServComponentFactory(get(), get(), get()) }
    factory { AuthComponent.AuthComponentFactory(get(), get(), get(), get()) }
    factory { RegisterComponent.RegisterComponentFactory(get(), get(), get(), get()) }
    factory { SavedMovieScreenComponent.SavedMovieFactory(get(), get(), get(), get(), get()) }
}
