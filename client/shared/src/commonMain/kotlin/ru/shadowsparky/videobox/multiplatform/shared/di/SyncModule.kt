package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.dsl.bind
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.SyncRepositoryImpl
import ru.shadowsparky.videobox.multiplatform.shared.domain.SyncRepository

val syncModule = module {
    factory { SyncRepositoryImpl(get()) } bind SyncRepository::class
}
