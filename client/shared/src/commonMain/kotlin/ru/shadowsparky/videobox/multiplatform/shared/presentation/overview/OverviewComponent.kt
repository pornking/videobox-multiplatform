package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.essenty.instancekeeper.InstanceKeeper
import com.arkivanov.essenty.instancekeeper.getOrCreate
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.IsSavedUseCase
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.interactor.AboutVideoInteractor
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.File
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.ShareManager
import ru.shadowsparky.videobox.multiplatform.shared.presentation.VideoLauncher
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.createErrorHandler
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route

class OverviewComponent(
    private val interactor: AboutVideoInteractor,
    private val savedMovieRepository: SavedMovieRepository,
    private val dispatcherProvider: DispatcherProvider,
    val resourceProvider: ResourceProvider,
    private val context: ComponentContext,
    private val id: Long,
    val doBack: () -> Unit,
    val showSeasons: () -> Unit,
    val videoLauncher: VideoLauncher,
    val shareManager: ShareManager,
    private val savedUseCase: IsSavedUseCase,
    private val snapshot: Snapshot = context.instanceKeeper.getOrCreate { Snapshot() }
) : ComponentContext by context {
    private val exceptionHandler = createErrorHandler(resourceProvider) {
        snapshot.errorCode.emit(it)
    }
    val details = snapshot.details.asStateFlow()
    val errorCode = snapshot.errorCode.asSharedFlow()
    val movieLink = snapshot.movieLink.asStateFlow()
    val movieUrl get() = movieLink.value?.first()?.url
    private var isSavedJob: Job? = null
    private val _isSaved = MutableStateFlow<Boolean?>(null)
    val isSaved = _isSaved.filterNotNull()

    fun init() {
        loadDetailsIfNeeded()
    }

    private fun listenIsSavedIfNeeded() {
        if (isSavedJob == null || isSavedJob?.isActive == false) {
            isSavedJob = componentScope.launch {
                savedUseCase.isSaved(id, _isSaved)
            }
        }
    }

    fun save() {
        val details = snapshot.details.value ?: return
        exec { savedMovieRepository.save(details) }
    }

    fun remove() {
        val details = snapshot.details.value ?: return
        exec { savedMovieRepository.remove(details.id) }
    }

    fun loadDetailsIfNeeded() {
        listenIsSavedIfNeeded()
        if (snapshot.details.value != null) {
            return
        }
        exec {
            val details = interactor.fetchVideoDetails(id)
            if (details.isMovie) {
                val files = interactor.fetchVideoLinks(id, null)
                snapshot.movieLink.emit(files.files)
            }
            snapshot.details.emit(details)
        }
    }

    private fun exec(callback: suspend () -> Unit): Job {
        return componentScope.launch(dispatcherProvider.main + exceptionHandler) {
            snapshot.errorCode.emit(null)
            callback.invoke()
        }
    }

    data class Snapshot(
        val details: MutableStateFlow<VideoDetails?> = MutableStateFlow(null),
        val errorCode: MutableSharedFlow<String?> = MutableSharedFlow(),
        val movieLink: MutableStateFlow<List<File>?> = MutableStateFlow(null)
    ) : InstanceKeeper.Instance

    class OverviewFactory(
        private val interactor: AboutVideoInteractor,
        private val resourceProvider: ResourceProvider,
        private val savedMovieRepository: SavedMovieRepository,
        private val videoLauncher: VideoLauncher,
        private val shareManager: ShareManager,
        private val dispatcherProvider: DispatcherProvider,
        private val savedUseCase: IsSavedUseCase
    ) : ComponentFactory<Child.OverviewChild, Route.Overview> {
        override fun create(
            route: Route.Overview,
            child: ComponentContext,
            root: RootComponent
        ): Child.OverviewChild {
            val comp = OverviewComponent(
                interactor,
                savedMovieRepository,
                dispatcherProvider,
                resourceProvider,
                child,
                route.id,
                showSeasons = { root.nav(Route.Overview.Season(route.id)) },
                doBack = { root.onBack() },
                videoLauncher = videoLauncher,
                shareManager = shareManager,
                savedUseCase = savedUseCase
            )
            return Child.OverviewChild(comp)
        }
    }
}
