package ru.shadowsparky.videobox.multiplatform.shared.domain

import kotlinx.coroutines.flow.MutableStateFlow

class IsSavedUseCase(
    private val remote: SavedMovieRepository,
    private val local: SavedMovieRepository,
    private val authTokenCache: AuthTokenCache
) {

    suspend fun isSaved(id: Long, flow: MutableStateFlow<Boolean?>) {
        authTokenCache.query(
            active = { remote.isSaved(id).collect { flow.emit(it) } },
            fallback = { local.isSaved(id).collect { flow.emit(it) } }
        )
    }
}
