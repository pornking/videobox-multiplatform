package ru.shadowsparky.videobox.multiplatform.shared.domain

import io.ktor.client.network.sockets.SocketTimeoutException
import io.ktor.client.plugins.ServerResponseException
import io.ktor.utils.io.CancellationException
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.ServerException

sealed interface InetError {
    data object SocketTimeout : InetError
    data object ServerNotResponding : InetError
    data object UnknownError : InetError
    data class ErrorFromServer(val code: Int, val msg: String?) : InetError
}

fun Throwable.asInetError(): InetError {
    return when (this) {
        is SocketTimeoutException, is CancellationException -> InetError.SocketTimeout
        is ServerResponseException -> InetError.ServerNotResponding
        is ServerException -> InetError.ErrorFromServer(httpCode, message)
        else -> InetError.UnknownError
    }
}

fun ResourceProvider.parseInetError(inetError: InetError): String {
    if (inetError is InetError.ErrorFromServer) {
        inetError.msg?.let { return it }
    }
    val resourceError = when (inetError) {
        is InetError.SocketTimeout -> ApplicationString.TIMEOUT_ERROR
        is InetError.ServerNotResponding -> ApplicationString.SERVER_NOT_RESPONDING
        else -> ApplicationString.UNKNOWN_ERROR
    }
    return getString(resourceError)
}
