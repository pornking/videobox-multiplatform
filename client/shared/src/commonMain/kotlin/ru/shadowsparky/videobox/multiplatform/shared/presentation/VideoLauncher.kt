package ru.shadowsparky.videobox.multiplatform.shared.presentation

expect open class VideoLauncher() {
    open fun launch(uri: String)
}
