package ru.shadowsparky.videobox.multiplatform.shared.presentation.search

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.SoftwareKeyboardController
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.presentation.BaseScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.applyColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.asColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.imePaddingInternal
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.ErrorAndRetry

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchVideoScreen(component: SearchScreenComponent) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()
    var textFieldValue by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(
            TextFieldValue(
                component.requestCache,
                TextRange(component.requestCache.length)
            )
        )
    }
    LaunchedEffect("search_event") {
        component.refresh()
        component.searchEvent.collect { component.refresh() }
    }
    BaseScreen(
        content = {
            val oldSearch by component.oldSearch.collectAsState()
            val errorMsg by component.errorMsg.collectAsState()
            val isLoading by component.isLoading.collectAsState()
            Column(modifier = Modifier.fillMaxSize()) {
                if (isLoading) {
                    LinearProgressIndicator(
                        modifier = Modifier
                            .padding(top = it.calculateTopPadding())
                            .fillMaxWidth()
                    )
                }
                if (errorMsg != null) {
                    ErrorAndRetry(
                        errorMsg!!,
                        component.res
                    ) { component.restart() }
                } else if (oldSearch.isNotEmpty()) {
                    OldSearchInfo(
                        info = oldSearch,
                        component.res,
                        if (isLoading) PaddingValues(0.dp) else it,
                        onClick = { value ->
                            component.onValueChange(value)
                            textFieldValue = TextFieldValue(value, TextRange(value.length))
                            component.done()
                        },
                        onDeleteRequested = {
                            component.removeFromSearch(it)
                        }
                    )
                }
            }
        },
        scrollBehavior = scrollBehavior,
        appBar = {
            InternalAppBar(
                onDone = { component.done() },
                onValueChange = {
                    component.onValueChange(it.text)
                    textFieldValue = it
                },
                onBack = { component.doBack() },
                scrollBehavior = scrollBehavior,
                textFieldValue = textFieldValue
            )
        })
}

@Composable
fun OldSearchInfo(
    info: List<String>,
    resourceProvider: ResourceProvider,
    contentPaddingValues: PaddingValues,
    onClick: (String) -> Unit,
    onDeleteRequested: (String) -> Unit
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val nestedScrollConnection = remember { HideKeyboardConnection { keyboardController } }
    LazyColumn(
        content = {
            item {
                Text(
                    text = resourceProvider.getString(ApplicationString.RECENT),
                    modifier = Modifier.padding(start = 16.dp, top = 16.dp, bottom = 4.dp),
                    color = MaterialTheme.colorScheme.primary,
                    style = MaterialTheme.typography.bodySmall
                )
            }
            items(info.size, key = { info[it] }) {
                OldSearchInfo(info[it], onDeleteRequested, onClick)
            }
        },
        modifier = Modifier.nestedScroll(nestedScrollConnection)
            .applyColumnPadding(contentPaddingValues).imePaddingInternal(),
        contentPadding = contentPaddingValues.asColumnPadding
    )
}

private class HideKeyboardConnection(
    private val keyboardControllerCallback: () -> SoftwareKeyboardController?
) : NestedScrollConnection {

    override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
        keyboardControllerCallback()?.hide()
        return super.onPreScroll(available, source)
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun OldSearchInfo(
    info: String,
    onDeleteRequested: (String) -> Unit,
    onClick: (String) -> Unit
) {
    Column(modifier = Modifier.combinedClickable { onClick(info) }) {
        Row {
            Text(
                text = info,
                modifier = Modifier.padding(16.dp).weight(1f),
                style = MaterialTheme.typography.titleMedium
            )
            IconButton(
                onClick = { onDeleteRequested(info) },
                modifier = Modifier.align(Alignment.CenterVertically)
            ) {
                Icon(Icons.Default.Delete, null)
            }
        }
        HorizontalDivider(Modifier.fillMaxWidth())
    }
}

@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun InternalAppBar(
    scrollBehavior: TopAppBarScrollBehavior? = null,
    textFieldValue: TextFieldValue,
    onDone: () -> Unit,
    onValueChange: (TextFieldValue) -> Unit,
    onBack: () -> Unit
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val inputFocusRequester = remember { FocusRequester() }
    val backFocusRequester = remember { FocusRequester() }
    val keyboardState by keyboardAsState()
    LaunchedEffect(keyboardState) {
        if (!keyboardState) {
            inputFocusRequester.freeFocus()
            backFocusRequester.requestFocus()
        }
    }
    Box {
        TopAppBar(
            title = {
                TextField(
                    value = textFieldValue,
                    onValueChange = { onValueChange(it) },
                    colors = TextFieldDefaults.colors(
                        focusedContainerColor = Color.Transparent,
                        unfocusedContainerColor = Color.Transparent,
                        disabledContainerColor = Color.Transparent,
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent,
                        disabledIndicatorColor = Color.Transparent,
                    ),
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                    keyboardActions = KeyboardActions(onDone = { onDone() }),
                    modifier = Modifier
                        .fillMaxWidth()
                        .focusRequester(inputFocusRequester)
                        .onFocusChanged {
                            if (it.isFocused) {
                                keyboardController?.show()
                            }
                        },
                    trailingIcon = {
                        if (textFieldValue.text.isNotEmpty()) {
                            IconButton(onClick = {
                                onValueChange(TextFieldValue(""))
                            }) {
                                Icon(Icons.Default.Clear, null)
                            }
                        }
                    },
                    singleLine = true
                )
            },
            navigationIcon = {
                IconButton(
                    onClick = { onBack() },
                    modifier = Modifier.focusRequester(backFocusRequester)
                ) {
                    Icon(Icons.AutoMirrored.Filled.ArrowBack, null)
                }
            },
            scrollBehavior = scrollBehavior
        )
        LaunchedEffect("focus") {
            delay(500)
            inputFocusRequester.requestFocus()
        }
    }
}

@Composable
private fun keyboardAsState(): State<Boolean> {
    val isImeVisible = WindowInsets.ime.getBottom(LocalDensity.current) > 0
    return rememberUpdatedState(isImeVisible)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppBarWithSearch(
    scrollBehavior: TopAppBarScrollBehavior? = null,
    title: String,
    isDefault: Boolean = false,
    onClick: () -> Unit,
    actions: @Composable RowScope.() -> Unit = {},
    onBack: (() -> Unit)? = null
) {
    TopAppBar(actions = {
        actions(this)
        IconButton(onClick = onClick) {
            Icon(Icons.Default.Search, null)
        }
    }, title = {
        Text(
            text = title,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.clickable(!isDefault) { onClick() }
        )
    }, navigationIcon = {
        if (onBack != null && !isDefault) {
            IconButton(onClick = { onBack() }) {
                Icon(Icons.AutoMirrored.Filled.ArrowBack, null)
            }
        }
    }, scrollBehavior = scrollBehavior)
}
