package ru.shadowsparky.videobox.multiplatform.shared.presentation

import com.arkivanov.essenty.instancekeeper.InstanceKeeper
import com.arkivanov.essenty.lifecycle.LifecycleOwner
import com.arkivanov.essenty.lifecycle.doOnDestroy
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.get
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.domain.asInetError
import ru.shadowsparky.videobox.multiplatform.shared.domain.parseInetError

val LifecycleOwner.componentScope: CoroutineScope
    get() {
        val main = koin.get<DispatcherProvider>().main
        val scope = CoroutineScope(main)
        lifecycle.doOnDestroy(scope::cancel)
        return scope
    }

data class ListSnapshot<T>(
    val items: MutableStateFlow<List<T>?> = MutableStateFlow(null),
    val errorCode: MutableSharedFlow<String?> = MutableSharedFlow()
) : InstanceKeeper.Instance

data class ItemSnapshot<T>(
    val items: MutableStateFlow<T?> = MutableStateFlow(null),
    val errorCode: MutableSharedFlow<String?> = MutableSharedFlow()
) : InstanceKeeper.Instance

fun LifecycleOwner.createErrorHandler(
    resourceProvider: ResourceProvider,
    errorCallback: suspend (String) -> Unit
): CoroutineExceptionHandler {
    return CoroutineExceptionHandler { _, throwable ->
        componentScope.launch {
            throwable.printStackTrace()
            errorCallback(resourceProvider.parseInetError(throwable.asInetError()))
        }
    }
}
