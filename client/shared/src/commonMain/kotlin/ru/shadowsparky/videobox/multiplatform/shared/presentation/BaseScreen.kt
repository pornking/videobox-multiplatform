package ru.shadowsparky.videobox.multiplatform.shared.presentation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.SnapshotMutationPolicy
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.runtime.structuralEqualityPolicy
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import com.arkivanov.decompose.ExperimentalDecomposeApi
import com.arkivanov.decompose.extensions.compose.stack.Children
import com.arkivanov.decompose.extensions.compose.stack.animation.fade
import com.arkivanov.decompose.extensions.compose.stack.animation.plus
import com.arkivanov.decompose.extensions.compose.stack.animation.predictiveback.androidPredictiveBackAnimatable
import com.arkivanov.decompose.extensions.compose.stack.animation.predictiveback.predictiveBackAnimation
import com.arkivanov.decompose.extensions.compose.stack.animation.scale
import com.arkivanov.decompose.extensions.compose.stack.animation.stackAnimation
import com.arkivanov.decompose.value.Value
import com.seiko.imageloader.LocalImageLoader
import org.koin.core.component.get
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ImageLoaderProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.VideoScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.OverviewScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode.EpisodesOverviewScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.season.SeasonOverviewScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.saved.SavedMovieScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.SearchVideoScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.SettingsScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.auth.AuthScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.register.RegisterScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.serv.EditServScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.theme.EditThemeScreen

@Composable
fun getColorScheme(): ColorScheme {
    return koin.get<UiConfiguration>()
        .getColorScheme(LocalThemeProvider.current)
}

@Composable
fun Modifier.imePaddingInternal(): Modifier {
    return koin.get<UiConfiguration>().imePadding(this)
}

val LocalThemeProvider = staticCompositionLocalOf<Theme> {
    throw IllegalStateException("Theme provider must be initialized here")
}

@Composable
fun Nav(rootComponent: RootComponent) {
    val colorScheme by rootComponent.selectedTheme.collectAsState(null)
    if (colorScheme != null) {
        CompositionLocalProvider(
            LocalImageLoader provides remember { ImageLoaderProvider().provide() },
            LocalThemeProvider provides colorScheme!!
        ) {
            NavInternal(rootComponent)
        }
    }
}

@OptIn(ExperimentalDecomposeApi::class)
@Composable
private fun NavInternal(rootComponent: RootComponent) {
    Children(
        stack = rootComponent.childStack,
        animation = predictiveBackAnimation(
            backHandler = rootComponent.backHandler,
            fallbackAnimation = stackAnimation(fade() + scale()),
            onBack = rootComponent::onBack,
            selector = { backEvent, _, _ -> androidPredictiveBackAnimatable(backEvent) },
        ),
    ) {
        when (val child = it.instance) {
            is Child.VideosChild -> VideoScreen(child.component)
            is Child.SearchChild -> SearchVideoScreen(child.component)
            is Child.OverviewChild -> OverviewScreen(child.component)
            is Child.SeasonOverviewChild -> SeasonOverviewScreen(child.component)
            is Child.EpisodeOverviewChild -> EpisodesOverviewScreen(child.component)
            is Child.SettingsChild -> SettingsScreen(child.component)
            is Child.EditServerAddrChild -> EditServScreen(child.component)
            is Child.AuthChild -> AuthScreen(child.component)
            is Child.RegisterChild -> RegisterScreen(child.component)
            is Child.EditThemeChild -> EditThemeScreen(child.component)
            is Child.SavedMovieChild -> SavedMovieScreen(child.component)
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SimpleAppBar(
    title: String,
    doBack: () -> Unit,
    scrollBehavior: TopAppBarScrollBehavior? = null
) {
    TopAppBar(
        title = { Text(title) },
        navigationIcon = { BackButton(doBack) },
        scrollBehavior = scrollBehavior
    )
}

@Composable
fun BackButton(doBack: () -> Unit) {
    IconButton(onClick = { doBack() }) {
        Icon(Icons.AutoMirrored.Filled.ArrowBack, contentDescription = null)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BaseScreen(
    scrollBehavior: TopAppBarScrollBehavior? = null,
    snackbarState: SnackbarHostState? = null,
    modifier: Modifier = Modifier,
    content: @Composable (padding: PaddingValues) -> Unit,
    appBar: @Composable () -> Unit
) {
    MaterialTheme(colorScheme = getColorScheme()) {
        Scaffold(topBar = {
            appBar()
        }, content = {
            content(it)
        }, modifier = if (scrollBehavior != null) {
            modifier.nestedScroll(scrollBehavior.nestedScrollConnection)
        } else {
            modifier
        }, snackbarHost = {
            snackbarState?.let { SnackbarHost(it) }
        })
    }
}
