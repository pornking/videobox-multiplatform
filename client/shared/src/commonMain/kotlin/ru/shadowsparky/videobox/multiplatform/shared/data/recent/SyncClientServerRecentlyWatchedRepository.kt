package ru.shadowsparky.videobox.multiplatform.shared.data.recent

import ru.shadowsparky.videobox.multiplatform.shared.data.RemoteSourceSynchronizer
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.SyncRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.RecentlyWatchedInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.query
import ru.shadowsparky.videobox.multiplatform.shared.domain.use

class SyncClientServerRecentlyWatchedRepository(
    private val remote: RecentlyWatchedRepository,
    private val local: RecentlyWatchedRepository,
    private val syncRepository: SyncRepository,
    private val authTokenCache: AuthTokenCache
) : RecentlyWatchedRepository {

    override suspend fun queryRecentlyWatched(movieId: Long, seasonId: Long?): List<RecentlyWatchedInfo> {
        return createSync(movieId, seasonId).synchronizeIfNeededAndQuery()
    }

    private suspend fun createSync(movieId: Long, seasonId: Long?): RemoteSourceSynchronizer<RecentlyWatchedInfo> {
        return RemoteSourceSynchronizer(
            isSync = { syncRepository.isRecentlyWatchedSynchronized() },
            setSync = { syncRepository.setRecentlyWatchedSynchronized(it) },
            authTokenCache = authTokenCache,
            queryRemote = { remote.query(movieId, seasonId, it) },
            queryLocal = { local.query(movieId, seasonId, it) },
            writeRemote = { remote.writeAll(it) },
            writeLocal = { local.writeAll(it) },
            prepare = { it.copy(id = 0) }
        )
    }

    private suspend fun RecentlyWatchedRepository.query(
        movieId: Long,
        seasonId: Long?,
        full: Boolean
    ): List<RecentlyWatchedInfo> {
        return if (full) {
            getAllRecentlyWatched()
        } else {
            queryRecentlyWatched(movieId, seasonId)
        }
    }

    override suspend fun getAllRecentlyWatched(): List<RecentlyWatchedInfo> {
        return authTokenCache.query(
            active = { remote.getAllRecentlyWatched() },
            fallback = { local.getAllRecentlyWatched() }
        )
    }

    override suspend fun writeAll(info: List<RecentlyWatchedInfo>) {
        authTokenCache.use(
            active = { remote.writeAll(info) },
            fallback = { local.writeAll(info) }
        )
    }

    override suspend fun removeInfo(info: RecentlyWatchedInfo) {
        authTokenCache.use(
            active = { remote.removeInfo(info) },
            fallback = { local.removeInfo(info) }
        )
    }

    override suspend fun writeInfo(info: RecentlyWatchedInfo) {
        authTokenCache.use(
            active = { remote.writeInfo(info) },
            fallback = { local.writeInfo(info) }
        )
    }

    override suspend fun toggleInfo(info: RecentlyWatchedInfo) {
        authTokenCache.use(
            active = { remote.toggleInfo(info) },
            fallback = { local.toggleInfo(info) }
        )
    }
}
