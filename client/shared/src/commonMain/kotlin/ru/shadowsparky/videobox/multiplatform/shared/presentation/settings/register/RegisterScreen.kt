package ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.register

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.auth.BaseLogin

@Composable
fun RegisterScreen(comp: RegisterComponent) {
    val login = rememberSaveable { mutableStateOf("") }
    val password = rememberSaveable { mutableStateOf("") }
    BaseLogin(
        login,
        password,
        comp.state,
        comp.res,
        comp.res.getString(ApplicationString.ACCOUNT_REG),
        doBack = comp.doBack,
        onDone = { comp.register(login.value, password.value) },
        onFinish = comp.doBack
    )
}
