package ru.shadowsparky.videobox.multiplatform.shared.data

import kotlinx.coroutines.flow.map
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.KeyValueStorage
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.TokenResponse

class AuthTokenCacheImpl(private val storage: KeyValueStorage) : AuthTokenCache {
    override val responseFlow = storage.get(KEY).map {
        it ?: return@map null
        TokenResponse(it)
    }

    override suspend fun put(token: TokenResponse) {
        storage.put(KEY, token.token)
    }

    override suspend fun remove() {
        storage.remove(KEY)
    }

    private companion object {
        const val KEY = "auth_token"
    }
}
