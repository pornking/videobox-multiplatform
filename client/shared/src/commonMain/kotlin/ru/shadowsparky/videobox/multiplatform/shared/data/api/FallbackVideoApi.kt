package ru.shadowsparky.videobox.multiplatform.shared.data.api

import app.cash.sqldelight.async.coroutines.awaitAsOneOrNull
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import ru.shadowsparky.videobox.multiplatform.domain.ApiCacheQueries
import ru.shadowsparky.videobox.multiplatform.shared.data.AppDatabaseProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideosResponse

class FallbackVideoApi(
    private val dbProvider: AppDatabaseProvider,
    private val wrapper: VideoApi,
    private val json: Json
) : VideoApi {

    override suspend fun fetchNewVideos(search: String?, page: Int, sort: String): VideosResponse {
        val key = "fetchNewVideos_${search};${page};${sort};"
        return try {
            wrapper.fetchNewVideos(search, page, sort).apply {
                writeByKey(key, this)
            }
        } catch (e: Exception) {
            getByKey(key) ?: throw e
        }
    }

    override suspend fun fetchDetails(id: Long): VideoDetails {
        val key = "fetchDetails_${id};"
        return try {
            wrapper.fetchDetails(id).apply {
                writeByKey(key, this)
            }
        } catch (e: Exception) {
            getByKey(key) ?: throw e
        }
    }

    override suspend fun fetchVideoLinks(id: Long, seasonId: Long?): VideoLinksResponse {
        val key = "fetchVideoLinks_${id};$seasonId;"
        return try {
            wrapper.fetchVideoLinks(id, seasonId).apply {
                writeByKey(key, this)
            }
        } catch (e: Exception) {
            getByKey(key) ?: throw e
        }
    }

    private suspend fun getCache(): ApiCacheQueries {
        return dbProvider.get().apiCacheQueries
    }

    private suspend inline fun <reified T> getByKey(key: String): T? {
        val value = getCache().selectByKey(key)
            .awaitAsOneOrNull()?.cache_value
            ?: return null
        return json.decodeFromString(value)
    }

    private suspend inline fun <reified T> writeByKey(key: String, value: T) {
        getCache().insert(
            key,
            json.encodeToString(value)
        )
    }
}
