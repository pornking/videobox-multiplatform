package ru.shadowsparky.videobox.multiplatform.shared.domain

import kotlinx.coroutines.flow.Flow

interface ThemeRepository {
    val theme: Flow<Theme>

    suspend fun updateTheme(newTheme: Theme)
}

enum class Theme(val code: Int) {
    DARK(0),
    LIGHT(1),
    AUTO(2)
}
