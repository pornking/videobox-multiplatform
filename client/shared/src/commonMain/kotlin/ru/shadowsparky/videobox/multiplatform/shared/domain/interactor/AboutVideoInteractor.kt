package ru.shadowsparky.videobox.multiplatform.shared.domain.interactor

import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.RecentlyWatchedInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse

class AboutVideoInteractor(
    private val api: VideoApi,
    private val recentlyDbRepo: RecentlyWatchedRepository
) {

    suspend fun fetchVideoDetails(id: Long): VideoDetails {
        return api.fetchDetails(id)
    }

    suspend fun toggleRecentlyWatched(
        info: VideoLinksResponse,
        rw: RecentlyWatchedInfo
    ): VideoLinksResponse {
        recentlyDbRepo.toggleInfo(rw)
        return info.toggleWatched(listOf(rw))
    }

    suspend fun fetchVideoLinks(id: Long, seasonId: Long? = null): VideoLinksResponse {
        val rawLink = api.fetchVideoLinks(id, seasonId)
        return if (seasonId != null) {
            val links = recentlyDbRepo.queryRecentlyWatched(id, seasonId)
            rawLink.markWatched(links)
        } else {
            rawLink
        }
    }
}
