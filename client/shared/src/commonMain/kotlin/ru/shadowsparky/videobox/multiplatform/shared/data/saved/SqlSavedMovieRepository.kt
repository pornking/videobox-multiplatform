package ru.shadowsparky.videobox.multiplatform.shared.data.saved

import app.cash.sqldelight.Query
import app.cash.sqldelight.async.coroutines.awaitAsList
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.domain.SavedMovie
import ru.shadowsparky.videobox.multiplatform.shared.data.AppDatabaseProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.SERIAL_FLAG
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails

class SqlSavedMovieRepository(
    private val dbProvider: AppDatabaseProvider
) : SavedMovieRepository {

    private suspend fun getRequest(): Query<SavedMovie> {
        return dbProvider.get().savedMovieQueries.selectAll()
    }

    override suspend fun getAll(): Flow<List<VideoDetails>> {
        return callbackFlow {
            val listener = Query.Listener {
                launch { send(obtain()) }
            }
            val request = getRequest()
            request.addListener(listener)
            send(obtain())
            awaitClose { request.removeListener(listener) }
        }
    }

    private suspend fun obtain(): List<VideoDetails> {
        val request = getRequest()
        return request.awaitAsList().map {
            VideoDetails(
                it.saved_movie_id,
                it.poster_url,
                it.description,
                it.title,
                (it.flags.toInt() and SERIAL_FLAG) != 0
            )
        }
    }

    override suspend fun isSaved(id: Long): Flow<Boolean> {
        return getAll().map { it.any { id == it.id } }
    }

    override suspend fun save(details: VideoDetails) {
        val canSave = dbProvider.get().savedMovieQueries.selectAll().awaitAsList()
            .none { it.saved_movie_id == details.id }
        if (canSave) {
            dbProvider.get().savedMovieQueries.insertSavedMovie(
                details.id,
                details.poster,
                details.desc,
                details.title,
                details.flags.toLong()
            )
        }
    }

    override suspend fun remove(id: Long) {
        dbProvider.get().savedMovieQueries.deleteSavedMovie(id)
    }
}
