package ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.theme

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme
import ru.shadowsparky.videobox.multiplatform.shared.domain.ThemeRepository
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route

class EditThemeComponent(
    private val context: ComponentContext,
    private val themeRepository: ThemeRepository,
    val res: ResourceProvider,
    val doBack: () -> Unit,
) : ComponentContext by context {
    val selectedTheme = themeRepository.theme

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    private val _done = MutableStateFlow<Unit?>(null)
    val done = _done.filterNotNull()

    fun edit(newTheme: Theme) {
        componentScope.launch {
            try {
                _isLoading.emit(true)
                themeRepository.updateTheme(newTheme)
                _done.emit(Unit)
            } finally {
                _isLoading.emit(false)
            }
        }
    }

    class EditThemeFactory(
        private val themeRepository: ThemeRepository,
        private val res: ResourceProvider,
    ) : ComponentFactory<Child.EditThemeChild, Route.Settings.EditTheme> {
        override fun create(
            route: Route.Settings.EditTheme,
            child: ComponentContext,
            root: RootComponent
        ): Child.EditThemeChild {
            val comp = EditThemeComponent(
                child,
                themeRepository,
                res,
                doBack = { root.onBack() }
            )
            return Child.EditThemeChild(comp)
        }
    }
}
