package ru.shadowsparky.videobox.multiplatform.shared.presentation.nav

import kotlinx.serialization.Serializable

@Serializable
sealed interface Route {

    @Serializable
    class Videos(val request: String?) : Route

    @Serializable
    data object SearchVideos : Route

    @Serializable
    data object Saved : Route

    @Serializable
    class Overview(val id: Long) : Route {

        @Serializable
        class Season(val id: Long) : Route

        @Serializable
        class Episode(val id: Long, val selectedSeasonId: Long) : Route
    }

    @Serializable
    data object Settings : Route {

        @Serializable
        data object EditServ : Route

        @Serializable
        data object Auth : Route

        @Serializable
        data object Register : Route

        @Serializable
        data object EditTheme : Route
    }
}
