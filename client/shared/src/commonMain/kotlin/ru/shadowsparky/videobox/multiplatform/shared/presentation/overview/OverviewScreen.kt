package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Share
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.ColorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.seiko.imageloader.rememberImagePainter
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.presentation.BaseScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.applyColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.asColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.clipboardUrl
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.ErrorAndRetry
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.Loading
import videoboxmultiplatform.client.shared.generated.resources.Res
import videoboxmultiplatform.client.shared.generated.resources.star_unchecked

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OverviewScreen(component: OverviewComponent) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()
    val errorState = component.errorCode.collectAsState(initial = null)
    val details = component.details.collectAsState()
    LaunchedEffect(null) { component.init() }
    val snackbarState = remember { SnackbarHostState() }
    BaseScreen(content = {
        if (errorState.value != null) {
            ErrorAndRetry(
                errorMsg = errorState.value!!, resourceProvider = component.resourceProvider
            ) { component.loadDetailsIfNeeded() }
        } else if (details.value != null) {
            val details2 = details.value!!
            DetailsInfo(
                details = details2,
                load = true,
                resourceProvider = component.resourceProvider,
                paddingValues = it,
                onWatchClicked = {
                    if (details2.isMovie) {
                        val movieUrl = component.movieUrl ?: return@DetailsInfo // impossible?
                        component.videoLauncher.launch(movieUrl)
                    } else {
                        component.showSeasons()
                    }
                }
            )
        } else {
            Loading()
        }
    }, appBar = {
        OverviewAppBar(
            details,
            snackbarState,
            component,
            scrollBehavior
        )
    }, scrollBehavior = scrollBehavior, snackbarState = snackbarState)
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalResourceApi::class)
@Composable
private fun OverviewAppBar(
    details: State<VideoDetails?>,
    snackbarHostState: SnackbarHostState,
    component: OverviewComponent,
    scrollBehavior: TopAppBarScrollBehavior
) {
    val isSaved by component.isSaved.collectAsState(null)
    TopAppBar(title = {
        if (details.value != null) {
            Text(
                details.value!!.title, maxLines = 1, overflow = TextOverflow.Ellipsis
            )
        }
    }, navigationIcon = {
        IconButton(onClick = { component.doBack() }) {
            Icon(Icons.AutoMirrored.Filled.ArrowBack, contentDescription = null)
        }
    }, actions = {
        if (component.shareManager.canShare() && details.value != null) {
            IconButton(onClick = {
                component.shareManager.shareDetails(details.value!!)
            }) {
                Icon(
                    Icons.Default.Share,
                    contentDescription = component.resourceProvider.getString(ApplicationString.SHARE)
                )
            }
        }
        if (isSaved != null) {
            if (isSaved!!) {
                IconButton(onClick = { component.remove() }) {
                    Icon(Icons.Default.Star, null)
                }
            } else {
                // FIXME на веб ресурсы не работают.
                IconButton(onClick = { component.save() }) {
                    Icon(painterResource(Res.drawable.star_unchecked), null)
                }
            }
        }
        if (details.value?.isMovie == true) {
            AdditionalAppBarMenu(component.resourceProvider, snackbarHostState, component.movieUrl)
        }
    }, scrollBehavior = scrollBehavior)
}

@Composable
private fun AdditionalAppBarMenu(
    res: ResourceProvider,
    snack: SnackbarHostState,
    url: String?
) {
    url ?: return
    var expanded by rememberSaveable { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val clipboardManager = LocalClipboardManager.current
    IconButton(onClick = {
        expanded = true
    }) {
        Icon(Icons.Default.MoreVert, null)
    }
    val text = res.getString(ApplicationString.COPY)
    DropdownMenu(expanded, onDismissRequest = { expanded = false }) {
        Text(text, Modifier.clickable {
            snack.clipboardUrl(url, scope, clipboardManager, res)
        }.padding(8.dp))
    }
}

@Composable
fun DetailsInfo(
    details: VideoDetails,
    load: Boolean = true,
    resourceProvider: ResourceProvider,
    paddingValues: PaddingValues,
    onWatchClicked: () -> Unit
) {
    DetailsInfoInternal(
        load,
        details,
        resourceProvider.getString(
            if (details.isSerial) {
                ApplicationString.DETAILS_SERIAL
            } else {
                ApplicationString.DETAILS_MOVIE
            }
        ),
        resourceProvider.getString(ApplicationString.DESC),
        resourceProvider.getString(ApplicationString.WATCH),
        paddingValues,
        onWatchClicked
    )
}

@Composable
private fun DetailsInfoInternal(
    load: Boolean,
    details: VideoDetails,
    showAllDetailsText: String,
    descriptionCategoryText: String,
    watchText: String,
    paddingValues: PaddingValues,
    onWatchClicked: () -> Unit,
) {
    val showDetails = rememberSaveable { mutableStateOf(false) }
    LazyColumn(
        modifier = Modifier.applyColumnPadding(paddingValues),
        contentPadding = paddingValues.asColumnPadding
    ) {
        item { OverviewTitle(load, details) }
        item {
            OverviewDescription(
                descriptionCategoryText, details, showDetails, showAllDetailsText
            )
        }
        item {
            OutlinedButton(
                onClick = { onWatchClicked() }, modifier = Modifier.fillMaxWidth().padding(8.dp)
            ) {
                Text(text = watchText)
            }
        }
    }
}

@Composable
private fun OverviewDescription(
    descriptionCategoryText: String,
    details: VideoDetails,
    showDetails: MutableState<Boolean>,
    showAllDetailsText: String
) {
    Column(
        modifier = Modifier.fillMaxWidth().padding(4.dp),
    ) {
        Text(
            text = descriptionCategoryText,
            color = MaterialTheme.colorScheme.primary,
            style = MaterialTheme.typography.titleMedium,
            modifier = Modifier.padding(vertical = 8.dp)
        )
        Text(
            text = details.desc.trim(),
            style = MaterialTheme.typography.bodyLarge,
            maxLines = if (showDetails.value) Int.MAX_VALUE else 2,
            overflow = TextOverflow.Ellipsis
        )
        if (!showDetails.value) {
            Text(text = showAllDetailsText,
                style = MaterialTheme.typography.bodyLarge,
                color = MaterialTheme.colorScheme.primary,
                modifier = Modifier.clickable {
                    showDetails.value = true
                })
        }
    }
}

@Composable
private fun OverviewTitle(
    load: Boolean, details: VideoDetails
) {
    Box(
        contentAlignment = Alignment.Center, modifier = Modifier.fillMaxWidth()
    ) {
        Image(
            painter = if (load) {
                rememberImagePainter(details.poster)
            } else {
                ColorPainter(Color.Red)
            },
            contentDescription = null,
            modifier = Modifier.size(150.dp).clip(RoundedCornerShape(8)),
            contentScale = ContentScale.Crop,
        )
    }
}
