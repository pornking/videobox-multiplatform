package ru.shadowsparky.videobox.multiplatform.shared.presentation.list

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.essenty.instancekeeper.getOrCreate
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoItem
import ru.shadowsparky.videobox.multiplatform.shared.presentation.ListSnapshot
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.createErrorHandler
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.LazyListStateHolder
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.TopAppBarStateHolder

class VideoScreenComponent(
    private val context: ComponentContext,
    val res: ResourceProvider,
    private val videoApi: VideoApi,
    private val dispatcherProvider: DispatcherProvider,
    val listStateHolder: LazyListStateHolder,
    val topAppBarStateHolder: TopAppBarStateHolder,
    val search: String? = null,
    val showOverview: (Long) -> Unit,
    val showSearch: () -> Unit,
    val doBack: () -> Unit,
    val showSettings: () -> Unit,
    val showSaved: () -> Unit
) : ComponentContext by context {
    private val snapshot = instanceKeeper.getOrCreate { ListSnapshot<VideoItem>() }
    private val exceptionHandler = createErrorHandler(res) {
        snapshot.errorCode.emit(it)
    }
    val items: StateFlow<List<VideoItem>?> = snapshot.items
    val errorCode = snapshot.errorCode

    init {
        load()
    }

    fun load() {
        componentScope.launch(dispatcherProvider.main + exceptionHandler) {
            snapshot.errorCode.emit(null)
            val rsp = videoApi.fetchNewVideos(search)
            snapshot.items.emit(rsp.items)
        }
    }

    class VideosChildFactory(
        private val resourceProvider: ResourceProvider,
        private val listStateHolder: LazyListStateHolder,
        private val topAppBarStateHolder: TopAppBarStateHolder,
        private val videoApi: VideoApi,
        private val dispatcherProvider: DispatcherProvider
    ) : ComponentFactory<Child.VideosChild, Route.Videos> {
        override fun create(
            route: Route.Videos,
            child: ComponentContext,
            root: RootComponent
        ): Child.VideosChild {
            val comp = VideoScreenComponent(
                child,
                resourceProvider,
                videoApi,
                dispatcherProvider,
                listStateHolder,
                topAppBarStateHolder,
                route.request,
                showOverview = { root.nav(Route.Overview(it))},
                showSearch = { root.nav(Route.SearchVideos, true) },
                showSettings = { root.nav(Route.Settings) },
                showSaved = { root.nav(Route.Saved) },
                doBack = { root.onBack() }
            )
            return Child.VideosChild(comp)
        }
    }
}
