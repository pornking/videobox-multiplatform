package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.dsl.bind
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.ThemeRepositoryImpl
import ru.shadowsparky.videobox.multiplatform.shared.domain.ThemeRepository

val themeModule = module {
    factory { ThemeRepositoryImpl(get(), get()) } bind ThemeRepository::class
}
