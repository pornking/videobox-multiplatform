package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.RemoteEventListenerImpl
import ru.shadowsparky.videobox.multiplatform.shared.data.recent.RemoteRecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.data.recent.SqlRecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.data.recent.SyncClientServerRecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.EventType
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventHandler
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventListener

val recentlyModule = module {
    factory(named(DbType.LOCAL)) {
        SqlRecentlyWatchedRepository(get())
    } bind RecentlyWatchedRepository::class
    factory(named(DbType.REMOTE)) {
        RemoteRecentlyWatchedRepository(
            get(),
            get(),
            get(),
            get()
        )
    } bind RecentlyWatchedRepository::class
    factory {
        SyncClientServerRecentlyWatchedRepository(
            get(named(DbType.REMOTE)),
            get(named(DbType.LOCAL)),
            get(),
            get()
        )
    } bind RecentlyWatchedRepository::class
    factory(named(EventType.RECENT)) {
        RemoteEventListenerImpl(
            get(),
            get(),
            get(),
            get(),
            get(),
            RemoteEventHandler.RECENTLY_CHANGED
        )
    } bind RemoteEventListener::class
}
