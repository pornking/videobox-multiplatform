package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import com.seiko.imageloader.ImageLoader

expect class ImageLoaderProvider() {
    fun provide(): ImageLoader
}
