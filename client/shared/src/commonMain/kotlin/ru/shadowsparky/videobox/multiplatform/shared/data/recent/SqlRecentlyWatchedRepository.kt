package ru.shadowsparky.videobox.multiplatform.shared.data.recent

import app.cash.sqldelight.Query
import app.cash.sqldelight.async.coroutines.awaitAsList
import ru.shadowsparky.videobox.multiplatform.domain.RecentlyWatched
import ru.shadowsparky.videobox.multiplatform.domain.RecentlyWatchedQueries
import ru.shadowsparky.videobox.multiplatform.shared.data.AppDatabaseProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.RecentlyWatchedInfo

class SqlRecentlyWatchedRepository(
    private val dbProvider: AppDatabaseProvider
) : RecentlyWatchedRepository {

    override suspend fun writeInfo(info: RecentlyWatchedInfo) {
        dbProvider.get().recentlyWatchedQueries.insertRecentlyWatched(
            info.movieId,
            info.episode ?: UNKNOWN,
            info.season ?: UNKNOWN
        )
    }

    override suspend fun toggleInfo(info: RecentlyWatchedInfo) {
        val recordInDb = queryRecentlyWatched(info.movieId).find {
            it.movieId == info.movieId && it.episode == info.episode && it.season == info.season
        }
        if (recordInDb != null) {
            removeInfo(recordInDb)
        } else {
            writeInfo(info)
        }
    }

    override suspend fun removeInfo(info: RecentlyWatchedInfo) {
        dbProvider.get().recentlyWatchedQueries.deleteRwByIds(
            info.movieId,
            info.episode ?: UNKNOWN,
            info.season ?: UNKNOWN
        )
    }

    override suspend fun queryRecentlyWatched(movieId: Long, seasonId: Long?): List<RecentlyWatchedInfo> {
        return query { selectByMovieId(movieId) }
    }

    override suspend fun getAllRecentlyWatched(): List<RecentlyWatchedInfo> {
        return query { selectAll() }
    }

    private suspend fun query(
        callback: RecentlyWatchedQueries.() -> Query<RecentlyWatched>
    ): List<RecentlyWatchedInfo> {
        val data = callback(dbProvider.get().recentlyWatchedQueries).awaitAsList()
        return data.map {
            val ep = if (it.episode == UNKNOWN) null else it.episode
            val season = if (it.season == UNKNOWN) null else it.season
            RecentlyWatchedInfo(it.id, it.movie_id, ep, season)
        }
    }

    override suspend fun clear() {
        dbProvider.get().recentlyWatchedQueries.clear()
    }

    private companion object {
        const val UNKNOWN = -1L
    }
}
