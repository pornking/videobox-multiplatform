package ru.shadowsparky.videobox.multiplatform.shared.domain

import kotlinx.serialization.json.Json

val json = Json {
    ignoreUnknownKeys = true
    isLenient = true
}
