package ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.auth

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.Log
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginInfo
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route

class AuthComponent(
    private val context: ComponentContext,
    val res: ResourceProvider,
    val doBack: () -> Unit,
    val doRegister: () -> Unit,
    private val authTokenRepository: AuthTokenRepository,
    private val dispatcherProvider: DispatcherProvider,
    private val log: Log
) : ComponentContext by context {
    private val _state = MutableSharedFlow<LoginState>()
    val state = _state.asSharedFlow()

    fun reset() {
        componentScope.launch(dispatcherProvider.main) {
            _state.emit(LoginState.Idle)
        }
    }

    fun login(login: String, password: String) {
        componentScope.launch(dispatcherProvider.main) {
            _state.emit(LoginState.Loading)
            try {
                authTokenRepository.login(LoginInfo(login, password))
                _state.emit(LoginState.Succeed)
            } catch (e: LoginException) {
                log.log("AuthComponent", e)
                _state.emit(LoginState.Error(e))
            }
        }
    }

    class AuthComponentFactory(
        private val resourceProvider: ResourceProvider,
        private val authTokenRepository: AuthTokenRepository,
        private val dispatcherProvider: DispatcherProvider,
        private val log: Log
    ) : ComponentFactory<Child.AuthChild, Route.Settings.Auth> {
        override fun create(
            route: Route.Settings.Auth,
            child: ComponentContext,
            root: RootComponent
        ): Child.AuthChild {
            val comp = AuthComponent(
                child,
                resourceProvider,
                doBack = { root.onBack() },
                doRegister = { root.nav(Route.Settings.Register) },
                authTokenRepository,
                dispatcherProvider,
                log
            )
            return Child.AuthChild(comp)
        }
    }
}

sealed interface LoginState {
    data object Idle : LoginState
    data object Loading : LoginState
    data class Error(val e: LoginException) : LoginState
    data object Succeed : LoginState
}
