package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.core.component.KoinComponent
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.HmacSha512Calc
import ru.shadowsparky.videobox.multiplatform.shared.domain.Log
import ru.shadowsparky.videobox.multiplatform.shared.domain.interactor.AboutVideoInteractor
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.LazyListStateHolder
import ru.shadowsparky.videobox.multiplatform.shared.presentation.ShareManager
import ru.shadowsparky.videobox.multiplatform.shared.presentation.UiConfiguration
import ru.shadowsparky.videobox.multiplatform.shared.presentation.VideoLauncher
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.TopAppBarStateHolder

const val IS_DEBUG_KEY = "is_debug_flag"

val appModule = module {
    single { UiConfiguration() }
    single { ShareManager() }
    factory { HmacSha512Calc(get()) }
    factory { LazyListStateHolder() }
    factory { TopAppBarStateHolder() }
}

val rootComponentModule = module {
    factory { AboutVideoInteractor(get(), get()) }
}

val videoLauncherModule = module {
    single { VideoLauncher() }
}

val koin = object : KoinComponent { }

fun newKoinModules(isDebug: Boolean): List<Module> {
    return listOf(
        appModule,
        dbModule,
        recentlyModule,
        rootComponentModule,
        httpModule,
        dataStoreModule,
        componentModule,
        logoutModule,
        videoLauncherModule,
        searchModule,
        savedModule,
        syncModule,
        themeModule,
        module {
            single(named(IS_DEBUG_KEY)) { isDebug }
            single { Log() }
        }
    ) + newCommonModulesList(false)
}
