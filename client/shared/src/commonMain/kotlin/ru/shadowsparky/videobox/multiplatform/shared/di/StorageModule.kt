package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.dsl.bind
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.SettingsKeyValueStorage
import ru.shadowsparky.videobox.multiplatform.shared.domain.KeyValueStorage

val dataStoreModule = module {
    single { SettingsKeyValueStorage(get(), get()) } bind KeyValueStorage::class
}
