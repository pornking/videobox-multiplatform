package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import app.cash.sqldelight.db.SqlDriver

expect class DatabaseDriverFactory() {
    suspend fun createDriver(): SqlDriver
}
