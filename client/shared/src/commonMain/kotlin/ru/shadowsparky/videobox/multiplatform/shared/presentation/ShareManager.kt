package ru.shadowsparky.videobox.multiplatform.shared.presentation

import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails

expect class ShareManager() {
    fun shareDetails(details: VideoDetails)
    fun canShare(): Boolean
}
