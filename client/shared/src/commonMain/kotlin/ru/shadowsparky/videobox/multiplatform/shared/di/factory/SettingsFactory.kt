package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import com.russhwolf.settings.Settings

expect class SettingsFactory() {
    fun create(): Settings
}
