package ru.shadowsparky.videobox.multiplatform.shared.domain

import kotlinx.coroutines.flow.MutableStateFlow
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoItem

class SavedGetAllUseCase(
    private val remote: SavedMovieRepository,
    private val local: SavedMovieRepository,
    private val authTokenCache: AuthTokenCache
) {

    suspend fun getAll(all: MutableStateFlow<List<VideoItem>?>) {
        authTokenCache.query(
            active = { remote.getAll().collect { all.emit(it.map()) } },
            fallback = { local.getAll().collect { all.emit(it.map()) } }
        )
    }

    private fun List<VideoDetails>.map(): List<VideoItem> {
        return this.map { VideoItem(it.id, it.poster, it.title, it.desc, it.isSerial) }
    }
}
