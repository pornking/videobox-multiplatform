package ru.shadowsparky.videobox.multiplatform.shared.presentation.nav

import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.VideoScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.OverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode.EpisodeOverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.season.SeasonOverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.saved.SavedMovieScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.SearchScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.SettingsComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.auth.AuthComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.register.RegisterComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.serv.EditServComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.theme.EditThemeComponent

sealed class Child {
    class VideosChild(val component: VideoScreenComponent) : Child()
    class SearchChild(val component: SearchScreenComponent) : Child()
    class OverviewChild(val component: OverviewComponent) : Child()
    class SeasonOverviewChild(val component: SeasonOverviewComponent) : Child()
    class EpisodeOverviewChild(val component: EpisodeOverviewComponent) : Child()
    class SettingsChild(val component: SettingsComponent) : Child()
    class EditServerAddrChild(val component: EditServComponent) : Child()
    class AuthChild(val component: AuthComponent) : Child()
    class RegisterChild(val component: RegisterComponent) : Child()
    class EditThemeChild(val component: EditThemeComponent) : Child()
    class SavedMovieChild(val component: SavedMovieScreenComponent) : Child()
}
