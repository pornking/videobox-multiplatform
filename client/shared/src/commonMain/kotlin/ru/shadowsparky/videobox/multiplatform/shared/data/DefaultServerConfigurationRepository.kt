package ru.shadowsparky.videobox.multiplatform.shared.data

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import ru.shadowsparky.videobox.multiplatform.shared.domain.KeyValueStorage
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfiguration
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository

class DefaultServerConfigurationRepository(
    private val storage: KeyValueStorage
) : ServerConfigurationRepository {
    override val serverConfiguration: Flow<ServerConfiguration> = storage.get(KEY)
        .map {
            if (it != null) {
                ServerConfiguration(it)
            } else {
                ServerConfiguration()
            }
        }

    override suspend fun setServerConfiguration(conf: ServerConfiguration) {
        storage.put(KEY, conf.asHttpStr())
    }

    private companion object {
        const val KEY = "custom_server_uri"
    }
}
