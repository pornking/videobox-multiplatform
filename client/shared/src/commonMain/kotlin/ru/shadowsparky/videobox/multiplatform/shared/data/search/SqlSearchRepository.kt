package ru.shadowsparky.videobox.multiplatform.shared.data.search

import app.cash.sqldelight.async.coroutines.awaitAsList
import app.cash.sqldelight.async.coroutines.awaitAsOneOrNull
import ru.shadowsparky.videobox.multiplatform.shared.data.AppDatabaseProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchRepository

class SqlSearchRepository(
    private val dbProvider: AppDatabaseProvider
) : SearchRepository {

    override suspend fun search(query: String): List<String> {
        return if (query.isNotEmpty()) {
            dbProvider.get().searchInfoQueries
                .selectByQuery(query.addWilcard())
                .awaitAsList()
                .map { it.query }
        } else {
            dbProvider.get().searchInfoQueries
                .selectAll()
                .awaitAsList()
                .map { it.query }
        }.filter { it.isNotBlank() }
    }

    override suspend fun clear() {
        dbProvider.get().searchInfoQueries.deleteAll()
    }

    override suspend fun deleteFromSearch(query: String) {
        dbProvider.get().searchInfoQueries.deleteSearchInfo(query)
    }

    override suspend fun addToSearch(query: String) {
        if (query.isBlank()) return
        if (dbProvider.get().searchInfoQueries.selectByQuery(query).awaitAsOneOrNull() == null) {
            dbProvider.get().searchInfoQueries.insertSearchInfo(query)
        } else {
            // element already added
        }
    }

    private fun String.addWilcard(): String {
        return if (this.endsWith("%")) this else "$this%"
    }
}
