package ru.shadowsparky.videobox.multiplatform.shared.data.saved

import io.ktor.client.HttpClient
import io.ktor.client.plugins.websocket.webSocket
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.serialization.json.Json
import ru.shadowsparky.videobox.multiplatform.shared.data.apply
import ru.shadowsparky.videobox.multiplatform.shared.data.setJsonBody
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoveRequest
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.getServerConfiguration
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails

class RemoteSavedMovieRepository(
    private val httpClient: HttpClient,
    private val serverConfigurationRepository: ServerConfigurationRepository,
    private val authTokenCache: AuthTokenCache,
    private val json: Json
) : SavedMovieRepository {

    override suspend fun getAll(): Flow<List<VideoDetails>> {
        return read(null).map { json.decodeFromString(it) }
    }

    private suspend fun obtainConfig() = serverConfigurationRepository.getServerConfiguration()

    override suspend fun save(details: VideoDetails) {
        httpClient.post {
            obtainConfig().apply(this, SavedMovieRepository.WRITE)
            setJsonBody(details)
        }
    }

    override suspend fun isSaved(id: Long): Flow<Boolean> {
        return read(id).map { it.toBoolean() }
    }

    private fun read(id: Long? = null) = flow {
        val token = authTokenCache.responseFlow.first() ?: return@flow
        val config = obtainConfig()
        val url = config.asWebSocketStr() + "/${SavedMovieRepository.QUERY}"
        httpClient.webSocket(request = { url(url) }) {
            outgoing.send(Frame.Text(token.token))
            outgoing.send(Frame.Text(id?.toString() ?: "null"))
            incoming.consumeAsFlow().collect {
                val text = (it as Frame.Text).readText()
                emit(text)
            }
        }
    }

    override suspend fun remove(id: Long) {
        httpClient.post {
            obtainConfig().apply(this, SavedMovieRepository.REMOVE)
            setJsonBody(RemoveRequest(id))
        }
    }
}
