package ru.shadowsparky.videobox.multiplatform.shared.data

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import ru.shadowsparky.videobox.multiplatform.shared.domain.KeyValueStorage
import ru.shadowsparky.videobox.multiplatform.shared.domain.Log
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme
import ru.shadowsparky.videobox.multiplatform.shared.domain.ThemeRepository

class ThemeRepositoryImpl(
    private val storage: KeyValueStorage,
    private val log: Log
) : ThemeRepository {
    private val defTheme = Theme.AUTO
    override val theme: Flow<Theme> = storage.getInt(KEY).map {
        log.log(TAG, "theme flow emits $it")
        val code = it ?: defTheme.code
        Theme.entries
            .firstOrNull { it.code == code }
            ?: defTheme
    }

    override suspend fun updateTheme(newTheme: Theme) {
        storage.putInt(KEY, newTheme.code)
        log.log(TAG, "updateTheme called ${newTheme.name}(${newTheme.code})")
    }

    private companion object {
        const val TAG = "ThemeRepositoryImpl"
        const val KEY = "theme"
    }
}
