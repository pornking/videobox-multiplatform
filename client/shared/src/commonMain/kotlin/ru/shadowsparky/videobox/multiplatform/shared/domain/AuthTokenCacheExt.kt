package ru.shadowsparky.videobox.multiplatform.shared.domain

suspend fun AuthTokenCache.use(
    active: suspend () -> Unit,
    fallback: suspend (Exception?) -> Unit
) {
    var fallbackCalled = false
    if (hasAccount()) {
        try {
            active()
        } catch (e: Exception) {
            fallbackCalled = true
            fallback(e)
        }
    }
    if (!fallbackCalled) {
        fallback(null)
    }
}

suspend fun <T> AuthTokenCache.query(
    active: suspend () -> T,
    fallback: suspend () -> T
): T {
    return if (hasAccount()) {
        try {
            active()
        } catch (e: Exception) {
            fallback()
        }
    } else {
        fallback()
    }
}
