package ru.shadowsparky.videobox.multiplatform.shared.presentation.search

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.essenty.instancekeeper.InstanceKeeper
import com.arkivanov.essenty.instancekeeper.getOrCreate
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.Log
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEvent
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventListener
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchRepository
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.createErrorHandler
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route

class SearchScreenComponent(
    private val context: ComponentContext,
    val search: (String) -> Unit,
    val doBack: () -> Unit,
    private val dispatcherProvider: DispatcherProvider,
    val res: ResourceProvider,
    private val searchRepository: SearchRepository,
    private val remoteEventListener: RemoteEventListener,
    private val log: Log,
    private val snapshot: Snapshot = context.instanceKeeper.getOrCreate { Snapshot() }
) : ComponentContext by context {
    val oldSearch = snapshot.oldSearch.asStateFlow()
    val searchEvent = remoteEventListener.event.filter { it is RemoteEvent.OnSearch }
    val requestCache get() = snapshot.requestCache
    val errorMsg get() = snapshot.errorMsg
    val isLoading get() = snapshot.isLoading
    private var lastCmd: (suspend () -> Unit)? = null
    private val exceptionHandler = createErrorHandler(res) {
        snapshot.errorMsg.emit(it)
    }

    fun refresh() {
        exec { snapshot.oldSearch.emit(searchRepository.search("")) }
    }

    fun restart() {
        componentScope.launch(dispatcherProvider.main + exceptionHandler) {
            snapshot.errorMsg.emit(null)
            snapshot.isLoading.emit(true)
            try {
                lastCmd?.invoke()
                lastCmd = null
            } finally {
                snapshot.isLoading.emit(false)
            }
        }
    }

    fun onValueChange(request: String) {
        snapshot.requestCache = request
    }

    fun done() = exec {
        searchRepository.addToSearch(requestCache)
        log.log("SearchScreenComponent", "request cache emit $requestCache")
        search(requestCache)
    }

    fun removeFromSearch(item: String) = exec {
        searchRepository.deleteFromSearch(item)
        snapshot.oldSearch.emit(searchRepository.search(requestCache))
    }

    private fun exec(callback: suspend () -> Unit) {
        componentScope.launch(dispatcherProvider.main + exceptionHandler) {
            try {
                snapshot.isLoading.emit(true)
                lastCmd = callback
                callback.invoke()
                lastCmd = null
            } finally {
                snapshot.isLoading.emit(false)
            }
        }
    }

    data class Snapshot(
        var requestCache: String = "",
        val oldSearch: MutableStateFlow<List<String>> = MutableStateFlow(emptyList()),
        val doSearch: MutableSharedFlow<String?> = MutableSharedFlow(),
        val errorMsg: MutableStateFlow<String?> = MutableStateFlow(null),
        val isLoading: MutableStateFlow<Boolean> = MutableStateFlow(false)
    ) : InstanceKeeper.Instance

    class SearchScreenFactory(
        private val dispatcherProvider: DispatcherProvider,
        private val searchRepository: SearchRepository,
        private val resourceProvider: ResourceProvider,
        private val remoteEventListener: RemoteEventListener,
        private val log: Log
    ) : ComponentFactory<Child.SearchChild, Route.SearchVideos> {
        override fun create(
            route: Route.SearchVideos,
            child: ComponentContext,
            root: RootComponent
        ): Child.SearchChild {
            val comp = SearchScreenComponent(
                child,
                search = { root.nav(Route.Videos(it)) },
                doBack = { root.onBack() },
                dispatcherProvider,
                resourceProvider,
                searchRepository,
                remoteEventListener,
                log
            )
            return Child.SearchChild(comp)
        }
    }
}
