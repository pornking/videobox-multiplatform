package ru.shadowsparky.videobox.multiplatform.shared.domain

enum class OsType {
    JVM,
    ANDROID,
    IOS,
    JS
}

expect fun getCurrentOsType(): OsType
