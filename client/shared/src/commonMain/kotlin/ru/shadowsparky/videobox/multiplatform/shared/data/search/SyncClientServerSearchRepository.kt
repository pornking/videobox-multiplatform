package ru.shadowsparky.videobox.multiplatform.shared.data.search

import ru.shadowsparky.videobox.multiplatform.shared.data.RemoteSourceSynchronizer
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.SyncRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.hasAccount

class SyncClientServerSearchRepository(
    private val remote: SearchRepository,
    private val local: SearchRepository,
    private val syncRepository: SyncRepository,
    private val authTokenCache: AuthTokenCache
) : SearchRepository {

    override suspend fun search(query: String): List<String> {
        return createSync(query).synchronizeIfNeededAndQuery()
    }

    private suspend fun createSync(query: String): RemoteSourceSynchronizer<String> {
        return RemoteSourceSynchronizer(
            isSync = { syncRepository.isSearchSynchronized() },
            setSync = { syncRepository.setSearchSynchronized(it) },
            authTokenCache = authTokenCache,
            queryRemote = { remote.query(query, it) },
            queryLocal = { local.query(query, it) },
            writeRemote = { remote.addToSearch(it) },
            writeLocal = { local.addToSearch(it) }
        )
    }

    private suspend fun SearchRepository.query(
        query: String,
        full: Boolean
    ): List<String> {
        return if (full) {
            search("")
        } else {
            search(query)
        }
    }

    override suspend fun addToSearch(query: String) {
        if (authTokenCache.hasAccount()) {
            remote.addToSearch(query)
        }
        local.addToSearch(query)
    }

    override suspend fun deleteFromSearch(query: String) {
        if (authTokenCache.hasAccount()) {
            remote.deleteFromSearch(query)
        }
        local.deleteFromSearch(query)
    }

    override suspend fun clear() {
        if (authTokenCache.hasAccount()) {
            remote.clear()
        }
        local.clear()
    }
}
