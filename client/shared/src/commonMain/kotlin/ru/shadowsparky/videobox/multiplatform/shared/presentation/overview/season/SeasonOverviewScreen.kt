package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.season

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.Season
import ru.shadowsparky.videobox.multiplatform.shared.presentation.BaseScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.PreferenceCategory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.applyColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.asColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.ErrorAndRetry
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.Loading

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SeasonOverviewScreen(component: SeasonOverviewComponent) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()
    val res = component.res
    LaunchedEffect(null) { component.load() }
    BaseScreen(
        content = {
            val seasons = component.season.collectAsState(null)
            val err = component.errorMessage.collectAsState(null)
            if (err.value != null) {
                ErrorAndRetry(err.value!!, res) {
                    component.load()
                }
            } else if (seasons.value != null) {
                SeasonsInfo(
                    component.res,
                    seasons.value!!,
                    it
                ) { seasonId -> component.showEpisode(seasonId) }
            } else {
                Loading()
            }
        },
        appBar = {
            TopAppBar(
                title = {
                    Text(component.res.getString(ApplicationString.SEASONS_OVERVIEW))
                },
                navigationIcon = {
                    IconButton(onClick = { component.doBack() }) {
                        Icon(Icons.AutoMirrored.Filled.ArrowBack, null)
                    }
                },
                scrollBehavior = scrollBehavior
            )
        },
        scrollBehavior = scrollBehavior
    )
}

@Composable
fun SeasonsInfo(
    resourceProvider: ResourceProvider,
    seasons: List<Season>,
    paddingValues: PaddingValues,
    onClick: (Long) -> Unit
) {
    LazyColumn(
        modifier = Modifier.applyColumnPadding(paddingValues).fillMaxWidth(),
        contentPadding = paddingValues.asColumnPadding
    ) {
        item {
            PreferenceCategory(resourceProvider.getString(ApplicationString.SELECT_SEASON))
        }
        items(seasons.size) {
            SeasonInfo(titleText = seasons[it].title) {
                onClick(seasons[it].id)
            }
        }
    }
}

@Composable
fun SeasonInfo(titleText: String, onClick: () -> Unit) {
    Text(titleText, modifier = Modifier.fillMaxWidth().clickable { onClick() }.padding(16.dp))
}
