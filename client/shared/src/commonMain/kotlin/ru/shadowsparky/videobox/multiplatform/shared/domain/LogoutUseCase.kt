package ru.shadowsparky.videobox.multiplatform.shared.domain

import ru.shadowsparky.videobox.multiplatform.shared.data.BearerTokenHolder

class LogoutUseCase(
    private val syncRepository: SyncRepository,
    private val tokenCache: AuthTokenCache,
    private val localSearchRepo: SearchRepository,
    private val localRecentlyRepo: RecentlyWatchedRepository,
    private val localSavedMovieRepository: SavedMovieRepository,
    private val bearerTokenHolder: BearerTokenHolder
) {
    val token = tokenCache.responseFlow

    suspend fun logout() {
        tokenCache.remove()
        bearerTokenHolder.clear()
        localSearchRepo.clear()
        localRecentlyRepo.clear()
        localSavedMovieRepository.clear()
        syncRepository.setRecentlyWatchedSynchronized(false)
        syncRepository.setSearchSynchronized(false)
        syncRepository.setSavedMovieSynchronized(false)
    }
}
