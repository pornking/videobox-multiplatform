package ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.theme

import androidx.compose.foundation.clickable
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme.AUTO
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme.DARK
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme.LIGHT
import ru.shadowsparky.videobox.multiplatform.shared.presentation.BaseScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.SimpleAppBar
import ru.shadowsparky.videobox.multiplatform.shared.presentation.TextPreference
import ru.shadowsparky.videobox.multiplatform.shared.presentation.applyColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.asColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.Loading

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditThemeScreen(comp: EditThemeComponent) {
    val isLoading by comp.isLoading.collectAsState()
    val theme by comp.selectedTheme.collectAsState(null)
    LaunchedEffect(null) {
        comp.done.collect { comp.doBack() }
    }
    if (isLoading || theme == null) {
        Loading()
    } else {
        BaseScreen(
            content = {
                LazyColumn(
                    modifier = Modifier.applyColumnPadding(it),
                    contentPadding = it.asColumnPadding
                ) {
                    Theme.entries.forEach {
                        item {
                            val isActive = it == theme
                            TextPreference(
                                it.title(comp.res),
                                if (isActive) {
                                    comp.res.getString(ApplicationString.THEME_CURRENTLY_ACTIVE)
                                } else {
                                    null
                                },
                                Modifier.clickable { comp.edit(it) }
                            )
                        }
                    }
                }
            },
            appBar = {
                SimpleAppBar(
                    comp.res.getString(ApplicationString.EDIT_THEME),
                    { comp.doBack() }
                )
            }
        )
    }
}

private fun Theme.title(res: ResourceProvider): String {
    val str = when (this) {
        DARK -> ApplicationString.DARK_THEME
        LIGHT -> ApplicationString.LIGHT_THEME
        AUTO -> ApplicationString.AUTO_THEME
    }
    return res.getString(str)
}
