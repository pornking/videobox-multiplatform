package ru.shadowsparky.videobox.multiplatform.shared.presentation.settings

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.LogoutUseCase
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route

class SettingsComponent(
    private val context: ComponentContext,
    val res: ResourceProvider,
    private val dispatcherProvider: DispatcherProvider,
    private val serverConfigurationRepository: ServerConfigurationRepository,
    private val logoutUseCase: LogoutUseCase,
    val doBack: () -> Unit,
    val editServAddr: () -> Unit,
    val doAuth: () -> Unit,
    val editTheme: () -> Unit
) : ComponentContext by context {
    val serverInfo = serverConfigurationRepository.serverConfiguration
        .filterNotNull()
        .map { it.asHttpStr() }

    val tokenInfo = logoutUseCase.token

    fun logout() {
        componentScope.launch(dispatcherProvider.main) {
            logoutUseCase.logout()
        }
    }

    class SettingsComponentFactory(
        private val resourceProvider: ResourceProvider,
        private val serverConfigurationRepository: ServerConfigurationRepository,
        private val dispatcherProvider: DispatcherProvider,
        private val logoutUseCase: LogoutUseCase
    ) : ComponentFactory<Child.SettingsChild, Route.Settings> {
        override fun create(
            route: Route.Settings,
            child: ComponentContext,
            root: RootComponent
        ): Child.SettingsChild {
            val comp = SettingsComponent(
                child,
                resourceProvider,
                dispatcherProvider,
                serverConfigurationRepository,
                doBack = { root.onBack() },
                editServAddr = { root.nav(Route.Settings.EditServ) },
                doAuth = { root.nav(Route.Settings.Auth) },
                logoutUseCase = logoutUseCase,
                editTheme = { root.nav(Route.Settings.EditTheme) }
            )
            return Child.SettingsChild(comp)
        }
    }
}
