package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.unit.dp
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.Episode
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.presentation.BaseScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.PreferenceCategory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.applyColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.asColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.clipboardUrl
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.ErrorAndRetry
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.Loading

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EpisodesOverviewScreen(component: EpisodeOverviewComponent) {
    val launcher = component.videoLauncher
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()
    LaunchedEffect("episodes_event") {
        component.tryToLoadLinks()
        component.recentlyWatchedEvent.collect { component.tryToLoadLinks() }
    }
    val snackbarState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()
    val clipboardManager = LocalClipboardManager.current
    BaseScreen(
        snackbarState = snackbarState,
        content = { padding ->
            val errorCode by component.errorCode.collectAsState(null)
            val links by component.links.collectAsState(null)
            EpisodesOverviewList(
                resourceProvider = component.res,
                onLaunch = {
                    component.toggleWatched(it.episode)
                    launcher.launch(it.url)
                },
                paddingValues = padding,
                errorCode = errorCode,
                links = links,
                loadLinks = { component.tryToLoadLinks() },
                onClipboardAction = {
                    snackbarState.clipboardUrl(it.url, scope, clipboardManager, component.res)
                }
            )
        },
        appBar = {
            TopAppBar(
                title = {
                    Text(component.res.getString(ApplicationString.EPISODES_OVERVIEW))
                },
                navigationIcon = {
                    IconButton(onClick = { component.doBack() }) {
                        Icon(Icons.AutoMirrored.Filled.ArrowBack, null)
                    }
                },
                scrollBehavior = scrollBehavior
            )
        },
        scrollBehavior = scrollBehavior
    )
}

@Composable
fun EpisodesOverviewList(
    errorCode: String?,
    links: VideoLinksResponse?,
    resourceProvider: ResourceProvider,
    paddingValues: PaddingValues,
    onLaunch: (Episode) -> Unit,
    onClipboardAction: (Episode) -> Unit,
    loadLinks: () -> Unit
) {
    if (errorCode != null) {
        ErrorAndRetry(errorMsg = errorCode, resourceProvider = resourceProvider) {
            loadLinks()
        }
    } else if (links != null) {
        OverviewLinks(paddingValues, resourceProvider, links, onLaunch, onClipboardAction)
    } else {
        Loading()
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun OverviewLinks(
    paddingValues: PaddingValues,
    resourceProvider: ResourceProvider,
    links: VideoLinksResponse?,
    onLaunch: (Episode) -> Unit,
    onClipboardAction: (Episode) -> Unit
) {
    LazyColumn(
        modifier = Modifier.applyColumnPadding(paddingValues),
        contentPadding = paddingValues.asColumnPadding
    ) {
        item {
            PreferenceCategory(resourceProvider.getString(ApplicationString.CHOSE_EPISODE))
        }
        val episodes = links?.seasons?.firstOrNull()?.episodes
        if (episodes != null) {
            items(
                episodes.size,
                key = { ep -> episodes[ep].episode }
            ) { episodeId ->
                val ep = episodes[episodeId]
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .combinedClickable(
                        onClick = { onLaunch(ep) },
                        onLongClick = { onClipboardAction(ep) }
                    )
                    .padding(16.dp)
                ) {
                    Text(
                        text = ep.title ?: episodeId.toString(),
                        modifier = Modifier.weight(1f)
                    )
                    if (ep.isWatched) {
                        Icon(
                            Icons.Default.Check,
                            contentDescription = null,
                            modifier = Modifier.align(Alignment.CenterVertically).padding(start = 4.dp)
                        )
                    }
                }
            }
        }
    }
}
