package ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.auth

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.Flow
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.EmptyLoginException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.EmptyPasswordException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.ServerLoginException
import ru.shadowsparky.videobox.multiplatform.shared.presentation.BaseScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.SimpleAppBar
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.Loading

@Composable
fun AuthScreen(comp: AuthComponent) {
    val login = rememberSaveable { mutableStateOf("") }
    val password = rememberSaveable { mutableStateOf("") }
    BaseLogin(
        login,
        password,
        comp.state,
        comp.res,
        comp.res.getString(ApplicationString.ACCOUNT_AUTH),
        doBack = comp.doBack,
        onDone = { comp.login(login.value, password.value) },
        onFinish = comp.doBack,
        buttonAction = {
            OutlinedButton(
                onClick = { comp.doRegister() },
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(comp.res.getString(ApplicationString.ACCOUNT_REG_HINT))
            }
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BaseLogin(
    login: MutableState<String>,
    password: MutableState<String>,
    stateFlow: Flow<LoginState>,
    res: ResourceProvider,
    title: String,
    doBack: () -> Unit,
    onDone: () -> Unit,
    onFinish: () -> Unit,
    buttonAction: (@Composable () -> Unit)? = null
) {
    val state by stateFlow.collectAsState(LoginState.Idle)
    var errorInfo by remember { mutableStateOf<LoginException?>(null) }
    LaunchedEffect(null) {
        stateFlow.collect {
            when (it) {
                is LoginState.Error -> { errorInfo = it.e }
                LoginState.Succeed -> onFinish()
                else -> {}
            }
        }
    }
    BaseScreen(
        appBar = { SimpleAppBar(title, doBack) },
        content = {
            if (state is LoginState.Loading) {
                Loading()
            } else {
                Box(Modifier.padding(it).fillMaxSize()) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier.padding(8.dp)
                    ) {
                        if (errorInfo != null && state is LoginState.Error) {
                            AlertDialog(
                                onDismissRequest = { errorInfo = null },
                                title = { Text(res.getString(ApplicationString.ERROR_TITLE)) },
                                text = { Text(errorInfo!!.toMessage(res)) },
                                confirmButton = {
                                    Text(
                                        text = res.getString(ApplicationString.OKAY),
                                        modifier = Modifier.padding(8.dp).clickable { errorInfo = null }
                                    )
                                },
                            )
                        }
                        OutlinedTextField(
                            login.value,
                            onValueChange = { login.value = it },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
                            label = { Text(res.getString(ApplicationString.ACCOUNT_LOGIN)) },
                            modifier = Modifier.fillMaxWidth()
                        )
                        OutlinedTextField(
                            password.value,
                            onValueChange = { password.value = it },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(
                                imeAction = ImeAction.Done,
                                keyboardType = KeyboardType.Password
                            ),
                            visualTransformation = PasswordVisualTransformation(),
                            keyboardActions = KeyboardActions(onDone = { onDone() }),
                            label = { Text(res.getString(ApplicationString.ACCOUNT_PASSWORD)) },
                            modifier = Modifier.fillMaxWidth()
                        )
                        Button(
                            onClick = { onDone() },
                            modifier = Modifier.fillMaxWidth().padding(top = 4.dp)
                        ) {
                            Text(title)
                        }
                        buttonAction?.invoke()
                    }
                }
            }
        }
    )
}

private fun LoginException.toMessage(res: ResourceProvider): String {
    return when (this) {
        is EmptyLoginException -> res.getString(ApplicationString.EMPTY_LOGIN)
        is EmptyPasswordException -> res.getString(ApplicationString.EMPTY_PASS)
        is ServerLoginException -> message ?: res.getString(ApplicationString.UNKNOWN_ERROR)
        else -> res.getString(ApplicationString.UNKNOWN_ERROR)
    }
}
