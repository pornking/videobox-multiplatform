package ru.shadowsparky.videobox.multiplatform.shared.domain

expect class LoggerImpl() {
    fun log(tag: String, msg: String)
    fun log(tag: String, e: Throwable, message: String? = null)
}
