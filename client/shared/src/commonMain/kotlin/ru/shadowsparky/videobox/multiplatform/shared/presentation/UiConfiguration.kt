package ru.shadowsparky.videobox.multiplatform.shared.presentation

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme.AUTO
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme.DARK
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme.LIGHT

open class UiConfiguration {

    @Composable
    open fun getColorScheme(theme: Theme): ColorScheme {
        return if (theme.isDark()) {
            darkColorScheme()
        } else {
            lightColorScheme()
        }
    }

    @Composable
    open fun imePadding(modifier: Modifier): Modifier {
        return modifier
    }
}

@Composable
fun Theme.isDark(): Boolean {
    return when (this) {
        AUTO -> isSystemInDarkTheme()
        DARK -> true
        LIGHT -> false
    }
}
