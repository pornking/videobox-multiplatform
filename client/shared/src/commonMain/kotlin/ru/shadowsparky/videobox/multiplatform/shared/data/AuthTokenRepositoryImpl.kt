package ru.shadowsparky.videobox.multiplatform.shared.data

import io.ktor.client.HttpClient
import io.ktor.client.request.post
import io.ktor.utils.io.core.toByteArray
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.getServerConfiguration
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.EmptyLoginException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.EmptyPasswordException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.ServerException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.ServerLoginException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.TokenResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.UnknownLoginException

class AuthTokenRepositoryImpl(
    private val httpClient: HttpClient,
    private val serverConfigurationProvider: ServerConfigurationRepository,
    private val tokenCache: AuthTokenCache,
    private val hashCalculator: HmacSha512Calc
) : AuthTokenRepository {

    override suspend fun register(loginInfo: LoginInfo): TokenResponse {
        return post(loginInfo, AuthTokenRepository.REGISTER_PATH, false)
    }

    override suspend fun login(loginInfo: LoginInfo): TokenResponse {
        return post(loginInfo, AuthTokenRepository.LOGIN_PATH, true)
    }

    @OptIn(ExperimentalStdlibApi::class)
    private suspend fun post(loginInfo: LoginInfo, path: String, saveToCache: Boolean): TokenResponse {
        if (loginInfo.login.trim().isEmpty()) {
            throw EmptyLoginException()
        } else if (loginInfo.passwordHash.trim().isEmpty()) {
            throw EmptyPasswordException()
        }
        val realPassHash = hashCalculator.calc(loginInfo.passwordHash.toByteArray())
        try {
            return httpClient.post {
                serverConfigurationProvider.getServerConfiguration().apply(this, path)
                setJsonBody(loginInfo.copy(passwordHash = realPassHash.toHexString()))
            }.parse<TokenResponse>().apply {
                if (saveToCache) {
                    tokenCache.put(this)
                }
            }
        } catch (e: ServerException) {
            throw ServerLoginException(e.message ?: "Unknown error")
        } catch (e: Exception) {
            throw UnknownLoginException(e)
        }
    }
}
