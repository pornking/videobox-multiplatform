package ru.shadowsparky.videobox.multiplatform.shared.presentation.nav

import com.arkivanov.decompose.ComponentContext
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent

interface ComponentFactory<T : Child, R : Route> {
    fun create(route: R, child: ComponentContext, root: RootComponent): T
}
