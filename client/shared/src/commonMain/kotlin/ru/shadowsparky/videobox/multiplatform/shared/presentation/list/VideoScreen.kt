package ru.shadowsparky.videobox.multiplatform.shared.presentation.list

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.ColorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.seiko.imageloader.rememberImagePainter
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoItem
import ru.shadowsparky.videobox.multiplatform.shared.presentation.BaseScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.applyColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.asColumnPadding
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.AppBarWithSearch
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.LazyListStateHolder
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.rememberLazyListStateByHolder
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.rememberTopAppBarStateByHolder

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VideoScreen(component: VideoScreenComponent) {
    val appBarState = rememberTopAppBarStateByHolder(component.topAppBarStateHolder)
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(appBarState)
    val resourceProvider = component.res
    val error by component.errorCode.collectAsState(null)
    val items by component.items.collectAsState()
    BaseScreen(
        content = {
            VideoScreenContent(
                it,
                error,
                items,
                onRefresh = { component.load() },
                onShowOverview = { component.showOverview(it) },
                resourceProvider = resourceProvider,
                emptyResource = ApplicationString.NOT_FOUND,
                holder = component.listStateHolder
            )
        },
        appBar = {
            val appName = resourceProvider.getString(ApplicationString.APP_NAME)
            val title = component.search ?: appName
            AppBarWithSearch(
                scrollBehavior,
                title,
                isDefault = title == appName,
                onClick = { component.showSearch() },
                onBack = { component.doBack() },
                actions = {
                    IconButton(onClick = { component.showSettings() }) {
                        Icon(Icons.Default.Settings, null)
                    }
                    IconButton(onClick = { component.showSaved() }) {
                        Icon(Icons.Default.Star, null)
                    }
                }
            )
        },
        scrollBehavior = scrollBehavior
    )
}

@Composable
fun VideoScreenContent(
    paddingValues: PaddingValues,
    error: String?,
    items: List<VideoItem>?,
    emptyResource: ApplicationString,
    resourceProvider: ResourceProvider,
    holder: LazyListStateHolder,
    onRefresh: () -> Unit,
    onShowOverview: (Long) -> Unit,
) {
    if (error != null) {
        ErrorAndRetry(error, resourceProvider) {
            onRefresh()
        }
    } else if (items != null) {
        VideosInfo(items = items, paddingValues, resourceProvider, emptyResource, holder) {
            onShowOverview(it.id)
        }
    } else {
        Loading()
    }
}

@Composable
fun VideosInfo(
    items: List<VideoItem>,
    paddingValues: PaddingValues,
    resourceProvider: ResourceProvider,
    emptyResource: ApplicationString,
    holder: LazyListStateHolder,
    load: Boolean = true,
    onClick: (item: VideoItem) -> Unit
) {
    if (items.isNotEmpty()) {
        val state = rememberLazyListStateByHolder(holder)
        LazyColumn(
            content = {
                items(items) { item -> VideoInfo(item = item, load) { onClick(item) } }
            },
            modifier = Modifier.applyColumnPadding(paddingValues),
            contentPadding = paddingValues.asColumnPadding,
            state = state
        )
    } else {
        VideosNotFound(resourceProvider, emptyResource)
    }
}

@Composable
fun ErrorAndRetry(errorMsg: String, resourceProvider: ResourceProvider, retry: () -> Unit) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {
        Text(
            text = errorMsg,
            style = MaterialTheme.typography.titleMedium,
            modifier = Modifier.padding(bottom = 4.dp),
            textAlign = TextAlign.Center
        )
        Button(onClick = { retry() }) {
            Text(
                text = resourceProvider.getString(ApplicationString.RETRY),
                style = MaterialTheme.typography.bodyMedium
            )
        }
    }
}

@Composable
fun VideosNotFound(resourceProvider: ResourceProvider, emptyResource: ApplicationString) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = resourceProvider.getString(emptyResource),
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.titleMedium,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun Loading() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun VideoInfo(
    item: VideoItem,
    load: Boolean = true,
    onClick: () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            .clickable {
                onClick()
            }
    ) {
        Image(
            painter = if (load) rememberImagePainter(item.poster) else ColorPainter(Color.Red),
            contentDescription = null,
            modifier = Modifier
                .size(130.dp)
                .clip(RoundedCornerShape(8)),
            contentScale = ContentScale.Crop,
        )
        Text(
            text = item.title,
            modifier = Modifier
                .fillMaxWidth()
                .padding(4.dp),
            maxLines = 4,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.titleMedium
        )
    }
}
