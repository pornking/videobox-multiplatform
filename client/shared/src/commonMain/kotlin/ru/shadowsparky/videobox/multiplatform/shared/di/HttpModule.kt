package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.dsl.bind
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.AuthTokenCacheImpl
import ru.shadowsparky.videobox.multiplatform.shared.data.AuthTokenRepositoryImpl
import ru.shadowsparky.videobox.multiplatform.shared.data.DefaultServerConfigurationRepository
import ru.shadowsparky.videobox.multiplatform.shared.data.api.FallbackVideoApi
import ru.shadowsparky.videobox.multiplatform.shared.data.api.VideoApiImpl
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi

val httpModule = module {
    single {
        val impl = VideoApiImpl(get(), get())
        FallbackVideoApi(get(), impl, get())
    } bind VideoApi::class
    factory { DefaultServerConfigurationRepository(get()) } bind ServerConfigurationRepository::class
    factory { AuthTokenRepositoryImpl(get(), get(), get(), get()) } bind AuthTokenRepository::class
    factory { AuthTokenCacheImpl(get()) } bind AuthTokenCache::class
}
