package ru.shadowsparky.videobox.multiplatform.shared.presentation

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.router.stack.ChildStack
import com.arkivanov.decompose.router.stack.StackNavigation
import com.arkivanov.decompose.router.stack.bringToFront
import com.arkivanov.decompose.router.stack.childStack
import com.arkivanov.decompose.router.stack.pop
import com.arkivanov.decompose.router.stack.push
import com.arkivanov.decompose.router.stack.pushNew
import com.arkivanov.decompose.value.Value
import com.arkivanov.essenty.backhandler.BackCallback
import com.arkivanov.essenty.backhandler.BackHandlerOwner
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.domain.ThemeRepository
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.VideoScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.OverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode.EpisodeOverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.season.SeasonOverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.saved.SavedMovieScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.SearchScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.SettingsComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.auth.AuthComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.register.RegisterComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.serv.EditServComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.theme.EditThemeComponent

class RootComponent(
    private val componentContext: ComponentContext,
    themeRepository: ThemeRepository,
    private val koin: KoinComponent,
    private val overviewId: Long? = null
) : ComponentContext by componentContext, BackHandlerOwner {
    val selectedTheme = themeRepository.theme
    private val nav = StackNavigation<Route>()
    private val _childStack = childStack(
        source = nav,
        serializer = Route.serializer(),
        initialStack = {
            if (overviewId == null) {
                listOf<Route>(Route.Videos(null))
            } else {
                listOf<Route>(Route.Overview(overviewId))
            }
        },
        childFactory = ::newChild
    )
    private val backCallback = BackCallback { onBack() }
    private val _onFinish = MutableStateFlow<Unit?>(null)
    val onFinish = _onFinish.filterNotNull()

    fun nav(route: Route, bringToFront: Boolean = false) {
        if (bringToFront) {
            nav.bringToFront(route)
        } else {
            nav.pushNew(route)
        }
    }

    fun onBack() {
        nav.pop {
            if (!it) {
                componentScope.launch {
                    _onFinish.emit(Unit)
                }
            }
        }
    }

    val childStack: Value<ChildStack<*, Child>> = _childStack

    init {
        backHandler.register(backCallback)
    }

    private fun newChild(route: Route, ctx: ComponentContext): Child {
        val factory = route.provideFactory<Child, Route>(koin)
        return factory.create(route, ctx, this)
    }

    class RootFactory(private val themeRepository: ThemeRepository) {
        fun create(
            context: ComponentContext,
            overviewId: Long? = null
        ): RootComponent {
            return RootComponent(context, themeRepository, koin, overviewId)
        }
    }
}

fun <T : Child, R : Route> Route.provideFactory(koin: KoinComponent): ComponentFactory<T, R> {
    val factory = when (this) {
        is Route.Videos -> koin.get<VideoScreenComponent.VideosChildFactory>()
        is Route.SearchVideos -> koin.get<SearchScreenComponent.SearchScreenFactory>()
        is Route.Overview -> koin.get<OverviewComponent.OverviewFactory>()
        is Route.Overview.Season -> koin.get<SeasonOverviewComponent.SeasonOverviewFactory>()
        is Route.Overview.Episode -> koin.get<EpisodeOverviewComponent.EpisodeOverviewFactory>()
        is Route.Settings -> koin.get<SettingsComponent.SettingsComponentFactory>()
        is Route.Settings.EditServ -> koin.get<EditServComponent.EditServComponentFactory>()
        is Route.Settings.Auth -> koin.get<AuthComponent.AuthComponentFactory>()
        is Route.Settings.Register -> koin.get<RegisterComponent.RegisterComponentFactory>()
        is Route.Settings.EditTheme -> koin.get<EditThemeComponent.EditThemeFactory>()
        is Route.Saved -> koin.get<SavedMovieScreenComponent.SavedMovieFactory>()
    }
    @Suppress("UNCHECKED_CAST")
    return factory as ComponentFactory<T, R>
}
