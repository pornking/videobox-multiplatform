package ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.register

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.Log
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginException
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.LoginInfo
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route
import ru.shadowsparky.videobox.multiplatform.shared.presentation.settings.auth.LoginState

class RegisterComponent(
    private val context: ComponentContext,
    val res: ResourceProvider,
    val doBack: () -> Unit,
    private val authTokenRepository: AuthTokenRepository,
    private val dispatcherProvider: DispatcherProvider,
    private val log: Log,
) : ComponentContext by context, KoinComponent {
    private val _state = MutableStateFlow<LoginState>(LoginState.Idle)
    val state = _state.asStateFlow()

    fun register(login: String, password: String) {
        componentScope.launch(dispatcherProvider.main) {
            _state.emit(LoginState.Loading)
            try {
                authTokenRepository.register(LoginInfo(login, password))
                _state.emit(LoginState.Succeed)
            } catch (e: LoginException) {
                log.log("RegisterComponent", e)
                _state.value = LoginState.Error(e)
            }
        }
    }

    class RegisterComponentFactory(
        private val resourceProvider: ResourceProvider,
        private val authTokenRepository: AuthTokenRepository,
        private val dispatcherProvider: DispatcherProvider,
        private val log: Log
    ) : ComponentFactory<Child.RegisterChild, Route.Settings.Register> {
        override fun create(
            route: Route.Settings.Register,
            child: ComponentContext,
            root: RootComponent
        ): Child.RegisterChild {
            val comp = RegisterComponent(
                child,
                resourceProvider,
                doBack = { root.onBack() },
                authTokenRepository,
                dispatcherProvider,
                log
            )
            return Child.RegisterChild(comp)
        }
    }
}
