package ru.shadowsparky.videobox.multiplatform.shared.data.recent

import io.ktor.client.HttpClient
import io.ktor.client.request.post
import kotlinx.coroutines.flow.firstOrNull
import ru.shadowsparky.videobox.multiplatform.shared.data.apply
import ru.shadowsparky.videobox.multiplatform.shared.data.get
import ru.shadowsparky.videobox.multiplatform.shared.data.setJsonBody
import ru.shadowsparky.videobox.multiplatform.shared.data.throwIfException
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.Log
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRequest
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.getServerConfiguration
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.RecentlyWatchedInfo

class RemoteRecentlyWatchedRepository(
    private val httpClient: HttpClient,
    private val serverConfigurationRepository: ServerConfigurationRepository,
    private val authTokenCache: AuthTokenCache,
    private val log: Log
) : RecentlyWatchedRepository {

    private suspend fun obtainConfig() = serverConfigurationRepository.getServerConfiguration()

    override suspend fun removeInfo(info: RecentlyWatchedInfo) {
        post(info, RecentlyWatchedRepository.REMOVE)
    }

    override suspend fun writeInfo(info: RecentlyWatchedInfo) {
        post(info, RecentlyWatchedRepository.WRITE)
    }

    override suspend fun writeAll(info: List<RecentlyWatchedInfo>) {
        if (info.isNotEmpty()) {
            post(info, RecentlyWatchedRepository.WRITE)
        }
    }

    override suspend fun toggleInfo(info: RecentlyWatchedInfo) {
        post(info, RecentlyWatchedRepository.TOGGLE)
    }

    private suspend fun post(info: RecentlyWatchedInfo, path: String) {
        post(listOf(info), path)
    }

    private suspend fun post(info: List<RecentlyWatchedInfo>, path: String) {
        authTokenCache.responseFlow.firstOrNull() ?: return
        httpClient.post {
            obtainConfig().apply(this, path)
            setJsonBody(RecentlyWatchedRequest(info))
        }.throwIfException()
    }

    override suspend fun queryRecentlyWatched(movieId: Long, seasonId: Long?): List<RecentlyWatchedInfo> {
        return queryInternal(movieId, seasonId)
    }

    override suspend fun getAllRecentlyWatched(): List<RecentlyWatchedInfo> {
        return queryInternal(null, null)
    }

    private suspend fun queryInternal(
        movieId: Long?,
        seasonId: Long?
    ): List<RecentlyWatchedInfo> {
        authTokenCache.responseFlow.firstOrNull() ?: return emptyList()
        val map = hashMapOf<String, String>()
        if (movieId != null) {
            map[RecentlyWatchedRepository.MOVIE_ID_ARG] = movieId.toString()
        }
        if (seasonId != null) {
            map[RecentlyWatchedRepository.SEASON_ID_ARG] = seasonId.toString()
        }
        try {
            return httpClient.get<RecentlyWatchedResponse>(
                RecentlyWatchedRepository.GET_INFO,
                map,
                obtainConfig()
            ).info
        } catch (e: Exception) {
            log.log("RemoteRecentlyWatchedRepository", "error")
            throw e
        }
    }
}
