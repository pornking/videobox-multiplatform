package ru.shadowsparky.videobox.multiplatform.shared.data

import io.ktor.client.HttpClient
import io.ktor.client.plugins.websocket.webSocket
import io.ktor.client.request.url
import io.ktor.http.HttpMethod
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.serialization.json.Json
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.Log
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEvent
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventListener
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.getServerConfiguration

class RemoteEventListenerImpl(
    private val httpClient: HttpClient,
    private val serverConfigurationRepository: ServerConfigurationRepository,
    private val authTokenCache: AuthTokenCache,
    private val json: Json,
    private val log: Log,
    private val suffix: String
) : RemoteEventListener {
    override val event: Flow<RemoteEvent> = flow {
        val config = serverConfigurationRepository.getServerConfiguration()
        val token = authTokenCache.responseFlow.first() ?: return@flow
        try {
            httpClient.webSocket(
                method = HttpMethod.Get,
                request = { url(config.asWebSocketStr() + "/$suffix") }
            ) {
                outgoing.send(Frame.Text(token.token))
                log.log(TAG, "send token ${token.token}")
                while (true) {
                    val text = (incoming.receive() as? Frame.Text)?.readText()
                    log.log(TAG, "receive $text")
                    if (text != null) {
                        emit(json.decodeFromString(text))
                    }
                }
            }
        } catch (e: Exception) {
            log.log(TAG, e, "error occurred")
        }
    }

    private companion object {
        const val TAG = "RemoteSearchEventListener"
    }
}
