package ru.shadowsparky.videobox.multiplatform.shared.data.search

import io.ktor.client.HttpClient
import io.ktor.client.request.delete
import io.ktor.client.request.post
import kotlinx.coroutines.flow.first
import ru.shadowsparky.videobox.multiplatform.shared.data.apply
import ru.shadowsparky.videobox.multiplatform.shared.data.get
import ru.shadowsparky.videobox.multiplatform.shared.data.setJsonBody
import ru.shadowsparky.videobox.multiplatform.shared.data.throwIfException
import ru.shadowsparky.videobox.multiplatform.shared.domain.AuthTokenCache
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchDeleteRequest
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchQueryRequest
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchQueryResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfiguration
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.getServerConfiguration

class RemoteSearchRepository(
    private val httpClient: HttpClient,
    private val serverConfigurationProvider: ServerConfigurationRepository,
    private val tokenCache: AuthTokenCache
) : SearchRepository {

    override suspend fun search(query: String): List<String> {
        tokenCache.responseFlow.first() ?: return emptyList()
        return httpClient.get<SearchQueryResponse>(
            SearchRepository.SEARCH,
            hashMapOf(SearchRepository.QUERY_ARG to query),
            obtainConf()
        ).search
    }

    override suspend fun addToSearch(query: String) {
        addToSearch(listOf(query))
    }

    override suspend fun addToSearch(query: List<String>) {
        if (query.isNotEmpty()) {
            tokenCache.responseFlow.first() ?: return
            httpClient.post {
                obtainConf().apply(this, SearchRepository.ADD_TO_SEARCH)
                setJsonBody(SearchQueryRequest(query))
            }.throwIfException()
        }
    }

    override suspend fun deleteFromSearch(query: String) { deleteInternal(query) }
    override suspend fun clear() { deleteInternal(null) }

    private suspend fun deleteInternal(query: String?) {
        tokenCache.responseFlow.first() ?: return
        httpClient.delete {
            obtainConf().apply(this, SearchRepository.CLEAR)
            setJsonBody(SearchDeleteRequest(query))
        }.throwIfException()
    }

    private suspend fun obtainConf(): ServerConfiguration {
        return serverConfigurationProvider.getServerConfiguration()
    }
}
