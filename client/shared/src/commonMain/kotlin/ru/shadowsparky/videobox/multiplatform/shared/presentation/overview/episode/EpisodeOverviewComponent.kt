package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.essenty.instancekeeper.getOrCreate
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEvent
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventListener
import ru.shadowsparky.videobox.multiplatform.shared.domain.interactor.AboutVideoInteractor
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.RecentlyWatchedInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.presentation.ItemSnapshot
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.VideoLauncher
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.createErrorHandler
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route

class EpisodeOverviewComponent(
    private val interactor: AboutVideoInteractor,
    val res: ResourceProvider,
    val videoLauncher: VideoLauncher,
    private val dispatcherProvider: DispatcherProvider,
    private val context: ComponentContext,
    private val id: Long,
    private val seasonId: Long,
    val doBack: () -> Unit,
    private val remoteEventListener: RemoteEventListener,
    private val snapshot: ItemSnapshot<VideoLinksResponse> = context.instanceKeeper.getOrCreate { ItemSnapshot() },
) : ComponentContext by context {
    val recentlyWatchedEvent = remoteEventListener.event
        .filter { it is RemoteEvent.OnRecent && it.movieId == id }
    private val exceptionHandler = createErrorHandler(res) {
        snapshot.errorCode.emit(it)
    }
    val links = snapshot.items.asStateFlow()
    val errorCode = snapshot.errorCode.asSharedFlow()

    fun tryToLoadLinks() {
        componentScope.launch(dispatcherProvider.main + exceptionHandler) {
            snapshot.errorCode.emit(null)
            val currentLinks = interactor.fetchVideoLinks(id, seasonId)
            snapshot.items.emit(currentLinks)
        }
    }

    fun toggleWatched(episode: Int) {
        val item = snapshot.items.value ?: return
        componentScope.launch(dispatcherProvider.main + exceptionHandler) {
            val rw = RecentlyWatchedInfo(0, id, episode.toLong(), seasonId)
            snapshot.items.value = interactor.toggleRecentlyWatched(item, rw)
        }
    }

    class EpisodeOverviewFactory(
        private val interactor: AboutVideoInteractor,
        private val resourceProvider: ResourceProvider,
        private val remoteEventListener: RemoteEventListener,
        private val dispatcherProvider: DispatcherProvider,
        private val videoLauncher: VideoLauncher
    ) : ComponentFactory<Child.EpisodeOverviewChild, Route.Overview.Episode> {
        override fun create(
            route: Route.Overview.Episode,
            child: ComponentContext,
            root: RootComponent
        ): Child.EpisodeOverviewChild {
            val comp = EpisodeOverviewComponent(
                interactor,
                resourceProvider,
                videoLauncher,
                dispatcherProvider,
                child,
                route.id,
                route.selectedSeasonId,
                doBack = { root.onBack() },
                remoteEventListener
            )
            return Child.EpisodeOverviewChild(comp)
        }
    }
}
