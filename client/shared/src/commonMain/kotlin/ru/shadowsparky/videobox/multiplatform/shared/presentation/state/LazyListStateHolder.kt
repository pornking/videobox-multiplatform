package ru.shadowsparky.videobox.multiplatform.shared.presentation.state

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.saveable.rememberSaveable

class LazyListStateHolder {
    var firstVisibleItemIndex: Int = 0
    var firstVisibleItemScrollOffset: Int = 0
}

@Composable
fun rememberLazyListStateByHolder(holder: LazyListStateHolder): LazyListState {
    val scrollState = rememberSaveable(saver = LazyListState.Saver) {
        val savedIndex = holder.firstVisibleItemIndex
        val savedOffset = holder.firstVisibleItemScrollOffset
        LazyListState(
            savedIndex,
            savedOffset
        )
    }
    DisposableEffect(Unit) {
        onDispose {
            holder.firstVisibleItemIndex = scrollState.firstVisibleItemIndex
            holder.firstVisibleItemScrollOffset = scrollState.firstVisibleItemScrollOffset
        }
    }
    return scrollState
}
