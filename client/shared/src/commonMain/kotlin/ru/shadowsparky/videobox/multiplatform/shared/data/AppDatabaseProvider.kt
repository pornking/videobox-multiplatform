package ru.shadowsparky.videobox.multiplatform.shared.data

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import ru.shadowsparky.videobox.multiplatform.app.AppDatabase
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DatabaseDriverFactory

class AppDatabaseProvider {
    private val mutex = Mutex()
    private var appDatabase: AppDatabase? = null

    suspend fun get(): AppDatabase {
        appDatabase?.let { return it }
        mutex.withLock {
            appDatabase?.let { return it }
            val driver = DatabaseDriverFactory().createDriver()
            AppDatabase.Schema.migrate(driver, 2, 3)
            return AppDatabase.invoke(driver).also { appDatabase = it }
        }
    }
}
