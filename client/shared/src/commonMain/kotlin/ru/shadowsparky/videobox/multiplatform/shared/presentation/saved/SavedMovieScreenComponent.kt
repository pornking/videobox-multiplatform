package ru.shadowsparky.videobox.multiplatform.shared.presentation.saved

import com.arkivanov.decompose.ComponentContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedGetAllUseCase
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoItem
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.componentScope
import ru.shadowsparky.videobox.multiplatform.shared.presentation.createErrorHandler
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Child
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.ComponentFactory
import ru.shadowsparky.videobox.multiplatform.shared.presentation.nav.Route
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.LazyListStateHolder
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.TopAppBarStateHolder

class SavedMovieScreenComponent(
    private val context: ComponentContext,
    private val dispatcherProvider: DispatcherProvider,
    private val savedGetAllUseCase: SavedGetAllUseCase,
    val listStateHolder: LazyListStateHolder,
    val topAppBarStateHolder: TopAppBarStateHolder,
    val resourceProvider: ResourceProvider,
    val doBack: () -> Unit,
    val showOverview: (Long) -> Unit
) : ComponentContext by context {
    private val _errorMsg = MutableStateFlow<String?>(null)
    val errorMsg = _errorMsg.asStateFlow()
    private val exceptionHandler = createErrorHandler(resourceProvider) { _errorMsg.emit(it) }
    private val _all = MutableStateFlow<List<VideoItem>?>(null)
    val all = _all.asStateFlow()

    init {
        load()
    }

    fun load() {
        componentScope.launch(dispatcherProvider.main + exceptionHandler) {
            _errorMsg.emit(null)
            savedGetAllUseCase.getAll(_all)
        }
    }

    class SavedMovieFactory(
        private val savedGetAllUseCase: SavedGetAllUseCase,
        private val resourceProvider: ResourceProvider,
        private val dispatcherProvider: DispatcherProvider,
        private val listStateHolder: LazyListStateHolder,
        private val topAppBarStateHolder: TopAppBarStateHolder,
    ) : ComponentFactory<Child.SavedMovieChild, Route.Saved> {
        override fun create(
            route: Route.Saved,
            child: ComponentContext,
            root: RootComponent
        ): Child.SavedMovieChild {
            val comp = SavedMovieScreenComponent(
                child,
                dispatcherProvider,
                savedGetAllUseCase,
                listStateHolder,
                topAppBarStateHolder,
                resourceProvider,
                doBack = { root.onBack() },
                showOverview = { root.nav(Route.Overview(it)) }
            )
            return Child.SavedMovieChild(comp)
        }
    }
}
