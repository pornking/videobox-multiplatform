package ru.shadowsparky.videobox.multiplatform.shared.data.api

import io.ktor.client.HttpClient
import ru.shadowsparky.videobox.multiplatform.shared.data.get
import ru.shadowsparky.videobox.multiplatform.shared.domain.ServerConfigurationRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.getServerConfiguration
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideosResponse

class VideoApiImpl(
    private val httpClient: HttpClient,
    private val serverConfigurationProvider: ServerConfigurationRepository
) : VideoApi {

    override suspend fun fetchNewVideos(search: String?, page: Int, sort: String): VideosResponse {
        val params = hashMapOf<String, String>().apply {
            search?.let { put(VideoApi.SEARCH_ARG, search) }
        }
        return get(VideoApi.VIDEOS_PATH, params)
    }

    override suspend fun fetchDetails(id: Long): VideoDetails {
        val params = hashMapOf<String, String>().apply {
            put(VideoApi.ID_ARG, id.toString())
        }
        return get(VideoApi.DETAILS_PATH, params)
    }

    override suspend fun fetchVideoLinks(id: Long, seasonId: Long?): VideoLinksResponse {
        val params = hashMapOf<String, String>().apply {
            put(VideoApi.ID_ARG, id.toString())
            seasonId?.let {
                put(VideoApi.SEASON_ID_ARG, it.toString())
            }
        }
        return get(VideoApi.LINKS_PATH, params)
    }

    private suspend inline fun <reified T> get(path: String, params: Map<String, String>): T {
        return httpClient.get(path, params, serverConfigurationProvider.getServerConfiguration())
    }
}
