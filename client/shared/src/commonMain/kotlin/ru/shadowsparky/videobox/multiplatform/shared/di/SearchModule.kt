package ru.shadowsparky.videobox.multiplatform.shared.di

import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.data.RemoteEventListenerImpl
import ru.shadowsparky.videobox.multiplatform.shared.data.search.RemoteSearchRepository
import ru.shadowsparky.videobox.multiplatform.shared.data.search.SqlSearchRepository
import ru.shadowsparky.videobox.multiplatform.shared.data.search.SyncClientServerSearchRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.EventType
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventHandler
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventListener
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchRepository

val searchModule = module {
    factory(named(DbType.LOCAL)) {
        SqlSearchRepository(get())
    } bind SearchRepository::class
    factory(named(DbType.REMOTE)) {
        RemoteSearchRepository(get(), get(), get())
    } bind SearchRepository::class
    factory {
        SyncClientServerSearchRepository(
            get(named(DbType.REMOTE)),
            get(named(DbType.LOCAL)),
            get(),
            get()
        )
    } bind SearchRepository::class
    factory(named(EventType.SEARCH)) {
        RemoteEventListenerImpl(
            get(),
            get(),
            get(),
            get(),
            get(),
            RemoteEventHandler.SEARCH_CHANGED
        )
    } bind RemoteEventListener::class
}
