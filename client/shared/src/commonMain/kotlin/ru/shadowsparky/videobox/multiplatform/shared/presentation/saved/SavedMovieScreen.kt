package ru.shadowsparky.videobox.multiplatform.shared.presentation.saved

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.presentation.BaseScreen
import ru.shadowsparky.videobox.multiplatform.shared.presentation.SimpleAppBar
import ru.shadowsparky.videobox.multiplatform.shared.presentation.list.VideoScreenContent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.state.rememberTopAppBarStateByHolder

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SavedMovieScreen(comp: SavedMovieScreenComponent) {
    val appBarState = rememberTopAppBarStateByHolder(comp.topAppBarStateHolder)
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(appBarState)
    val error by comp.errorMsg.collectAsState(null)
    val items by comp.all.collectAsState(null)
    BaseScreen(
        scrollBehavior,
        appBar = {
            SimpleAppBar(
                comp.resourceProvider.getString(ApplicationString.SAVED_MOVIES),
                doBack = { comp.doBack() },
                scrollBehavior
            )
        },
        content = {
            VideoScreenContent(
                it,
                error,
                items,
                onRefresh = { comp.load() },
                onShowOverview = { comp.showOverview(it) },
                resourceProvider = comp.resourceProvider,
                emptyResource = ApplicationString.SAVED_MOVIES_NOT_FOUND,
                holder = comp.listStateHolder
            )
        }
    )
}
