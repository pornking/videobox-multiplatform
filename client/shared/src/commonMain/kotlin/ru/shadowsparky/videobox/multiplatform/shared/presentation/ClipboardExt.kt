package ru.shadowsparky.videobox.multiplatform.shared.presentation

import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.ui.platform.ClipboardManager
import androidx.compose.ui.text.AnnotatedString
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider

fun SnackbarHostState.clipboardUrl(
    url: String,
    scope: CoroutineScope,
    clipboardManager: ClipboardManager,
    resourceProvider: ResourceProvider
) {
    clipboardManager.setText(AnnotatedString(url))
    scope.launch {
        currentSnackbarData?.dismiss()
        showSnackbar(
            message = resourceProvider.getString(ApplicationString.URL_COPIED),
            duration = SnackbarDuration.Short
        )
    }
}
