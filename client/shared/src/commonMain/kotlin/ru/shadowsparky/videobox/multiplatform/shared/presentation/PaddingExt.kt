package ru.shadowsparky.videobox.multiplatform.shared.presentation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun Modifier.applyColumnPadding(paddingValues: PaddingValues): Modifier {
    return padding(paddingValues)
}

val PaddingValues.asColumnPadding: PaddingValues
    get() { return PaddingValues(0.dp) }
