package ru.shadowsparky.videobox.di

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import org.koin.dsl.binds
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.domain.RecentlyWatchedRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.SavedMovieRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.SearchRepository
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.RecentlyWatchedInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails

val dbRepoModule = module {
    single { MockRecentlyWatchedRepository() } binds arrayOf(RecentlyWatchedRepository::class, MockRecentlyWatchedRepository::class)
    single { MockSearchRepository() } binds arrayOf(SearchRepository::class, MockSearchRepository::class)
    single { MockSavedMovieRepository() } binds arrayOf(SavedMovieRepository::class, MockSavedMovieRepository::class)
}

class MockSavedMovieRepository : SavedMovieRepository {
    override suspend fun save(details: VideoDetails) {
    }
    override suspend fun getAll(): Flow<List<VideoDetails>> {
        return flowOf(emptyList())
    }

    override suspend fun isSaved(id: Long): Flow<Boolean> {
        return flowOf(false)
    }

    override suspend fun remove(id: Long) {
    }
}

class MockSearchRepository : SearchRepository {
    var list = mutableListOf<String>()

    override suspend fun search(query: String): List<String> {
        return list.filter { it.lowercase().startsWith(query.lowercase()) }
    }

    override suspend fun addToSearch(query: String) {
        list.add(query)
    }

    override suspend fun deleteFromSearch(query: String) {
        list.remove(query)
    }

    override suspend fun clear() {
        list = mutableListOf()
    }
}

class MockRecentlyWatchedRepository : RecentlyWatchedRepository {
    var list = mutableListOf<RecentlyWatchedInfo>()

    override suspend fun removeInfo(info: RecentlyWatchedInfo) {
        list.remove(info)
    }

    override suspend fun writeInfo(info: RecentlyWatchedInfo) {
        list.add(info)
    }

    override suspend fun toggleInfo(info: RecentlyWatchedInfo) {
        if (list.contains(info)) {
            removeInfo(info)
        } else {
            writeInfo(info)
        }
    }

    override suspend fun queryRecentlyWatched(movieId: Long, seasonId: Long?): List<RecentlyWatchedInfo> {
        return list
    }

    override suspend fun getAllRecentlyWatched(): List<RecentlyWatchedInfo> {
        return list
    }

    fun reset() {
        list = mutableListOf()
    }
}
