package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.runBlocking
import org.koin.core.component.get
import ru.shadowsparky.videobox.BaseTest
import ru.shadowsparky.videobox.di.MockRecentlyWatchedRepository
import ru.shadowsparky.videobox.di.MockVideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.RecentlyWatchedInfo
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.presentation.ItemSnapshot
import kotlin.test.Test

class EpisodeOverviewComponentTest : BaseTest() {

    @Test
    fun `succeed load links`() {
        val links = VideoLinksResponse.getStub(false)
        get<MockVideoApi>().linksRsp = links
        newComp().apply {
            assert(this.links.value == links)
        }
    }

    @Test
    fun `failed load links`(): Unit = runBlocking {
        get<MockVideoApi>().e = RuntimeException("Test")
        val err = MutableStateFlow<String?>(null)
        assert(err.value == null)
        newComp(ItemSnapshot(errorCode = err)).apply {
            assert(err.value != null)
        }
    }

    @Test
    fun `set watched`(): Unit = runBlocking {
        val links = VideoLinksResponse.getStub(false)
        get<MockVideoApi>().linksRsp = links
        val recentlyRepo = get<MockRecentlyWatchedRepository>()
        val seasonId = 228L
        newComp(seasonId = seasonId).apply {
            assert(recentlyRepo.list.isEmpty())
            toggleWatched(1)
            assert(recentlyRepo.list.first().season == seasonId)
        }
    }

    @Test
    fun `unset watched`(): Unit = runBlocking {
        val links = VideoLinksResponse.getStub(false)
        get<MockVideoApi>().linksRsp = links
        val recentlyRepo = get<MockRecentlyWatchedRepository>()
        val seasonId = 228L
        recentlyRepo.writeInfo(RecentlyWatchedInfo(0, 1, 1, seasonId))
        newComp(seasonId = seasonId).apply {
            assert(recentlyRepo.list.first().season == seasonId)
            toggleWatched(1)
            assert(recentlyRepo.list.isEmpty())
        }
    }

    private fun newComp(
        snapshot: ItemSnapshot<VideoLinksResponse> = ItemSnapshot(),
        seasonId: Long = 1
    ): EpisodeOverviewComponent {
        return EpisodeOverviewComponent(
            get(),
            get(),
            get(),
            get(),
            get(),
            1,
            seasonId,
            {},
            get(),
            snapshot
        ).apply { tryToLoadLinks() }
    }
}
