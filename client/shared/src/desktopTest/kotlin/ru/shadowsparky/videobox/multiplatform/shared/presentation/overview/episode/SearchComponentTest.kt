package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode

import kotlinx.coroutines.runBlocking
import org.koin.core.component.get
import ru.shadowsparky.videobox.BaseTest
import ru.shadowsparky.videobox.di.MockSearchRepository
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.SearchScreenComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.search.SearchScreenComponent.Snapshot
import kotlin.test.Test

class SearchComponentTest : BaseTest() {
    private var searchCalled = false

    override fun before() {
        searchCalled = false
    }

    @Test
    fun `save query`(): Unit = runBlocking {
        val searchRepo = get<MockSearchRepository>()
        newComp().apply {
            assert(searchRepo.list.isEmpty())
            onValueChange(QUERY)
            assert(!searchCalled)
            done()
            assert(searchCalled)
            assert(searchRepo.list.first() == QUERY)
        }
    }

    @Test
    fun `filter already saved`(): Unit = runBlocking {
        val searchRepo = get<MockSearchRepository>().apply {
            addToSearch(QUERY)
            addToSearch("Кто убил дору дуру")
            addToSearch("реальные пацаны (лучший сериал современности)")
            addToSearch("cчастливы вместе")
            addToSearch("$QUERY и иная вариация событий")
        }
        newComp().apply {
            assert(searchRepo.list.isNotEmpty())
            onValueChange(QUERY)
            assert(oldSearch.value.size == 2)
        }
    }

    @Test
    fun `delete by user`(): Unit = runBlocking {
        val searchRepo = get<MockSearchRepository>().apply {
            addToSearch(QUERY)
        }
        newComp().apply {
            removeFromSearch(QUERY)
            assert(oldSearch.value.isEmpty() && searchRepo.list.isEmpty())
        }
    }

    private fun newComp(snap: Snapshot = Snapshot()): SearchScreenComponent {
        return SearchScreenComponent(
            get(),
            { searchCalled = true },
            {},
            get(),
            get(),
            get(),
            get(),
            get(),
            snap
        )
    }

    companion object {
        const val QUERY = "дора дура"
    }
}
