package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode

import kotlinx.coroutines.flow.MutableStateFlow
import org.koin.core.component.get
import ru.shadowsparky.videobox.BaseTest
import ru.shadowsparky.videobox.di.MockVideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.OverviewComponent
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.OverviewComponent.Snapshot
import kotlin.test.Test

class OverviewComponentTest : BaseTest() {

    @Test
    fun `succeed load serial detail`() {
        val details = VideoDetails.getStub("details")
        get<MockVideoApi>().details = details
        newComp().apply {
            assert(this.details.value == details)
        }
    }

    @Test
    fun `succeed load movie detail`() {
        val details = VideoDetails.getStub("details", false)
        val links = VideoLinksResponse.getStub(false)
        get<MockVideoApi>().apply {
            this.details = details
            this.linksRsp = links
        }
        newComp().apply {
            assert(this.details.value == details)
            assert(this.movieLink.value == links.files) {
                movieLink.value?.toString() ?: "null"
            }
        }
    }

    @Test
    fun `failed load detail`() {
        get<MockVideoApi>().e = RuntimeException()
        val err = MutableStateFlow<String?>(null)
        newComp(Snapshot(errorCode = err)).apply {
            assert(err.value != null)
        }
    }

    @Test
    fun `failed load movie url`() {
        val details = VideoDetails.getStub("details", false)
        get<MockVideoApi>().apply {
            this.details = details
            get<MockVideoApi>().linksE = RuntimeException()
        }
        val err = MutableStateFlow<String?>(null)
        newComp(Snapshot(errorCode = err)).apply {
            assert(this.details.value == null)
            assert(this.movieLink.value == null)
            assert(err.value != null)
        }
    }

    private fun newComp(snap: Snapshot = Snapshot()): OverviewComponent {
        return OverviewComponent(get(), get(), get(), get(), get(), 1, {}, {}, get(), get(), snap).apply {
            init()
        }
    }
}
