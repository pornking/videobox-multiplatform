package ru.shadowsparky.videobox.di

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.DefaultComponentContext
import com.arkivanov.essenty.lifecycle.LifecycleRegistry
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.runBlocking
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.binds
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.di.IS_DEBUG_KEY
import ru.shadowsparky.videobox.multiplatform.shared.di.appModule
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.DispatcherProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.newKoinModules
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEvent
import ru.shadowsparky.videobox.multiplatform.shared.domain.RemoteEventListener
import ru.shadowsparky.videobox.multiplatform.shared.domain.interactor.AboutVideoInteractor
import ru.shadowsparky.videobox.multiplatform.shared.presentation.VideoLauncher

val testUtilsModule = module {
    single { MockVideoLauncher() } binds arrayOf(MockVideoLauncher::class, VideoLauncher::class)
    single { AboutVideoInteractor(get(), get()) }
    single { DefaultComponentContext(LifecycleRegistry()) } bind ComponentContext::class
    single {
        object : DispatcherProvider {
            override val main = unconfined
            override val io = unconfined
        }
    } bind DispatcherProvider::class
    single {
        object : RemoteEventListener {
            override val event: Flow<RemoteEvent> = emptyFlow()
        }
    } bind RemoteEventListener::class
    single(named(IS_DEBUG_KEY)) { true }
}

class MockVideoLauncher : VideoLauncher() {
    var launchedUri: String? = null
    override fun launch(uri: String) {
        launchedUri = uri
    }

    fun reset() {
        launchedUri = null
    }
}

fun attachKoin(): Unit = runBlocking {
    startKoin {
        loadKoinModules(newKoinModules(false))
        allowOverride(true)
        loadKoinModules(newKoinTestModules())
    }
}

fun newKoinTestModules(): List<Module> {
    return listOf(
        appModule,
        dbRepoModule,
        httpModule,
        testUtilsModule,
    )
}
