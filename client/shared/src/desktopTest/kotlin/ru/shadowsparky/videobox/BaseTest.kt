package ru.shadowsparky.videobox

import kotlinx.coroutines.runBlocking
import org.koin.core.component.KoinComponent
import org.koin.core.context.stopKoin
import ru.shadowsparky.videobox.di.attachKoin
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.assertTrue

abstract class BaseTest : KoinComponent {
    @BeforeTest
    fun attachKoinToTest() {
        attachKoin()
        before()
    }

    protected open fun before() {

    }

    protected fun assert(cond: Boolean) {
        assertTrue(cond)
    }

    protected fun assert(cond: Boolean, msgCallback: () -> Any) {
        assertTrue(cond, msgCallback.invoke().toString())
    }

    @AfterTest
    fun after() = runBlocking {
        stopKoin()
    }
}
