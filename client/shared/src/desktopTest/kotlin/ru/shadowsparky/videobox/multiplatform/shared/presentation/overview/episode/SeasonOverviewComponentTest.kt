package ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.episode

import kotlinx.coroutines.flow.MutableStateFlow
import org.koin.core.component.get
import ru.shadowsparky.videobox.BaseTest
import ru.shadowsparky.videobox.di.MockVideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.Season
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.presentation.ListSnapshot
import ru.shadowsparky.videobox.multiplatform.shared.presentation.overview.season.SeasonOverviewComponent
import kotlin.test.Test

class SeasonOverviewComponentTest : BaseTest() {

    @Test
    fun `succeed load seasons`() {
        val rsp = VideoLinksResponse.getStub(true)
        get<MockVideoApi>().linksRsp = rsp
        newComp().apply {
            assert(season.value == rsp.seasons) {
                season.value?.toString() ?: "null"
            }
        }
    }

    @Test
    fun `failed load seasons`() {
        get<MockVideoApi>().e = RuntimeException()
        val errorFlow = MutableStateFlow<String?>(null)
        newComp(ListSnapshot(errorCode = errorFlow)).apply {
            assert(errorFlow.value != null)
        }
    }

    private fun newComp(snap: ListSnapshot<Season> = ListSnapshot()): SeasonOverviewComponent {
        return SeasonOverviewComponent(
            get(),
            get(),
            get(),
            get(),
            1,
            {},
            {},
            snap
        ).apply { load() }
    }
}
