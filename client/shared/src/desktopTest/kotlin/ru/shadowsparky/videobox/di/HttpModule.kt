package ru.shadowsparky.videobox.di

import org.koin.dsl.binds
import org.koin.dsl.module
import ru.shadowsparky.videobox.multiplatform.shared.domain.VideoApi
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoLinksResponse
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideosResponse

val httpModule = module {
    single { MockVideoApi() } binds arrayOf(VideoApi::class, MockVideoApi::class)
}

class MockVideoApi : VideoApi {
    var newVideosRsp: VideosResponse? = null
    var details: VideoDetails? = null
    var e: Exception? = null
    var linksE: Exception? = null
    var linksRsp: VideoLinksResponse? = null

    override suspend fun fetchNewVideos(search: String?, page: Int, sort: String): VideosResponse {
        e?.let { throw it }
        return newVideosRsp!!
    }

    override suspend fun fetchDetails(id: Long): VideoDetails {
        e?.let { throw it }
        return details!!
    }

    override suspend fun fetchVideoLinks(id: Long, seasonId: Long?): VideoLinksResponse {
        e?.let { throw it }
        linksE?.let { throw it }
        return linksRsp!!
    }
}
