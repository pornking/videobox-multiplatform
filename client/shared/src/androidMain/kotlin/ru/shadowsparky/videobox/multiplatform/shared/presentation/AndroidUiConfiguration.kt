package ru.shadowsparky.videobox.multiplatform.shared.presentation

import android.os.Build
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import ru.shadowsparky.videobox.multiplatform.shared.domain.Theme

class AndroidUiConfiguration : UiConfiguration() {

    @Composable
    override fun getColorScheme(theme: Theme): ColorScheme {
        val isDarkTheme = theme.isDark()
        val dynamicColor = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S
        return if (isDarkTheme && dynamicColor) {
            dynamicDarkColorScheme(LocalContext.current)
        } else if (!isDarkTheme && dynamicColor) {
            dynamicLightColorScheme(LocalContext.current)
        } else if (isDarkTheme) {
            darkColorScheme()
        } else {
            lightColorScheme()
        }
    }
}
