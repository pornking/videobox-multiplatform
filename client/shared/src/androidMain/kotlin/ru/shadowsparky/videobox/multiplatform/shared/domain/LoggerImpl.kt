package ru.shadowsparky.videobox.multiplatform.shared.domain

import android.util.Log

actual class LoggerImpl {
    actual fun log(tag: String, msg: String) {
        Log.d(tag, msg)
    }
    actual fun log(tag: String, e: Throwable, message: String?) {
        Log.e(tag, message, e)
    }
}
