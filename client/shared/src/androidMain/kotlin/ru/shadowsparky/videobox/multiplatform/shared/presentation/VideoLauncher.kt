package ru.shadowsparky.videobox.multiplatform.shared.presentation

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString

actual open class VideoLauncher : KoinComponent {
    private val context by lazy { get<Context>() }

    actual open fun launch(uri: String) {
        if (!launchInternal(uri, true)) {
            if (!launchInternal(uri, false)) {
                Toast.makeText(
                    context,
                    ApplicationString.CANT_OPEN_VIDEO_VIEWER.raw,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun launchInternal(uri: String, withType: Boolean): Boolean {
        val intent = Intent(Intent.ACTION_VIEW)
            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (withType) {
            intent.setDataAndType(Uri.parse(uri), "video/*")
        } else {
            intent.data = Uri.parse(uri)
        }
        try {
            context.startActivity(intent)
        } catch (e: Exception) {
            return false
        }
        return true
    }
}
