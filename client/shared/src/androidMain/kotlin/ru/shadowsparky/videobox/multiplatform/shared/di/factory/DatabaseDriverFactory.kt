package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import android.app.Application
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.android.AndroidSqliteDriver
import org.koin.core.component.get
import ru.shadowsparky.videobox.multiplatform.app.AppDatabase
import ru.shadowsparky.videobox.multiplatform.shared.di.koin

actual class DatabaseDriverFactory {

    // https://github.com/cashapp/sqldelight/issues/5058
    actual suspend fun createDriver(): SqlDriver {
        val app = koin.get<Application>()
        return AndroidSqliteDriver(AppDatabase.Schema, app, "videobox.db")
    }
}
