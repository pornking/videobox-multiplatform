package ru.shadowsparky.videobox.multiplatform.shared.presentation

import android.content.Context
import android.content.Intent
import org.koin.core.component.get
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.domain.model.VideoDetails

// adb shell am start -W -a android.intent.action.VIEW -d "https://shadowsparky.ru:444/deep-link/overview?id=191" ru.shadowsparky.videobox.multiplatform
// adb shell am start -W -a android.intent.action.VIEW -d "app://shadowsparky.videobox/overview?id=191" ru.shadowsparky.videobox.multiplatform
actual class ShareManager actual constructor() {
    private val context: Context by lazy { koin.get() }

    actual fun shareDetails(details: VideoDetails) {
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, "https://shadowsparky.ru:444/deep-link/overview?id=${details.id}")
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        context.startActivity(intent)
    }

    actual fun canShare(): Boolean = true
}
