package ru.shadowsparky.videobox.multiplatform.shared.di.factory

import android.app.Application
import android.content.Context
import com.russhwolf.settings.Settings
import com.russhwolf.settings.SharedPreferencesSettings
import org.koin.core.component.get
import ru.shadowsparky.videobox.multiplatform.shared.di.koin

actual class SettingsFactory {
    actual fun create(): Settings {
        val prefs = koin.get<Application>().getSharedPreferences("app_settings", Context.MODE_PRIVATE)
        return SharedPreferencesSettings(prefs)
    }
}
