
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.CanvasBasedWindow
import com.arkivanov.decompose.DefaultComponentContext
import com.arkivanov.essenty.lifecycle.LifecycleRegistry
import com.arkivanov.essenty.lifecycle.destroy
import kotlinx.browser.window
import org.jetbrains.skiko.wasm.onWasmReady
import org.koin.core.component.get
import org.koin.core.context.GlobalContext.startKoin
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ApplicationString
import ru.shadowsparky.videobox.multiplatform.shared.di.factory.ResourceProvider
import ru.shadowsparky.videobox.multiplatform.shared.di.koin
import ru.shadowsparky.videobox.multiplatform.shared.di.newKoinModules
import ru.shadowsparky.videobox.multiplatform.shared.presentation.Nav
import ru.shadowsparky.videobox.multiplatform.shared.presentation.RootComponent

@OptIn(ExperimentalComposeUiApi::class)
fun main() {
    startKoin {
        modules(newKoinModules(true))
    }
    val lifecycle = LifecycleRegistry()
    val root = koin.get<RootComponent.RoootFactory>().create(DefaultComponentContext(lifecycle))
    val resourceProvider = koin.get<ResourceProvider>()
    onWasmReady {
        val appName = resourceProvider.getString(ApplicationString.APP_NAME)
        window.onclose = { lifecycle.destroy() }
        CanvasBasedWindow(appName) {
            Box(modifier = Modifier.fillMaxSize()) {
                Nav(root)
            }
        }
    }
}
