plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization") version libs.versions.kotlin.version.get()
    alias(libs.plugins.jetbrains.compose)
    alias(libs.plugins.compose.compiler)
}

kotlin {
    js(IR) {
        browser()
        binaries.executable()
    }
    sourceSets {
        named("jsMain") {
            dependencies {
                implementation(libs.sql.delight.runtime)
                implementation(project(":client:shared"))
                implementation(libs.web.worker.driver)
                implementation(devNpm("copy-webpack-plugin", "9.1.0"))
                implementation(npm("@cashapp/sqldelight-sqljs-worker", "2.0.1"))
                implementation(npm("sql.js", "1.8.0"))
            }
        }
    }
}

compose.experimental {
    web.application {}
}
