plugins {
    `kotlin-dsl`
}

dependencies {
    compileOnly(libs.plugins.android.library.toDep())
    compileOnly(libs.plugins.android.application.toDep())
    compileOnly(libs.plugins.compose.compiler.toDep())
    compileOnly(libs.plugins.jetbrains.compose.toDep())
    compileOnly(libs.plugins.serialization.toDep())
    compileOnly(libs.plugins.kotlin.multiplatform.toDep())
}

fun Provider<PluginDependency>.toDep() = map {
    "${it.pluginId}:${it.pluginId}.gradle.plugin:${it.version}"
}

val prefix = "ru.shadowsparky.videobox.plugin."

gradlePlugin {
    plugins {
        create("vbKmp") {
            id = "vb-kmp"
            implementationClass = "${prefix}VbKmp"
        }
        create("vbKtorClient") {
            id = "vb-ktor-client"
            implementationClass = "${prefix}VbKtorClient"
        }
        create("vbCompose") {
            id = "vb-compose"
            implementationClass = "${prefix}VbCompose"
        }
        create("vbSerialization") {
            id = "vb-serialization"
            implementationClass = "${prefix}VbSerialization"
        }
        create("vbAndroidLib") {
            id = "vb-android-lib"
            implementationClass = "${prefix}VbAndroidLibrary"
        }
        create("vbSqlDelight") {
            id = "vb-sqldelight"
            implementationClass = "${prefix}VbSqlDelight"
        }
        create("vbServer") {
            id = "vb-server"
            implementationClass = "${prefix}VbServer"
        }
    }
}
