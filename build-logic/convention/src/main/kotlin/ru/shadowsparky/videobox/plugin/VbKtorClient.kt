package ru.shadowsparky.videobox.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies
import ru.shadowsparky.videobox.androidImplementation
import ru.shadowsparky.videobox.desktopImplementation
import ru.shadowsparky.videobox.findLibs
import ru.shadowsparky.videobox.hasAndroidSupport
import ru.shadowsparky.videobox.implementation

class VbKtorClient : Plugin<Project> {
    override fun apply(target: Project) {
        val libs = target.findLibs()
        target.dependencies {
            implementation(libs.findLibrary("ktor.client.websockets").get())
            implementation(libs.findLibrary("ktor-serialization-kotlinx-json").get())
            implementation(libs.findLibrary("ktor-client-content-negotiation").get())
            implementation(libs.findLibrary("ktor-client-logging").get())
            implementation(libs.findLibrary("ktor-client-auth").get())
            implementation(libs.findLibrary("ktor-client-core").get())
            val okHttp = libs.findLibrary("ktor.client.okhttp").get()
            val cio = libs.findLibrary("ktor-client-cio").get()
            if (target.hasAndroidSupport()) {
                androidImplementation(cio)
                desktopImplementation(cio)
            }
            androidImplementation(okHttp)
            desktopImplementation(okHttp)
        }
    }
}
