package ru.shadowsparky.videobox.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.VersionCatalog
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.dependencies
import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension
import ru.shadowsparky.videobox.androidImplementation
import ru.shadowsparky.videobox.findLibs
import ru.shadowsparky.videobox.hasAndroidSupport
import ru.shadowsparky.videobox.implementation

class VbKmp : Plugin<Project> {

    override fun apply(target: Project) = with(target) {
        val libs = findLibs()
        pluginManager.apply(libs.findPlugin("kotlin-multiplatform").get().get().pluginId)
        val hasAndroid = hasAndroidSupport()
        extensions.configure<KotlinMultiplatformExtension> { apply(libs, hasAndroid) }
        dependencies {
            implementation(libs.findLibrary("kotlinx-coroutines-core").get())
            implementation(libs.findLibrary("koin-core").get())
            implementation(libs.findLibrary("image.loader").get())
            implementation(libs.findLibrary("multiplatform.settings").get())
            if (hasAndroid) {
                androidImplementation(libs.findLibrary("kotlinx.coroutines.android").get())
            }
        }
    }

    private fun KotlinMultiplatformExtension.apply(libs: VersionCatalog, hasAndroid: Boolean) {
        if (hasAndroid) {
            androidTarget()
        }
        val javaVersion = libs.findVersion("proj-java").get()
        jvm("desktop") {
            compilations.all {
                kotlinOptions.jvmTarget = javaVersion.toString()
            }
        }
    }
}
