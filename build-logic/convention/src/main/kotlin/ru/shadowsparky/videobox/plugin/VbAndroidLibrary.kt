package ru.shadowsparky.videobox.plugin

import com.android.build.gradle.TestedExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure
import ru.shadowsparky.videobox.findLibs
import ru.shadowsparky.videobox.setupAndroid

class VbAndroidLibrary : Plugin<Project> {
    override fun apply(target: Project): Unit = with(target) {
        val libs = findLibs()
        pluginManager.apply(libs.findPlugin("android.library").get().get().pluginId)
        extensions.configure<TestedExtension> { setupAndroid(target) }
    }
}
