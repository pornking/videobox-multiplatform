package ru.shadowsparky.videobox.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.kotlin.dsl.DependencyHandlerScope
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.withType
import ru.shadowsparky.videobox.findLibs
import ru.shadowsparky.videobox.implementation

class VbServer : Plugin<Project> {
    override fun apply(target: Project): Unit = with(target) {
        val libs = findLibs()
        plugins.apply(libs.findPlugin("java-library").get().get().pluginId)
        plugins.apply(libs.findPlugin("jetbrains-kotlin-jvm").get().get().pluginId)
        plugins.apply(libs.findPlugin("devtools-ksp").get().get().pluginId)

        tasks.withType<JavaCompile> {
            sourceCompatibility = "17"
            targetCompatibility = "17"
        }

        dependencies {
            implementation(libs.findLibrary("koin-core").get())
            implementation(libs.findLibrary("kotlinx-coroutines-core").get())
            implementation(libs.findLibrary("koin-annotations").get())

            ksp(libs.findLibrary("koin-ksp-compiler").get())
        }
    }
}

internal fun DependencyHandlerScope.ksp(deps: Any) {
    add("ksp", deps)
}
