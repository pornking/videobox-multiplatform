package ru.shadowsparky.videobox.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies
import ru.shadowsparky.videobox.androidImplementation
import ru.shadowsparky.videobox.desktopImplementation
import ru.shadowsparky.videobox.findLibs
import ru.shadowsparky.videobox.hasAndroidSupport
import ru.shadowsparky.videobox.implementation

class VbSqlDelight : Plugin<Project> {
    override fun apply(target: Project): Unit = with(target) {
        val libs = findLibs()
        plugins.apply(libs.findPlugin("sqldelight").get().get().pluginId)
        dependencies {
            implementation(libs.findLibrary("coroutines.extensions").get())
            implementation(libs.findLibrary("sql.delight.runtime").get())

            if (hasAndroidSupport()) {
                androidImplementation(libs.findLibrary("androidx.sqlite").get())
                androidImplementation(libs.findLibrary("androidx.sqlite.framework").get())
            }

            desktopImplementation(libs.findLibrary("sqlite.driver").get())
        }
    }
}
